# Documentation #

CADSharpTools.PDM is a utility libraries for SOLIDWORKS API and SOLIDWORKS PDM API. Check documentation [here](https://jliliamen.github.io/CADSharpToolsPDMDocs/html/d594e155-da23-4f32-85ec-b90466286d00.htm).

# How to use #

Use the nuget package manager to get the latset version of the lib: 

``` Install-package CADSharpTools.PDM ``` 