﻿using Microsoft.Win32;
using Prism.Mvvm;
using SolidWorks.Interop.sldworks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace CADSharpTools.SOLIDWORKS
{
    /// <summary>
    /// SOLIDWORKS application singleton class. 
    /// </summary>
    public class Singleton
    {

        private static SldWorks swApp;


         

        private Singleton()
        {
        }
        /// <summary>
        /// Creates an instance of the latest version of SOLIDWORKS asynchronously. 
        /// </summary>
        /// <param name="version">SOLIDWORKS revision number.</param>
        /// <param name="visible">True to start SOLIDWORKS visible.</param>
        /// <returns>MethodReturn of SldWorks, the SOLIDWORKS application API object.</returns>
        public static async Task<SldWorks> GetApplicationAsync(int version = -1, bool visible = false)
        {
            // if app has been closed properly swapp will be null;
            if (swApp == null)
            {

                return await Task.Run(() =>
                {
                    if (version == -1)
                        swApp = Activator.CreateInstance(Type.GetTypeFromProgID("SldWorks.Application")) as SldWorks;
                    else
                        swApp = Activator.CreateInstance(Type.GetTypeFromProgID("SldWorks.Application." + version.ToString())) as SldWorks;

                    if (swApp == null)
                        return null;

                    swApp.Visible = visible;

                    return swApp;
                });
            }

            return swApp;

        }
        /// <summary>
        /// Creates an instance of the latest version of SOLIDWORKS synchronously. 
        /// </summary>
        /// <param name="version">SOLIDWORKS revision number.</param>
        /// <param name="visible">True to start SOLIDWORKS visible.</param>
        /// <returns>MethodReturn of SldWorks, the SOLIDWORKS application API object.</returns>
        public static SldWorks GetApplication(int version = -1, bool visible = false)
        {

            // if app has been closed properly swapp will be null;
            if (swApp == null)
            {


                if (version == -1)
                    swApp = Activator.CreateInstance(Type.GetTypeFromProgID("SldWorks.Application")) as SldWorks;
                else
                    swApp = Activator.CreateInstance(Type.GetTypeFromProgID("SldWorks.Application." + version.ToString())) as SldWorks;

                if (swApp == null)
                    return null;

                swApp.Visible = visible;

                return swApp;

            }

            return swApp;

        }


        /// <summary>
        /// Terminates the SOLIDWORKS crash dialog and starts a new SOLIDWORKS session.
        /// </summary>
        /// <param name="version">SOLIDWORKS revision number.</param>
        /// <returns>MethodReturn of SldWorks, the SOLIDWORKS application API object.</returns>
        /// <remarks>Use this method to terminate the SOLIDWORKS crash dialog.</remarks>
        internal static async Task<SldWorks> ForceGetApplicationAsync(int version = -1)
        {
            return await Task<SldWorks>.Run(() =>
            {
                try
                {
                    Process[] runingProcess = Process.GetProcesses();
                    for (int i = 0; i < runingProcess.Length; i++)
                    {
                        Debug.Print(runingProcess[i].ProcessName);
                        // compare equivalent process by their name
                        if (runingProcess[i].ProcessName == "SLDEXITAPP")
                        {
                            // kill  running process
                            runingProcess[i].Kill();
                        }

                    }
                }
                catch (Exception e)
                {
                    // 
                    Debug.Print(e.Message);
                }

                if (version == -1)
                    swApp = Activator.CreateInstance(Type.GetTypeFromProgID("SldWorks.Application")) as SldWorks;
                else
                    swApp = Activator.CreateInstance(Type.GetTypeFromProgID("SldWorks.Application." + version.ToString())) as SldWorks;

                if (swApp == null)
                    return null;

                swApp.Visible = true;

                return swApp;
            });
        }






        /// <summary>
        /// Exits the SOLIDWORKS application and disposes of SOLIDWORKS in memory.
        /// </summary>
        /// <remarks>Call this method after you call <see cref="Close"/>.</remarks>
        public static void Dispose()
        {
            if (swApp != null)
            {                
                Marshal.ReleaseComObject(swApp);
                swApp = null;
            }
        }

        /// <summary>
        /// Closes the opened session of SOLIDWORKS
        /// </summary>
        public static void Close()
        {
            if (swApp != null)
            swApp.ExitApp();
        }
        
        
    }







}