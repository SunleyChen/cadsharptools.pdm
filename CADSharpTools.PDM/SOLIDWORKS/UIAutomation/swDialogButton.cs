﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace CADSharpTools.SOLIDWORKS.UIAutomation
{

    /// <summary>
    /// Button class.
    /// </summary>
    ///<remarks>UI Automation is only valid for English language versions of SOLIDWORKS.</remarks>
    public class swDialogButton
    {
        /// <summary>
        ///  Expected action.
        /// </summary>
        public swDialogButtonAction_e AssociatedAction { get; set; }
        /// <summary>
        /// Associated <see cref="AutomationElement"/> object.
        /// </summary>
        public AutomationElement AssociatedAutomationElement { get; set; }
    }
}
