﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CADSharpTools.SOLIDWORKS.UIAutomation
{

    /// <summary>
    /// SOLIDWORKS dialog.
    /// </summary>
    ///<remarks>UI Automation is only valid for English language versions of SOLIDWORKS.</remarks>
    public class swDialog
    {

        /// <summary>
        /// Sets how the dialog box is compared against others.
        /// </summary>
        public swDialogCompareMethod_e CompareMethod { get; private set; } = swDialogCompareMethod_e.Undefined;

        /// <summary>
        /// Gets or sets the dialog title.
        /// </summary>
        public string Title { get; private set; } = string.Empty;
        /// <summary>
        /// Gets or sets the captions. Captions dialog text labels.
        /// </summary>
        public string[] Captions { get; private set; } = new string[] { };
        /// <summary>
        /// Gets or sets the action to perform.
        /// </summary>
        public swDialogButtonAction_e UserAction { get; private set; } = swDialogButtonAction_e.Undefined;

        /// <summary>
        /// Gets or sets dialog's buttons.
        /// </summary>
        public swDialogButton[] Buttons { get; private set; } = new swDialogButton[] { };


        /// <summary>
        /// Adds a new button to the dialog.
        /// </summary>
        /// <param name="button">Button object.</param>
        public void AddButton(swDialogButton button)
        {
            var internalList = new List<swDialogButton>();
            internalList.AddRange(Buttons);
            if (internalList.Exists(x => x.AssociatedAction == button.AssociatedAction) == false)
            {
                internalList.Add(button);
                this.Buttons = internalList.ToArray();
            }

        }

        /// <summary>
        /// Creates a new instance of the SOLIDWORKS dialog class.
        /// </summary>
        /// <param name="title">Title of the dialog box.</param>
        /// <param name="userAction">Action for <see cref="swDialogWatcher"/> to perform on dialog.</param>
        /// <param name="captions">Captions found in this dialog box.</param>
        /// <param name="compareMethod">Compare method.</param>
        public swDialog(string title, string[] captions = null, swDialogButtonAction_e userAction = swDialogButtonAction_e.Undefined, swDialogCompareMethod_e compareMethod = swDialogCompareMethod_e.Undefined)
        {
            this.Title = title;
            this.Captions = captions;
            this.UserAction = userAction;
            this.CompareMethod = compareMethod;
        }
        /// <summary>
        /// Creates an instance of the Open file dialog.
        /// </summary>
        /// <returns>Dialog object</returns>
        public static swDialog CreateOpenFileDialog()
        {
            var dialog = new swDialog("New SOLIDWORKS Document");
            dialog.CompareMethod = swDialogCompareMethod_e.Title;
            return dialog;
        }


        /// <summary>
        /// Creates an instance of the low memory dialog.
        /// </summary>
        /// <returns>Dialog object</returns>
        public static swDialog CreateSystemLowMemoryDialog()
        {
            var dialog = new swDialog("SOLIDWORKS", new string[] { "WARNING! Your system is running critically low on memory." });
            dialog.CompareMethod = swDialogCompareMethod_e.TitleAndCaptions;
            return dialog;
        }

        /// <summary>
        /// Creates an instance of the Options dialog.
        /// </summary>
        /// <returns>Dialog object </returns>
        public static swDialog CreateOptionsDialog()
        {
            var dialog = new swDialog("System Options - General");
            dialog.CompareMethod = swDialogCompareMethod_e.Title;
            return dialog;
        }


        /// <summary>
        /// Returns if two controls are equal.
        /// </summary>
        /// <param name="control">Target control.</param>
        /// <returns></returns>
        public bool IsEqual(swDialog control)
        {

            switch (this.CompareMethod)
            {
                case swDialogCompareMethod_e.Undefined:
                    break;
                case swDialogCompareMethod_e.Title:
                    {
                        if (this.Title != control.Title)
                            return false;
                        else if (this.Title.Trim() == control.Title.Trim())
                            return true;
                    }
                    break;
                case swDialogCompareMethod_e.ContainsCaptions:
                    {
                        if (this.Captions != null || Captions.Length == 0)
                            return true;

                        foreach (var controlCaption in control.Captions)
                        {
                            if (this.Captions.ToList().Exists(x => x.Trim() == controlCaption.Trim()))
                                return true;
                        }
                        return false;
                    }
                    break;
                case swDialogCompareMethod_e.TitleAndCaptions:
                    {
                        bool isTitle = false;
                        bool areCaptions = false;
                        if (this.Captions != null || Captions.Length == 0)
                            isTitle = true;

                        foreach (var controlCaption in control.Captions)
                        {
                            if (this.Captions.ToList().Exists(x => x.Trim() == controlCaption.Trim()))
                                areCaptions = true;
                        }

                        if (isTitle || areCaptions)
                            return true;
                        else
                            return false;
                    }
                    break;
                default:
                    break;
            }





            return true;
        }

    }

}
