﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CADSharpTools.SOLIDWORKS.UIAutomation
{


    /// <summary>
    /// Action to perform on SOLIDWORKS dialogs.
    /// </summary>
    ///<remarks>UI Automation is only valid for English language versions of SOLIDWORKS.</remarks>
    public enum swDialogButtonAction_e
    {
        /// <summary>
        /// Undefined action.
        /// </summary>
        Undefined,
        /// <summary>
        /// Click on the Yes button.
        /// </summary>
        ClickYes,
        /// <summary>
        /// Click on the No button.
        /// </summary>
        ClickNo,
        /// <summary>
        /// Click on the OK button.
        /// </summary>
        ClickOK,
        /// <summary>
        /// Click on the Cancel button.
        /// </summary>
        ClickCancel,
        /// <summary>
        /// Click on the Close button.
        /// </summary>
        CloseDialog
    }
    
    
    
    /// <summary>
    /// Compare method approaches for dialog box.
    /// </summary>
    public enum swDialogCompareMethod_e
    {
        /// <summary>
        /// Undefined compare method.
        /// </summary>
        Undefined,
        /// <summary>
        /// Title.
        /// </summary>
        Title,
        /// <summary>
        /// Compare captions.
        /// </summary>
        ContainsCaptions,
        /// <summary>
        /// Compare title and captions.
        /// </summary>
        TitleAndCaptions
    }
}
