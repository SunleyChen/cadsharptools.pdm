﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;
using static System.Windows.Automation.AutomationElement;

namespace CADSharpTools.SOLIDWORKS.UIAutomation
{

    /// <summary>
    /// Allows user to perform UI actions such as button clicks on SOLIDWORKS dialogs.
    /// </summary>
    ///<remarks>UI Automation is only valid for English language versions of SOLIDWORKS.</remarks>
    ///<example>
    /// Create a console application and run the code below after install CADSharpTools.PDM via Nuget. Attempt to create a new document by going to File > New and the application will automatically click on OK. 
    ///<code>
    ///using CADSharpTools.SOLIDWORKS;
    ///using System;
    ///using SolidWorks.Interop.sldworks;
    ///namespace CADSharpTools.PDM.Tests
    ///{
    ///    public class Test
    ///    {
    ///        [STAThread]
    ///       static void Main(string[] args)
    ///        {
    ///            var swApp = CADSharpTools.SOLIDWORKS.Singleton.GetApplication(-1, true);
    ///            int processID = swApp.GetProcessID();
    ///            var newDocumentDialog =
    ///                new CADSharpTools.SOLIDWORKS.UIAutomation.swDialog("New SOLIDWORKS Document", null, SOLIDWORKS.UIAutomation.swDialogButtonAction_e.ClickOK, SOLIDWORKS.UIAutomation.swDialogCompareMethod_e.Title);
    ///            var swDialogWatcher = new CADSharpTools.SOLIDWORKS.UIAutomation.swDialogWatcher(processID);
    ///            swDialogWatcher.AddDialog(newDocumentDialog);
    ///            swDialogWatcher.Start();
    ///            // swDialogWatcher.Stop() stops watching.
    ///            Console.ReadLine();
    ///        }
    ///    }
    ///}
    /// </code>
    /// </example>
    public class swDialogWatcher : BindableBase
    {
        private AutomationElement sldElement;
        private bool isIdle = true;

        /// <summary>
        /// Returns whether the dialog watcher class is idle or not. This property implements <see cref="System.ComponentModel.INotifyPropertyChanged"/>.
        /// </summary>
        public bool IsIdle
        {
            get { return isIdle; }
            private set { SetProperty(ref isIdle, value); }
        }

        /// <summary>
        /// Adds a new dialog to be watched. 
        /// </summary>
        /// <param name="dialog">Dialog</param>
        public void AddDialog(swDialog dialog)
        {
            var list = Dialogs.ToList();
            list.Add(dialog);
            Dialogs = list.ToArray();
        }


        /// <summary>
        /// Remove a dialog from the watcher's list of dialogs to watch. 
        /// </summary>
        /// <param name="dialog">Dialog</param>
        public void RemoveDialog(swDialog dialog)
        {
            var list = Dialogs.ToList();
            list.Remove(dialog);
            Dialogs = list.ToArray();
        }

        /// <summary>
        ///  Dialogs to be watched.
        /// </summary>
        /// <remarks>If this property is set to null or an empty array, the object will attempt to perform <see cref="GenericAction"/> on all SOLIDWORKS dialogs.</remarks>
        public swDialog[] Dialogs { get; private set; } = new swDialog[] { };

        /// <summary>
        /// Action to perform on all dialog if <see cref="swDialogWatcher.Dialogs"/> is empty or null.
        /// </summary>
        public swDialogButtonAction_e GenericAction { get; set; } = swDialogButtonAction_e.CloseDialog;

        /// <summary>
        /// Gets or sets main window handle of the affected SOLIDWORKS instance.
        /// </summary>
        public long Handle { get; set; }

        
        /// <summary>
        /// Creates a new instance of the SOLIDWORKS dialog watcher class.
        /// </summary>
        /// <param name="handle">Handle of the main window.</param>
        public swDialogWatcher(long handle)
        {
            this.Handle = handle;
        }
        /// <summary>
        /// Starts watching dialogs.
        /// </summary>
        public void Start()
        {
            this.IsIdle = true;
             
            sldElement = AutomationElement.FromHandle(new IntPtr(Handle));
            Automation.AddAutomationEventHandler(WindowPattern.WindowOpenedEvent, sldElement, TreeScope.Subtree, (s, e) =>
            {

                var element = s as AutomationElement;
                var foundDialog = GetDialogFromAutomationElement(element);
                if (foundDialog == null)
                    return;

                if (Dialogs != null)
                    foreach (var Dialog in Dialogs)
                    {
                        if (Dialog.IsEqual(foundDialog))
                        {
                            switch (Dialog.UserAction)
                            {
                                case swDialogButtonAction_e.Undefined:
                                    break;
                                case swDialogButtonAction_e.ClickYes:
                                    {
                                        var YesButton = foundDialog.Buttons.ToList().Find(x => x.AssociatedAction == swDialogButtonAction_e.ClickYes);
                                        if (YesButton != null)
                                        {
                                            var associatedElement = YesButton.AssociatedAutomationElement;
                                            var invokePattern = associatedElement.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                            invokePattern.Invoke();
                                        }
                                    }
                                    break;
                                case swDialogButtonAction_e.ClickNo:
                                    var NoButton = foundDialog.Buttons.ToList().Find(x => x.AssociatedAction == swDialogButtonAction_e.ClickNo);
                                    if (NoButton != null)
                                    {
                                        var associatedElement = NoButton.AssociatedAutomationElement;
                                        var invokePattern = associatedElement.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                        invokePattern.Invoke();
                                    }
                                    break;
                                case swDialogButtonAction_e.ClickOK:
                                    var OKButton = foundDialog.Buttons.ToList().Find(x => x.AssociatedAction == swDialogButtonAction_e.ClickOK);
                                    if (OKButton != null)
                                    {
                                        var associatedElement = OKButton.AssociatedAutomationElement;
                                        var invokePattern = associatedElement.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                        invokePattern.Invoke();
                                    }
                                    break;
                                case swDialogButtonAction_e.ClickCancel:
                                    var CancelButton = foundDialog.Buttons.ToList().Find(x => x.AssociatedAction == swDialogButtonAction_e.ClickCancel);
                                    if (CancelButton != null)
                                    {
                                        var associatedElement = CancelButton.AssociatedAutomationElement;
                                        var invokePattern = associatedElement.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                        invokePattern.Invoke();
                                    }
                                    break;
                                case swDialogButtonAction_e.CloseDialog:
                                    {
                                        var windowPattern = element.GetCurrentPattern(WindowPattern.Pattern) as WindowPattern;
                                        windowPattern.Close();
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                else
                {
                    var foundDialog_ = GetDialogFromAutomationElement(element);

                    switch (GenericAction)
                    {
                        case swDialogButtonAction_e.Undefined:
                            break;
                        case swDialogButtonAction_e.ClickYes:
                            {
                                var YesButton = foundDialog_.Buttons.ToList().Find(x => x.AssociatedAction == swDialogButtonAction_e.ClickYes);
                                if (YesButton != null)
                                {
                                    var associatedElement = YesButton.AssociatedAutomationElement;
                                    var invokePattern = associatedElement.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                    invokePattern.Invoke();
                                }
                            }
                            break;
                        case swDialogButtonAction_e.ClickNo:
                            var NoButton = foundDialog_.Buttons.ToList().Find(x => x.AssociatedAction == swDialogButtonAction_e.ClickNo);
                            if (NoButton != null)
                            {
                                var associatedElement = NoButton.AssociatedAutomationElement;
                                var invokePattern = associatedElement.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                invokePattern.Invoke();
                            }
                            break;
                        case swDialogButtonAction_e.ClickOK:
                            var OKButton = foundDialog_.Buttons.ToList().Find(x => x.AssociatedAction == swDialogButtonAction_e.ClickOK);
                            if (OKButton != null)
                            {
                                var associatedElement = OKButton.AssociatedAutomationElement;
                                var invokePattern = associatedElement.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                invokePattern.Invoke();
                            }
                            break;
                        case swDialogButtonAction_e.ClickCancel:
                            var CancelButton = foundDialog_.Buttons.ToList().Find(x => x.AssociatedAction == swDialogButtonAction_e.ClickCancel);
                            if (CancelButton != null)
                            {
                                var associatedElement = CancelButton.AssociatedAutomationElement;
                                var invokePattern = associatedElement.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                invokePattern.Invoke();
                            }
                            break;
                        case swDialogButtonAction_e.CloseDialog:
                            var windowPattern = element.GetCurrentPattern(WindowPattern.Pattern) as WindowPattern;
                            windowPattern.Close();
                            break;
                        default:
                            break;
                    }

                }
            });


            this.IsIdle = false;
        }

        /// <summary>
        /// Stops watching dialogs.
        /// </summary>
        public void Stop()
        {
            this.IsIdle = false;
            Automation.RemoveAutomationEventHandler(WindowPattern.WindowOpenedEvent, sldElement, (s, e) =>
            {
                var element = s as AutomationElement;
                var dialog = GetDialogFromAutomationElement(element);
                if (Dialogs != null)
                    foreach (var supressibleDialog in Dialogs)
                    {
                        if (supressibleDialog.IsEqual(dialog))
                        {
                            switch (dialog.UserAction)
                            {
                                case swDialogButtonAction_e.Undefined:
                                    break;
                                case swDialogButtonAction_e.ClickYes:
                                    {
                                        var YesButton = dialog.Buttons.ToList().Find(x => x.AssociatedAction == swDialogButtonAction_e.ClickYes);
                                        if (YesButton != null)
                                        {
                                            var associatedElement = YesButton.AssociatedAutomationElement;
                                            var invokePattern = associatedElement.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                            invokePattern.Invoke();
                                        }
                                    }
                                    break;
                                case swDialogButtonAction_e.ClickNo:
                                    var NoButton = dialog.Buttons.ToList().Find(x => x.AssociatedAction == swDialogButtonAction_e.ClickNo);
                                    if (NoButton != null)
                                    {
                                        var associatedElement = NoButton.AssociatedAutomationElement;
                                        var invokePattern = associatedElement.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                        invokePattern.Invoke();
                                    }
                                    break;
                                case swDialogButtonAction_e.ClickOK:
                                    var OKButton = dialog.Buttons.ToList().Find(x => x.AssociatedAction == swDialogButtonAction_e.ClickOK);
                                    if (OKButton != null)
                                    {
                                        var associatedElement = OKButton.AssociatedAutomationElement;
                                        var invokePattern = associatedElement.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                        invokePattern.Invoke();
                                    }
                                    break;
                                case swDialogButtonAction_e.ClickCancel:
                                    var CancelButton = dialog.Buttons.ToList().Find(x => x.AssociatedAction == swDialogButtonAction_e.ClickCancel);
                                    if (CancelButton != null)
                                    {
                                        var associatedElement = CancelButton.AssociatedAutomationElement;
                                        var invokePattern = associatedElement.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                        invokePattern.Invoke();
                                    }
                                    break;
                                case swDialogButtonAction_e.CloseDialog:
                                    {
                                        var windowPattern = element.GetCurrentPattern(WindowPattern.Pattern) as WindowPattern;
                                        windowPattern.Close();
                                    }
                                    break;
                                default:
                                    break;
                            }
                            return;
                        }
                    }
                else
                {
                    var windowPattern = element.GetCurrentPattern(WindowPattern.Pattern) as WindowPattern;
                    windowPattern.Close();
                }
            });
            this.IsIdle = true;
        }


        private static swDialog GetDialogFromAutomationElement(AutomationElement control)
        {

            string swTitle = string.Empty;
            string[] swCaptions = new string[] { };
            swDialogButton[] Buttons = new swDialogButton[] { };

            string title = string.Empty;
            try
            {
                title = control.Current.Name;
            }
            catch (Exception)
            {
            }
            


            if (string.IsNullOrWhiteSpace(title) == false)
                swTitle = title;
            else
                return null;

            var buttons = new List<swDialogButton>();
            var captions = new List<string>();

            var traverseAction = default(Action<AutomationElement, int>);
            traverseAction = (e, d) =>
            {
                if (e == null)
                    return;

                AutomationElement elementNode =
                    TreeWalker.ControlViewWalker.GetFirstChild(e);

                while (elementNode != null)
                {

                    string elementName = string.Empty;
                    try
                    {
                         elementName = elementNode.Current.Name;
                    }
                    catch (Exception)
                    {
                    }
                    



                    if (string.IsNullOrWhiteSpace(elementName))
                        elementName = "No Name";

                    string controlType = elementNode.Current.ControlType.LocalizedControlType;

                    if (controlType == "button")
                    {
                        string buttonText = elementName;
                        if (string.IsNullOrWhiteSpace(buttonText) == false)
                        {
                            if (elementName == "OK")
                                buttons.Add(new swDialogButton() { AssociatedAutomationElement = elementNode, AssociatedAction = swDialogButtonAction_e.ClickOK });
                            else if (elementName == "Cancel")
                                buttons.Add(new swDialogButton() { AssociatedAutomationElement = elementNode, AssociatedAction = swDialogButtonAction_e.ClickCancel });
                            if (elementName == "Yes")
                                buttons.Add(new swDialogButton() { AssociatedAutomationElement = elementNode, AssociatedAction = swDialogButtonAction_e.ClickYes });
                            else if (elementName == "No")
                                buttons.Add(new swDialogButton() { AssociatedAutomationElement = elementNode, AssociatedAction = swDialogButtonAction_e.ClickNo });
                        }
                    }
                    else if (controlType == "text")
                    {
                        if (string.IsNullOrWhiteSpace(elementName) == false)
                            captions.Add(elementName);
                    }

                    traverseAction(elementNode, d);
                    elementNode =
                        TreeWalker.ControlViewWalker.GetNextSibling(elementNode);
                }
            };
            traverseAction(control, 5);

            var dialog = new swDialog(swTitle, captions.ToArray(), swDialogButtonAction_e.Undefined, swDialogCompareMethod_e.Undefined);
            buttons.ForEach(x => dialog.AddButton(x));
            return dialog;
        }

    }

}
