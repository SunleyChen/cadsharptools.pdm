﻿using Microsoft.Win32;
using SolidWorks.Interop.sldworks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CADSharpTools.SOLIDWORKS
{
    /// <summary>
    /// A Disposable wrapper over the SOLIDWORKS application.
    /// </summary>
    public class Application : IDisposable
    {
        /// <summary>
        /// Creates a new instance of the Application class.
        /// </summary>
        Application()
        {

        }
        /// <summary>
        /// Returns the SOLIDWORKS installation directory for the specified year (if it exists).
        /// </summary>
        /// <param name="Year">Year</param>
        /// <returns><see cref="DirectoryInfo"/> object, null if failed.</returns>
        public static DirectoryInfo GetSOLIDWORKSInstallationDirectory(int Year)
        {
            using (RegistryKey hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
            {
                using (RegistryKey key = hklm.OpenSubKey(@"SOFTWARE\SolidWorks\SOLIDWORKS " + (Year) + @"\Setup"))
                {
                    if (key == null)
                        return null;
                    else
                    {
                        return new DirectoryInfo(key.GetValue("SolidWorks Folder") as string);
                    }
                }
            }
        }

           /// <summary>
        /// Creates a new instance of the SOLIDWORKS application using <see cref="Process.Start()"/>.
        /// </summary>
        /// <param name="timeoutSec"></param>
        /// <param name="suppressDialog">True to suppress SOLIDWORKS dialogs.</param>
        /// <returns>Pointer to the new instance of SOLIDWORKS.</returns>
        /// <exception cref="TimeoutException">Thrown if method times out.</exception>
        public static ISldWorks CreateSldWorks(bool suppressDialog = false, int timeoutSec = 30)
        {
            int[] years = ReleaseYears();
            if (years.Length == 0)
                throw new Exception("SOLIDWORKS is not installed on this computer.");
            Array.Sort(years);
            int year = years.Last();
            var installationDirectory = GetSOLIDWORKSInstallationDirectory(year);
            if (installationDirectory == null)
                throw new Exception($"Could not find installation directory for SOLIDWORKS. Year = [{year}].");

            string appPath = installationDirectory.FullName;
            var timeout = TimeSpan.FromSeconds(timeoutSec);
            var startTime = DateTime.Now;
            string args = suppressDialog ? "/r" : string.Empty;
            var prc = Process.Start(appPath + "sldworks.exe",args);
            ISldWorks app = null;
            while (app == null)
            {
                if (DateTime.Now - startTime > timeout)
                {
                    throw new TimeoutException();
                }

                app = GetSwAppFromProcess(prc.Id);
            }

            return app;
        }


        /// <summary>
        /// Returns an array of all installed SOLIDWORKS release years.
        /// </summary>
        /// <returns>Array of integers.</returns>
        public static  int[]  ReleaseYears()
        {
            var solidworksKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\SolidWorks");
            var names = solidworksKey.GetSubKeyNames();
            var years = new List<int>();
            if (names == null)
                return years.ToArray();
            

            var regex = new Regex(@"^solidworks ([\d]{4})$", RegexOptions.IgnoreCase);
            foreach (var name in names)
            {
                if (regex.IsMatch(name))
                {
                    int year = int.MinValue;
                    var match = regex.Match(name);
                    var capture = match.Groups[1].Value;
                    var ret = int.TryParse(capture, out year);
                    if (ret)
                        years.Add(year);
                }
            }

            return years.ToArray();
        }

        /// <summary>
        /// Creates a new instance of the latest installed SOLIDWORKS application using <see cref="Process.Start()"/>.
        /// </summary>
        /// <param name="timeoutSec"></param>
        /// <param name="suppressDialog">True to suppress SOLIDWORKS dialogs.</param>
        /// <returns>Pointer to the new instance of SOLIDWORKS.</returns>
        /// <exception cref="TimeoutException">Thrown if method times out.</exception>
        public static Application CreateApplication(bool suppressDialog = false, int timeoutSec = 30)
        {
            int[] years = ReleaseYears();
            if (years.Length == 0)
                throw new Exception("SOLIDWORKS is not installed on this computer.");
            Array.Sort(years);
            int year = years.Last();
            var installationDirectory = GetSOLIDWORKSInstallationDirectory(year);
            if (installationDirectory == null)
                throw new Exception($"Could not find installation directory for SOLIDWORKS. Year = [{year}].");

            string appPath = installationDirectory.FullName;
            var timeout = TimeSpan.FromSeconds(timeoutSec);
            var startTime = DateTime.Now;
            string args = suppressDialog ? "/r" : string.Empty;
            var prc = Process.Start(appPath + "sldworks.exe", args);
            ISldWorks app = null;
            while (app == null)
            {
                if (DateTime.Now - startTime > timeout)
                {
                    throw new TimeoutException();
                }

                app = GetSwAppFromProcess(prc.Id);
            }

            var application = new Application();
            application.SOLIDWORKS = app as SldWorks;

            return application;
               
        }


        [DllImport("ole32.dll")]
        private static extern int CreateBindCtx(uint reserved, out IBindCtx ppbc);

        private static ISldWorks GetSwAppFromProcess(int processId)
        {
            var monikerName = "SolidWorks_PID_" + processId.ToString();

            IBindCtx context = null;
            IRunningObjectTable rot = null;
            IEnumMoniker monikers = null;

            try
            {
                CreateBindCtx(0, out context);

                context.GetRunningObjectTable(out rot);
                rot.EnumRunning(out monikers);

                var moniker = new IMoniker[1];

                while (monikers.Next(1, moniker, IntPtr.Zero) == 0)
                {
                    var curMoniker = moniker.First();

                    string name = null;

                    if (curMoniker != null)
                    {
                        try
                        {
                            curMoniker.GetDisplayName(context, null, out name);
                        }
                        catch (UnauthorizedAccessException)
                        {
                        }
                    }

                    if (string.Equals(monikerName,
                        name, StringComparison.CurrentCultureIgnoreCase))
                    {
                        object app;
                        rot.GetObject(curMoniker, out app);
                        return app as ISldWorks;
                    }
                }
            }
            finally
            {
                if (monikers != null)
                {
                    Marshal.ReleaseComObject(monikers);
                }

                if (rot != null)
                {
                    Marshal.ReleaseComObject(rot);
                }

                if (context != null)
                {
                    Marshal.ReleaseComObject(context);
                }
            }

            return null;
        }


        /// <summary>
        ///  Pointer to the SOLIDWORKS application.
        /// </summary>
        public SldWorks SOLIDWORKS { get; private set; }

        /// <summary>
        /// Converts a release year to the SOLIDWORKS revision number.
        /// </summary>
        /// <param name="releaseYear">Release year.</param>
        /// <returns>SOLIDWORKS revision number</returns>
        /// <remarks>This method produces correct results for revision number from the year 2003 and newer.</remarks>
        public static int ConvertYearToSWRevisionNumber(int releaseYear)
        {
            return releaseYear - 1992;
        }

        /// <summary>
        /// Converts the SOLIDWORKS revision number to a release year.
        /// </summary>
        /// <param name="revNumber">Revision number.</param>
        /// <returns>Release year</returns>
        /// <remarks><ul>
        /// <li>This method produces correct results for revision number from the year 2003 and newer.</li>
        /// <li>Method return -1 if it fails.</li>
        /// </ul></remarks>
        public static int ConvertSWRevisionNumberToYear(string revNumber)
        {
            try
            {
                int spN = int.Parse(revNumber.Split('.').First());
                return 1992 + spN;
            }
            catch (Exception)
            {
                return -1; 
            }
          
        }
        /// <summary>
        /// Converts the SOLIDWORKS revision number to a release year.
        /// </summary>
        /// <param name="revNumber">Revision number.</param>
        /// <returns>Release year</returns>
        /// <remarks><ul>
        /// <li>This method produces correct results for revision number from the year 2003 and newer.</li>
        /// <li>Method return -1 if it fails.</li>
        /// </ul></remarks>
        public static int ConvertSWRevisionNumberToYear(int revNumber)
        {
           
                return 1992 + revNumber;
         

        }
        /// <summary>
        /// Creates a new instance of the Application class.
        /// </summary>
        public Application(int revisionNumber = -1, bool visible = false)
        {
            if (revisionNumber == -1)
                SOLIDWORKS = Activator.CreateInstance(Type.GetTypeFromProgID("SldWorks.Application")) as SldWorks;
            else
                SOLIDWORKS = Activator.CreateInstance(Type.GetTypeFromProgID("SldWorks.Application." + revisionNumber.ToString())) as SldWorks;

            if (SOLIDWORKS != null)
                SOLIDWORKS.Visible = visible;
            else
                throw CADSharpTools.Exceptions.CADSharpToolsPDMException.CreateInstance(CADSharpTools_e.CADSharpToolsPDMExceptionMessages_e.FailedToLaunchSW);
        }

        


        /// <summary>
        /// Dispose of the SOLIDWORKS application.
        /// </summary>
        /// <remarks>This method will close all open documents. Any changes will be lost.</remarks>
        public void Dispose()
        {
            if (SOLIDWORKS != null)
            {
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(SOLIDWORKS);
                SOLIDWORKS = null;
            }
        }

        /// <summary>
        /// Closes all open documents in SOLIDWORKS and exits application.
        /// </summary>
        public void Exit()
        {
            if (SOLIDWORKS != null)
            {
                SOLIDWORKS.CloseAllDocuments(false);
                SOLIDWORKS.ExitApp();
                
            }
        }


        

    }

   
}
