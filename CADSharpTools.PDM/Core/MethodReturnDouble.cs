﻿using Prism.Mvvm;
using System;
using System.ComponentModel;
using System.Reflection;

namespace CADSharpTools.Core
{

    /// <summary>
    /// MethodReturn serves as wrapper for method returns.  
    /// </summary>
    /// <typeparam name="T">Specified return type of the method.</typeparam>
    /// <typeparam name="IConvertible">Enum object.</typeparam>
    /// <remarks>Use this class to capture exceptions throwing by SOLIDWORKS API or SOLIDWORKS PDM API objects. The class has a second generic property that can be used as an <see cref="Enum"/>.</remarks>
    public class MethodReturn<T, IConvertible> : BindableBase
    {
        #region properties
        /// <summary>
        /// Exception object.
        /// </summary>
        private Exception exception;

        public Exception Exception
        {
            get { return exception; }
            set { SetProperty(ref exception, value); }
        }

        /// <summary>
        /// Enum value.
        /// </summary>
        public IConvertible EnumValue { get; private set; }
        /// <summary>
        /// Return object.
        /// </summary>
        public T Value { get; private set; }
        /// <summary>
        /// Property indicating whether the method has successfully finished or not. 
        /// </summary>
        public bool IsError { get; private set; } = false;
        /// <summary>
        /// Error message.
        /// </summary>
        public string Error { get; private set; }
        /// <summary>
        /// Warning message. 
        /// </summary>
        public string Warning { get; private set; }
        #endregion
        #region constructors 


        /// <summary>
        /// Create a new instance of the MethodReturn class.
        /// </summary>
        /// <param name="value">Return object.</param>
        /// <param name="isError">True if successful, false if not.</param>
        /// <param name="_enum">Enum value. All enumerators must be decorated with <see cref="System.ComponentModel.DisplayNameAttribute"/> attribute.</param>
        /// <remarks>The <see cref="IsError"/> property has the value of the description attribute of the _enum argument.</remarks>
        [Obsolete("Replaced by the constructor with an Exception parameter")]
        public MethodReturn(T value, bool isError = false, IConvertible _enum = default(IConvertible))
        {
            this.Value = value;
            this.IsError = isError;
            this.Error = GetEnumDescription(_enum);
            this.EnumValue = _enum;
        }
        /// <summary>
        /// Create a new instance of the MethodReturn class.
        /// </summary>
        /// <param name="value">Return object.</param>
        /// <param name="isError">True if successful, false if not.</param>
        /// <param name="e">Exception</param>
        /// <param name="_enum">Enum value. All enumerators must be decorated with <see cref="System.ComponentModel.DisplayNameAttribute"/> attribute.</param>
        /// <remarks>The <see cref="IsError"/> property has the value of the description attribute of the _enum argument.</remarks>
        public MethodReturn(T value, Exception e, bool isError = true, IConvertible _enum = default(IConvertible))
        {
            this.Exception = e;
            this.Error = e.Message;
            this.Value = value;
            this.IsError = isError;
            this.Error = GetEnumDescription(_enum);
            this.EnumValue = _enum;
        }

        /// <summary>
        /// Create a new instance of the MethodReturn class.
        /// </summary>
        /// <param name="value">Return object.</param>
        /// <param name="warning">Warning message.</param>
        public MethodReturn(T value, string warning)
        {
            this.Value = value;
            this.Warning = warning;

        }

        /// <summary>
        /// Create a new instance of the MethodReturn class.
        /// </summary>
        /// <param name="value">Return object.</param>
        public MethodReturn(T value)
        {
            this.Value = value;
        }



        internal static string GetEnumDescription(IConvertible value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
        #endregion
    }
}
