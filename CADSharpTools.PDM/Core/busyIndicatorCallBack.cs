﻿using Prism.Mvvm;
using System;
using System.ComponentModel;
using System.Threading;

namespace CADSharpTools.Core
{
    /// <summary>
    /// This class is used to report status of time-consuming operation.
    /// </summary>
    /// <example>
    /// This code examples show how to use the busy indicator while getting the references of a large assembly.
    /// <code>
    ///  [STAThread]
    ///static void Main(string[] args)
    ///    {
    ///        // local variables 
    ///        string vaultName = "PDM2019";
    ///        string relativePathName = @"costing\large customer assy\2596-A0000.sldasm";
    ///        // create pdm objects
    ///        var vault = EdmObjectFactory.CreateVaultObject(vaultName);
    ///        var file = EdmObjectFactory.CreateFileObjectFromRelativePath(vault, relativePathName);
    ///        var folder = file.GetFolder();
    ///        string localPath = file.GetLocalPath(folder.ID);
    ///        // create document
    ///        var doc = new Document(file.ID, folder.ID, localPath);
    ///        // define busyIndicatorCallBack 
    ///        var biCallback = new busyIndicatorCallBack();
    ///        // set the progress bar to indeterminate because the number of references is unknown 
    ///        biCallback.IsIndeterminate = true;
    ///        // invoker will invoke time consuming method
    ///        var biInvoker = new busyIndicatorInvoker(biCallback);
    ///        // execute timer-consuming task in a new thread
    ///        biInvoker.InvokeAsync(() => {
    ///            // remark: Use dispatcher to update UI controls on main thread from this context.
    ///            doc.ConstructReferenceTree(vault, biCallback);
    ///        });
    ///        // create busy indicator window
    ///        var biViewModel = new BusyIndicatorViewModel&lt;busyIndicatorCallBack&gt;(biCallback, $"Constructing  reference tree for {file.Name}...");
    ///        biViewModel.ShowView(true);
    ///        // get all references
    ///        var references = doc.FlattenReferences();
    ///        // print results 
    ///        if (biCallback.CancelRequested)
    ///            Console.WriteLine($"This operation has been canceled by user. Found {references.Length} references. {biViewModel.TimeElapsed}");
    ///        else
    ///            Console.WriteLine($"This operation has been complete. Found {references.Length} references.  {biViewModel.TimeElapsed}");
    ///        Console.ReadLine();
    ///    }
    /// </code>
    /// Preview of the busy indicator view in Windows 10:<br/>
    /// <img src="../Images/busyindicatorview.PNG"></img><br/>
    /// Result:<br/>
    /// <img src="../Images/busyindicatorresult.png"></img>
    /// </example>
    public class busyIndicatorCallBack : BindableBase , ctpICallback
    {
        /// <summary>
        /// Announces the operation is finished and fires the <see cref="Finished"/> event.
        /// </summary>
        public virtual void Finish()
        {
            if (Finished != null)
            {
                Finished(message, new busyIndicatorCallBackFinishEventArgs() { State = busyIndicatorCallBackState_e.Finished });
            }
        }

        /// <summary>
        /// Creates a new instance of the busyIndicatorCallBack class.
        /// </summary>
        public busyIndicatorCallBack()
        {

        }

        /// <summary>
        /// Fires when the busy indicator action has been ended.
        /// </summary>
        public virtual event EventHandler<busyIndicatorCallBackFinishEventArgs> Finished;

        private  bool isIndeterminate;
        /// <summary>
        /// True is the operation is not deterministic, false is not. 
        /// </summary>
        public virtual bool IsIndeterminate
        {
            get { return isIndeterminate; }
            set { SetProperty(ref isIndeterminate, value); }
        }

        /// <summary>
        /// Returns whether a cancel question has been initiated by the user.
        /// </summary>
        public bool CancelRequested { get; private set; } 


        /// <summary>
        /// Cancels operation.
        /// </summary>
        public virtual void Cancel()
        {
            if (Finished != null)
            {
                CancelRequested = true;
                Finished(message, new busyIndicatorCallBackFinishEventArgs() { State = busyIndicatorCallBackState_e.Canceled });
            }
        }

        private int current;
        /// <summary>
        /// Index of the current element.
        /// </summary>
        public virtual int Current
        {
            get { return current; }
            set { SetProperty(ref current, value); }
        }

        private int count;

        /// <summary>
        /// Count of the total elements.
        /// </summary>
        public virtual int Count
        {
            get { return count; }
            set { SetProperty(ref count, value); }
        }
        private string message;
        /// <summary>
        /// Message to display to the user.
        /// </summary>
        public virtual string Message
        {
            get { return message; }
            set
            {
                SetProperty(ref message, value);
            }
        }

    }

    /// <summary>
    /// CADSharpTools.PDM callback.
    /// </summary>
    public interface ctpICallback
    {
        /// <summary>
        /// Returns whether the user has initiated a cancel request.
        /// </summary>
        bool CancelRequested { get;  }
        /// <summary>
        /// Gets or sets a message to display to the user by callback to indicate status during a time-consuming task.
        /// </summary>
        string Message { get; set; }
        /// <summary>
        /// Cancels a time-consuming task.
        /// </summary>
        void Cancel();
        /// <summary>
        /// Finishes a time-consuming task.
        /// </summary>
        void Finish();
    }

    

    /// <summary>
    /// State of the callback.
    /// </summary>
    public enum busyIndicatorCallBackState_e
    {
        /// <summary>
        /// Running.
        /// </summary>
        Running,
        /// <summary>
        /// Finished.
        /// </summary>
        Finished,
        /// <summary>
        /// Canceled.
        /// </summary>
        Canceled,
    }

    /// <summary>
    /// Arguments of the <see cref="busyIndicatorCallBack.Finished"/> event.
    /// </summary>
    public class busyIndicatorCallBackFinishEventArgs : EventArgs
    {
        /// <summary>
        /// State.
        /// </summary>
        public busyIndicatorCallBackState_e State { get; set; }
    }

    /// <summary>
    /// Invokes a time-consuming method. 
    /// </summary>
    public class busyIndicatorInvoker
    {
        /// <summary>
        /// Callback.
        /// </summary>
        public busyIndicatorCallBack Callback { get; private set; }
        /// <summary>
        /// Creates a new instance of the busyIndicatorInvoker class.
        /// </summary>
        public busyIndicatorInvoker(busyIndicatorCallBack callBack)
        {
            this.Callback = callBack;
        }

        private busyIndicatorInvoker()
        {

        }

        /// <summary>
        /// Invokes the action in a new thread.
        /// </summary>
        /// <param name="action">Time consuming action.</param>
        /// <remarks>Your action must not interact with the UI.</remarks>
        public void InvokeAsync(Action action)
        {
            var thread = new Thread(new ThreadStart(() => {
                action();
                this.Callback.Finish();
            }));
            thread.Start();
        }
    }
 }
