﻿using System;

namespace CADSharpTools.Core
{

    /// <summary>
    /// MethodReturn serves as wrapper for method returns.  
    /// </summary>
    /// <typeparam name="T">Specified return type of the method.</typeparam>
    /// <remarks>Use this class to capture exceptions throwing by SOLIDWORKS API or SOLIDWORKS PDM API objects.</remarks>
    public class MethodReturn<T>
    {
        #region properties
        /// <summary>
        /// Error message.
        /// </summary>
        public string Error { get { return this.Exception.Message; } }

        /// <summary>
        /// Exception object.
        /// </summary>
        public Exception Exception { get; private set; }
        /// <summary>
        /// Time stamp. 
        /// </summary>
        public DateTime TimeStamp { get; private set; } = DateTime.Now;

        /// <summary>
        /// The returned object. 
        /// </summary>
        public T Value { get; private set; }
        /// <summary>
        /// Property indicating whether the method has successfully completed or not. 
        /// </summary>
        public bool IsError { get; private set; }
        /// <summary>
        /// The warning message. 
        /// </summary>       
        public string Warning { get; private set; }
        #endregion
        #region constructors 

        /// <summary>
        /// Creates a new instance of the MethodReturn class.
        /// </summary>
        /// <param name="value">Returned object.</param>
        /// <param name="e">Exception object.</param>
        /// <param name="isError">True if the method has successfully completed, false if not.</param>
        public MethodReturn(T value, Exception e, bool isError = true)
        {
            this.Value = value;
            this.IsError = isError;
            this.Exception = e;
        }

        /// <summary>
        /// Creates a new instance of the MethodReturn class.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <param name="value">Returned object.</param>
        /// <param name="e">Exception object.</param>
        /// <param name="isError">True if the method has successfully completed, false if not.</param>
        public MethodReturn(T value,  bool isError = false, string error = "")
        {
            this.Value = value;
            this.IsError = isError;
            this.Exception = new Exception(error);
 
        }

        /// <summary>
        /// Create a new instance of the MethodReturn class.
        /// </summary>
        /// <param name="value">Returned object.</param>
        /// <param name="warning">Warning message.</param>
        /// <remarks>Use this constructor in the case that the method returns a valid object with warnings. SOLIDWORKS opening a part document with warning message is an example.</remarks>
        public MethodReturn(T value, string warning)
        {
            this.Value = value;
            this.Warning = warning;
        }
        #endregion

    }

    /// <summary>
    /// MethodReturn serves as wrapper for methods that return void. 
    /// </summary>
    /// <remarks>Use this class to capture exceptions throwing by SOLIDWORKS API or SOLIDWORKS PDM API objects.</remarks>
    public class MethodReturn
    {    
        
        /// <summary>
             /// Error message.
             /// </summary>
             /// <remarks>This returns the Message property from <see cref="MethodReturn{T}.Exception"/>.</remarks>
        public string Error { get { return this.Exception.Message; } }

        /// <summary>
        /// Exception object.
        /// </summary>
        public Exception Exception { get; private set; } = new Exception();
        /// <summary>
        /// Time stamp. 
        /// </summary>
        public DateTime TimeStamp { get; private set; } = DateTime.Now;

       
        /// <summary>
        /// Property indicating whether the method has successfully completed or not. 
        /// </summary>
        public bool IsError { get; private set; }
        /// <summary>
        /// The warning message. 
        /// </summary>       
        public string Warning { get; private set; }

        #region constructors 
        /// <summary>
        /// Creates a new instance of the MethodReturn class.
        /// </summary>
        public MethodReturn()
        {

        }

        /// <summary>
        /// Creates a new instance of the MethodReturn class.
        /// </summary>
        /// <param name="e">Exception object.</param>
        /// <param name="isError">True if the method has successfully completed, false if not.</param>
        public MethodReturn(Exception e, bool isError = true)
        {
          
            this.IsError = isError;
            this.Exception = e;
        }

        /// <summary>
        /// Creates a new instance of the MethodReturn class.
        /// </summary>
        /// <param name="error">Error object.</param>
        /// <param name="isError">True if the method has successfully completed, false if not.</param>
        public MethodReturn(string error, bool isError = true)
        {

            this.IsError = isError;
            this.Exception = new Exception(error);
        }

        /// <summary>
        /// Create a new instance of the MethodReturn class.
        /// </summary>
        /// <param name="warning">Warning message.</param>
        /// <remarks>Use this constructor in the case that the method returns a valid object with warnings. SOLIDWORKS opening a part document with warning messages (Rebuild errors) is an example.</remarks>
        public MethodReturn(string warning)
        {
            this.Warning = warning;
        }

      #endregion
    }
}
