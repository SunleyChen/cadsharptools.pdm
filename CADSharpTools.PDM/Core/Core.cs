﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CADSharpTools.Core
{
    /// <summary>
    /// Extension class provides miscellaneous methods.
    /// </summary>
    public static class Extension
    {
        internal static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        /// <summary>
        /// Returns the location of the specified assembly.
        /// </summary>
        /// <param name="assembly">Assembly object.</param>
        /// <returns>DirectoryInfo object.</returns>
        public static DirectoryInfo GetAssemblyLocation(this Assembly assembly)
        {
            return (new FileInfo(new Uri(assembly.Location).LocalPath).Directory);
        }


        /// <summary>
        /// Returns the product name and the file version of the specified assembly in the following fashion: Name MAJOR.MINOR.PATCH.REVISION.
        /// REVISION is omitted if not specified.
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public static string GetAssemblyNameAndVersion(this Assembly assembly)
        {
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            return $"{fvi.ProductName} {version}";
        }

        /// <summary>
        /// Returns the evaluated directory from a directory that contains the specified folder alias.
        /// </summary>
        /// <param name="directory">Directory.</param>
        /// <param name="specialFolder">Special folder alias.</param>
        /// <returns>Evaluated directory</returns>
        /// <remarks>The specialFolder parameter is not flag-sensitive.</remarks>
        public static MethodReturn<DirectoryInfo> EvaluateDirectory(string directory, CADSharpTools_e.SpecialFolder specialFolder)
        {
            string evaluatedDirectory = string.Empty;
            try
            {
                switch (specialFolder)
                {
                    case CADSharpTools_e.SpecialFolder.Unspecified:
                        return new MethodReturn<DirectoryInfo>(new DirectoryInfo(directory));
                    case CADSharpTools_e.SpecialFolder.Desktop:
                        {
                            string specialFolderAlias = GetEnumDescription(specialFolder);
                            evaluatedDirectory = directory.Replace(specialFolderAlias, System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                            return new MethodReturn<DirectoryInfo>(new DirectoryInfo(evaluatedDirectory));
                        }
                    case CADSharpTools_e.SpecialFolder.ApplicationData:
                        {
                            string specialFolderAlias = GetEnumDescription(specialFolder);
                            evaluatedDirectory = directory.Replace(specialFolderAlias, System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
                            return new MethodReturn<DirectoryInfo>(new DirectoryInfo(evaluatedDirectory));

                        }
                    default:
                        return new MethodReturn<DirectoryInfo>(new DirectoryInfo(directory));
                }
            }
            catch (Exception e)
            {
                string message = $"Failed to evaluate directory. Directory [{directory}] - Alias [{GetEnumDescription(specialFolder)}] - Evaluated Directory [{evaluatedDirectory}]  - Exception message : [{e.Message}] ";
                return new MethodReturn<DirectoryInfo>(null, new Exception(message), true);
            }

        }

        /// <summary>
        /// Formats fractional minutes in the <i>hh:mm:ss</i> format.
        /// </summary>
        /// <param name="timeInMins">time in minutes.</param>
        /// <returns></returns>
        public static string FormatTimeFromMinutes(this double timeInMins)
        {
            double timeInSeconds = timeInMins * 60;
            string formattedTime = $"{TimeSpan.FromSeconds(timeInSeconds).Hours.ToString("D2")}:{TimeSpan.FromSeconds(timeInSeconds).Minutes.ToString("D2")}:{TimeSpan.FromSeconds(timeInSeconds).Seconds.ToString("D2")}";
            return formattedTime;
        }

        /// <summary>
        /// Opens the CommonOpenFileDialog window to browse to a file.
        /// </summary>
        /// <param name="startupDirectory">startup directory.</param>
        /// <param name="extension">file system. Must include the dot.</param>
        /// <param name="fileTypeName">File type name.</param>
        /// <param name="multiSelect">Allow multiple selection.</param>
        /// <returns>Array of pathnames of the selected files. An array of String.Empty if no file was selected.</returns>
        public static string[] BrowseToFile(string startupDirectory, string extension, string fileTypeName, bool multiSelect = false)
        {

            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.Multiselect = true;
            dialog.Filters.Add(new CommonFileDialogFilter(fileTypeName, extension));
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                if (multiSelect == false)
                {
                    string fileName = dialog.FileName;
                    if (System.IO.File.Exists(fileName))
                    {
                        return new string[] { fileName };
                    }
                }
                else {
                    var FileNames = dialog.FileNames;
                    if (FileNames != null || FileNames.Count() > 0)
                    {
                        return FileNames.ToArray();
                    }
                }
            }
            return new string[] { string.Empty };
        }
        /// <summary>
        /// Opens the CommonOpenFileDialog window to browse to a folder.
        /// </summary>
        /// <param name="startupDirectory">startup directory.</param>
        /// <param name="multiSelect">Allow multiple selection.</param>
        /// <returns>Array of selected directory.</returns>
        public static string[] BrowseToFolder(string startupDirectory, bool multiSelect = false)
        {

            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            dialog.Multiselect = multiSelect;


            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                if (multiSelect == false)
                {
                    string Directory = dialog.FileName;
                    if (System.IO.Directory.Exists(Directory))
                    {
                        return new string[] { Directory };
                    }

                }
                else
                {
                    var FileNames = dialog.FileNames;
                    if (FileNames != null || FileNames.Count() > 0)
                    {
                        return FileNames.ToArray();
                    }
                }

            }
            return new string[] { string.Empty };
        }

        /// <summary>
        /// Returns an array of pathnames of all files with matching extension. Hidden files and filenames that start with a tilde are ignored. 
        /// </summary>
        /// <param name="directory">Directory to look for files in.</param>
        /// <param name="extension">file extension without the dot.</param>
        /// <remarks> 
        /// <ul>
        ///<li>Do not use this method on directories that contain a large number of files.</li>
        /// <li>A temporary file is created on your disk whenever a file is read by SOLIDWORKS. The temporary filename starts with tilde and is ignored by this method.</li>
        ///</ul>
        ///</remarks>
        /// <returns>Array of files pathnames.</returns>
        public static string[] GetFilesFromDirectory(string directory, string extension)
        {


            DirectoryInfo dirInfo = new DirectoryInfo(directory);
            FileInfo[] fileInfos = dirInfo.GetFiles("*.*" + extension, SearchOption.AllDirectories);
            var cleanFilesList = new List<string>();
            foreach (var fileInfo in fileInfos)
            {
                if (fileInfo.Attributes != FileAttributes.Hidden && fileInfo.Name.StartsWith("~") == false)
                {
                    cleanFilesList.Add(fileInfo.FullName);
                }
            }

            return cleanFilesList.ToArray();
        }

        /// <summary>
        /// Returns a Task of an array of pathnames of all files with matching extension. Hidden files and filenames that start with a tilde are ignored. 
        /// </summary>
        /// <param name="directory">Directory to look for files in.</param>
        /// <param name="extension">file extension without the dot.</param>
        /// <remarks> 
        /// <ul>
        ///<li>Do not use this method on directories that contain a large number of files.</li>
        /// <li>A temporary file is created on your disk whenever a file is read by SOLIDWORKS. The temporary filename starts with tilde and is ignored by this method.</li>
        ///</ul>
        ///</remarks>
        /// <returns>A task of array of files pathnames.</returns>
        public static Task<string[]> GetFilesFromDirectoryAsync(string directory, string extension)
        {
            return Task.Run(() => {
                return GetFilesFromDirectory(directory, extension);
            });

        }
    }

}


