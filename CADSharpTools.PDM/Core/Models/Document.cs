﻿using CADSharpTools.PDM;
using EPDM.Interop.epdm;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CADSharpTools.Core.Models
{
    /// <summary>
    /// Base class for PDM documents. 
    /// </summary>
    public class Document : BindableBase
    {
        /// <summary>
        /// Gets the name of the document.
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Returns the local version number of this document. Default value is -1.
        /// </summary>
        public int LocalVersion { get; private set; } = -1;

        /// <summary>
        /// Returns whether the document has any references.
        /// </summary>
        public bool HasContainsReference { get { return ContainsReferences.Length > 0 ? true : false; } }
        string localPath = string.Empty; 
        /// <summary>
        /// Gets the local path of the document. 
        /// </summary>
        public string LocalPath { get { return localPath; } private set { localPath = value; }  }
        private int id;
        /// <summary>
        /// PDM ID of the document. 
        /// </summary>
        public int ID
        {
            get { return id; }
            private set { SetProperty(ref id, value); }
        }
        private bool needsRegeneration;
        /// <summary>
        /// Gets or sets whether this document requires to rebuild in SOLIDWORKS.
        /// </summary>
        public bool NeedsRegeneration
        {
            get { return needsRegeneration; }
            set {  SetProperty(ref needsRegeneration, value); }
        }
        private int folderid;
        /// <summary>
        /// Folder ID. 
        /// </summary>
        public int FolderID
        {
            get { return folderid; }
            private set { SetProperty(ref folderid, value); }
        }
        /// <summary>
        /// Gets an array of level referenced documents. 
        /// </summary>
        public Document[] ContainsReferences { get; private set; } = new Document[] { };
        /// <summary>
        /// Gets an array of all parents.
        /// </summary>
        public Document[] WhereUsedReferences { get; private set; } = new Document[] { };

        /// <summary>
        /// Creates a new instance of the document class. 
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="folderID">ID of the parent folder</param>
        /// <param name="localPath">Local path</param>
        public Document(int id, int folderID, string localPath = "")
        {
            this.ID = id;
            this.FolderID = folderID; 
            this.LocalPath = localPath;
            this.Name = System.IO.Path.GetFileName(localPath);
        }
        /// <summary>
        /// Adds a referenced documents 
        /// </summary>
        /// <param name="document">Document</param>
         void AddContainsReference(Document document)
        {
            var internalLst = ContainsReferences.ToList();

            internalLst.Add(document);

            ContainsReferences =  internalLst.ToArray();

        }


        /// <summary>
        /// Adds a referenced documents 
        /// </summary>
        /// <param name="document">Document</param>
         void AddWhereUsedReference(Document document)
        {
            var internalLst = WhereUsedReferences.ToList();

            internalLst.Add(document);

            WhereUsedReferences = internalLst.ToArray();

        }

        /// <summary>
        /// Removes a reference. 
        /// </summary>
        /// <param name="document">Document</param>
         void RemoveContainsReference(Document document)
        {
            var internalLst = ContainsReferences.ToList();

            internalLst.Remove(document);

            ContainsReferences = internalLst.ToArray();
        }

        /// <summary>
        /// Constructs the document contains reference tree using the vault object.
        /// </summary>
        /// <param name="vault">Vault object.</param>
        /// <param name="callback">Callback</param>
        /// <remarks>This method clears up <see cref="ContainsReferences"/>.</remarks>
        /// <exception cref="Exception">Document not found.</exception>
        public void ConstructContainsReferenceTree(IEdmVault5 vault, ctpICallback callback = null)
        {
            
            var pdmDocumentObj = vault.GetObject(EdmObjectType.EdmObject_File, id) as IEdmFile5;
            if (pdmDocumentObj != null)
            {
                var getReferences = default(Action<IEdmReference5, string, Document>);
                getReferences = new Action<IEdmReference5, string, Document>(
                    (IEdmReference5 reference, string filePath, Document parentDocument)
                    =>

                    {
                    if (callback != null)
                        if (callback.CancelRequested)
                        {
                            callback.Cancel();
                            return;
                        }

                    bool Top = false;
                    if (reference == null)
                    {
                        parentDocument = this;
                        //This is the first time this function is called for this 
                        //reference tree; i.e., this is the root
                        Top = true;
                        IEdmFile5 File = null;
                        IEdmFolder5 ParentFolder = null;
                        File = vault.GetFileFromPath(filePath, out ParentFolder);
                        var NeedsRegeneration = File.NeedsRegeneration2(0, ParentFolder.ID);
                        parentDocument.NeedsRegeneration = NeedsRegeneration;
                            parentDocument.LocalVersion = File.GetLocalVersionNo(ParentFolder.ID);

                            //Get the reference tree for this file
                            reference = File.GetReferenceTree(ParentFolder.ID);
 
                        getReferences(reference, "", parentDocument);

                    }
                    else
                    {
                        //Execute this code when this function is called recursively; 
                        //i.e., this is not the top-level IEdmReference in the tree
                        //Recursively traverse the references
                        IEdmPos5 pos = default(IEdmPos5);
                        IEdmReference9 Reference2 = (IEdmReference9)reference;
                            pos = Reference2.GetFirstChildPosition3("A", Top, true, (int)EdmRefFlags.EdmRef_File, "", 0);
                            if (callback != null)
                                if (callback.CancelRequested)
                                {
                                    callback.Cancel();
                                    callback.Message = "This operation has been canceled.";
                                    return;
                                }
                            IEdmReference5 @ref = default(IEdmReference5);
                            while ((!pos.IsNull))
                            {
                                @ref = reference.GetNextChild(pos);
                                if (@ref.FileID > 0)
                                {
                                    // create document 
                                    var childDocument = new Document(@ref.FileID, @ref.FolderID, @ref.FoundPath);
                                    childDocument.LocalVersion = @ref.VersionLocal;
                                    // add as a child to parent document 
                                    callback.Message = $"Found reference {@ref.Name}...";
                                    parentDocument.AddContainsReference(childDocument);
                                // process next level 
                                getReferences(@ref, childDocument.LocalPath, childDocument);
                                }
                            }
                        }
                    }
                    );

                getReferences(null, LocalPath,this);
            }
            else 
            throw new Exception("Document not found.");
        }

        /// <summary>
        /// Constructs the document where used reference tree using the vault object.
        /// </summary>
        /// <param name="vault">Vault object.</param>
        /// <param name="callback">Callback</param>
        /// <remarks>This method clears up <see cref="WhereUsedReferences"/>.</remarks>
        /// <exception cref="Exception">Document not found.</exception>
        public void ConstructWhereUsedReferenceTree(IEdmVault5 vault, ctpICallback callback = null)
        {
           
            var pdmDocumentObj = vault.GetObject(EdmObjectType.EdmObject_File, id) as IEdmFile5;
            if (pdmDocumentObj != null)
            {
                var getReferences = default(Action<IEdmReference5, string, Document>);
                getReferences = new Action<IEdmReference5, string, Document>(
                    (IEdmReference5 reference, string filePath, Document parentDocument)
                    =>
                    {
                        if (callback != null)
                            if (callback.CancelRequested)
                            {
                                callback.Cancel();
                                return;
                            }
                        bool Top = false;
                        if (reference == null)
                        {
                            parentDocument = this;
                            //This is the first time this function is called for this 
                            //reference tree; i.e., this is the root
                            Top = true;
                            IEdmFile5 File = null;
                            IEdmFolder5 ParentFolder = null;
                            File = vault.GetFileFromPath(filePath, out ParentFolder);
                            parentDocument.LocalVersion = File.GetLocalVersionNo(ParentFolder.ID);

                            //Get the reference tree for this file
                            reference = File.GetReferenceTree(ParentFolder.ID);

                            getReferences(reference, "", parentDocument);

                        }
                        else
                        {
                            //Execute this code when this function is called recursively; 
                            //i.e., this is not the top-level IEdmReference in the tree
                            //Recursively traverse the references
                            IEdmPos5 pos = default(IEdmPos5);
                            IEdmReference9 Reference2 = (IEdmReference9)reference;
                            pos = Reference2.GetFirstParentPosition2(0, true, (int)EdmRefFlags.EdmRef_File);
                            if (callback != null)
                                if (callback.CancelRequested)
                                {
                                    callback.Cancel();
                                    callback.Message = "This operation has been canceled.";
                                    return;
                                }
                            IEdmReference5 @ref = default(IEdmReference5);
                            while ((!pos.IsNull))
                            {
                                @ref = reference.GetNextChild(pos);
                                if (@ref.FileID > 0)
                                {
                                    // create document 
                                    var childDocument = new Document(@ref.FileID, @ref.FolderID, @ref.FoundPath);
                                    childDocument.LocalVersion = @ref.VersionLocal;
                                    // add as a child to parent document 
                                    callback.Message = $"Found reference {@ref.Name}...";
                                    parentDocument.AddWhereUsedReference(childDocument);
                                    // process next level 
                                    getReferences(@ref, childDocument.LocalPath, childDocument);
                                }
                            }
                        }
                    }
                    );

                getReferences(null, LocalPath, this);
            }
            else
                throw new Exception("Document not found.");
        }

        /// <summary>
        /// Returns an array of all contains referenced documents.
        /// </summary>
        /// <returns></returns>
        public Document[] FlattenContainsReferences()
        {
            var interLst = new List<Document>();
            var flattenFunc = default(Action<Document>);     
            flattenFunc =  new Action<Document>((x) => 
            {
            Array.ForEach(x.ContainsReferences, ((y) =>
             {
                 interLst.Add(y);
                 flattenFunc(y);
             }  
                ));
            });
            flattenFunc(this);
            return interLst.ToArray();
        }
        /// <summary>
        /// Returns an array of all where used referenced documents.
        /// </summary>
        /// <returns></returns>
        public Document[] FlattenWhereUsedReferences()
        {
            var interLst = new List<Document>();
            var flattenFunc = default(Action<Document>);
            flattenFunc = new Action<Document>((x) =>
            {
                Array.ForEach(x.WhereUsedReferences, ((y) =>
                {
                    interLst.Add(y);
                    flattenFunc(y);
                }
                    ));
            });
            flattenFunc(this);
            return interLst.ToArray();
        }

        /// <summary>
        /// Returns a dictionary of documents and their level in the order which they need to be rebuilt.
        /// </summary>
        /// <returns>Dictionary</returns>
        /// <remarks><ul>
        /// <li>
        /// The first element in the dictionary is the first document that needs to rebuild and so on. Check <see cref="NeedsRegeneration"/> to check if the document needs to rebuild or not. 
        /// </li>
        /// <li>
        /// Call <see cref="ConstructContainsReferenceTree(IEdmVault5, ctpICallback)"/> before calling this method to build the reference tree of this document.
        /// </li>
        /// </ul></remarks>
        public Dictionary<Document, int> GetRebuildOrder(ctpICallback callback = null)
        {
            var rebuildOrderDictionary = new Dictionary<Document, int>();
            var list = new List<Document>();
            var levels = new List<Level>();


            if (callback != null)
                callback.Message = $"Getting references build order. Please wait...";
            // add level 0
            var rootLevel = new Level() { Number = 0 };
            rootLevel.References.Add(this);
            levels.Add(rootLevel);
            int level = 1;

            var traverseLevels = default(Action<Level>);
            traverseLevels = (Level Level) => 
            {

                if (Level == null)
                {
                    var References = this.ContainsReferences;
                    var topLevel = new Level() { Number = level};
                    topLevel.References.AddRange(References);
                    levels.Add(topLevel);
                    level++;
                    traverseLevels(topLevel);
                }
                else
                {
                    if (callback != null)
                        if (callback.CancelRequested)
                        return;


                    var nLevel = new Level() { Number = level };
                    foreach (var Reference in Level.References)
                        nLevel.References.AddRange(Reference.ContainsReferences);

                    if (nLevel.References.Count > 0)
                    {
                        levels.Add(nLevel);
                        level++;
                        traverseLevels(nLevel);

                    }

                }

                
        };

            if (callback != null)
            if (callback.CancelRequested)
                return rebuildOrderDictionary;




            
            traverseLevels(null);

             
            // reverse levels
            levels.Reverse();

           
            levels.ForEach(x =>
            {
                foreach (var Reference in x.References)
                {
                    if (callback != null)
                        callback.Message = $"Processing {System.IO.Path.GetFileName(Reference.LocalPath)}...";
                    if (callback != null)
                        if (callback.CancelRequested)
                        return;
                    var retGet = rebuildOrderDictionary.Keys.ToList().Exists(y => Reference.ID == y.ID);
                    // reference does not exist 
                    if (retGet == false)
                    {
                        rebuildOrderDictionary.Add(Reference, x.Number);
                    }
                }
            });
            return rebuildOrderDictionary;
        } 




        class Level
        {
            public int Number { get; set; }
            public List<Document> References { get; set; } = new List<Document>();

        }


    }

    

   
    
    /// <summary>
    /// Document types.
    /// </summary>
    [Flags]
    public enum DocumentType_e
    {
        /// <summary>
        /// Part.
        /// </summary>
        Part,
        /// <summary>
        /// Assembly
        /// </summary>
        Assembly,
        /// <summary>
        /// Drawing
        /// </summary>
        Drawing,
    }

    
}
