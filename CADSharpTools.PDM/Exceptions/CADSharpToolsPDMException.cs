﻿using CADSharpTools_e;
using System;
using CADSharpTools.Core;
namespace CADSharpTools.Exceptions
{
    /// <summary>
    /// Exception class for the CADSharpTools.PDM library.
    /// </summary>
    /// <remarks>This class cannot be instantiated.</remarks>
    /// <example>
    /// This console example shows how to handle a CADSharpToolsPDMException exception. The exception is thrown by <see cref="CADSharpTools.PDM.Extension.SetDataCardVariable(EPDM.Interop.epdm.IEdmFile5, string, string, object)"/> because the specified file is checked in.
    /// <code>
    ///using EPDM.Interop.epdm;
    ///using System;
    ///using CADSharpTools.PDM;
    ///namespace Test
    ///{
    ///    class Program
    ///    {
    ///        public enum EdmLoginFlags
    ///        {
    ///            Nothing,
    ///            WebClient
    ///        }
    ///        [STAThread]
    ///        static void Main(string[] args)
    ///        {
    ///            try
    ///            { 
    ///            //create vault object 
    ///            var swPDMVault = new EdmVault5();
    ///            var swPDMVaultEx = swPDMVault as IEdmVault13;
    ///            //required per PDM EULA in out-of-process applications
    ///            swPDMVaultEx.LoginEx("Admin", "****", "CADSharp", (int)EdmLoginFlags.Nothing);
    ///             IEdmFolder5 folder = default(IEdmFolder5);
    ///             var file = swPDMVaultEx.GetFileFromPath(@"C:\CADSharp\Vault\CADSharp\Part1.sldprt", out folder);
    ///            // attempt to set a variable
    ///            file.SetDataCardVariable("PartNumber", "@", "15");
    ///            }
    ///            catch (CADSharpTools.Commons.CADSharpToolsPDMException ex)
    ///            {
    ///                Console.WriteLine("CADSharpTools exception caught: {0} [enum: {1} value: {2}]",ex.Message, ex.MessageEnum.ToString(),(int)ex.MessageEnum);
    ///            }
    ///            catch (Exception e)
    ///            {
    ///                Console.WriteLine("General exception caught: " + e.Message);
    ///            }
    ///            finally
    ///            {
    ///                Console.ReadKey();
    ///            }
    ///            /* result:
    ///             * CADSharpTools exception caught: File must be checked out. [enum: FileMustBeCheckedOut value: 1]
    ///             */
    ///        }
    ///    }
    ///}
    /// </code>
    /// </example>
    public class CADSharpToolsPDMException : Exception
        {
            /// <summary>
            /// Gets the CADSharpTools exception enumerator.
            /// </summary>
            /// <example>
            /// This console example shows how to handle a CADSharpToolsPDMException exception. The exception is thrown by <see cref="CADSharpTools.PDM.Extension.SetDataCardVariable(EPDM.Interop.epdm.IEdmFile5, string, string, object)"/> because the specified file is checked in.   
            ///<code>
            ///using EPDM.Interop.epdm;
            ///using System;
            ///using CADSharpTools.PDM;
            ///namespace Test
            ///{
            ///    class Program
            ///    {
            ///        public enum EdmLoginFlags
            ///        {
            ///            Nothing,
            ///            WebClient
            ///        }
            ///        [STAThread]
            ///        static void Main(string[] args)
            ///        {
            ///            try
            ///            { 
            ///            //create vault object 
            ///            var swPDMVault = new EdmVault5();
            ///            var swPDMVaultEx = swPDMVault as IEdmVault13;
            ///            //required per PDM EULA in out-of-process applications
            ///            swPDMVaultEx.LoginEx("Admin", "****", "CADSharp", (int)EdmLoginFlags.Nothing);
            ///             IEdmFolder5 folder = default(IEdmFolder5);
            ///             var file = swPDMVaultEx.GetFileFromPath(@"C:\CADSharp\Vault\CADSharp\Part1.sldprt", out folder);
            ///            // attempt to set a variable
            ///            file.SetDataCardVariable("PartNumber", "@", "15");
            ///            }
            ///            catch (CADSharpTools.Commons.CADSharpToolsPDMException ex)
            ///            {
            ///                Console.WriteLine("CADSharpTools exception caught: {0} [enum: {1} value: {2}]",ex.Message, ex.MessageEnum.ToString(),(int)ex.MessageEnum);
            ///            }
            ///            catch (Exception e)
            ///            {
            ///                Console.WriteLine("General exception caught: " + e.Message);
            ///            }
            ///            finally
            ///            {
            ///                Console.ReadKey();
            ///            }
            ///            /* result:
            ///             * CADSharpTools exception caught: File must be checked out. [enum: FileMustBeCheckedOut value: 1]
            ///             */
            ///        }
            ///    }
            ///}
            /// </code>
            /// </example>
            public CADSharpToolsPDMExceptionMessages_e MessageEnum { get; private set; }

            private CADSharpToolsPDMException(string message) : base(message)
            {

            }

            /// <summary>
            /// Creates a new instance of the CADSharpToolsPDMException class.
            /// </summary>
            /// <param name="message">Message</param>
            /// <returns>CADSharpToolsPDMException</returns>
            internal static CADSharpToolsPDMException CreateInstance(CADSharpToolsPDMExceptionMessages_e message)
            {
                string exceptionMessage = Extension.GetEnumDescription(message);
                var exception = new CADSharpToolsPDMException(exceptionMessage);
                exception.MessageEnum = message;
                return exception;
            }
        }
}
