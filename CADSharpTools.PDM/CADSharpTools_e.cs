﻿using CADSharpTools.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CADSharpTools_e
{
    /// <summary>
    /// Alias for environment specific folders. 
    /// </summary>
    public enum SpecialFolder
    {
     
        /// <summary>
        /// Unspecified
        /// </summary>
        [Description("")]
        Unspecified,

        /// <summary>
        /// The alias is &lt;Desktop&gt;
        /// </summary>
        [Description("<Desktop>")]
        Desktop,
        /// <summary>
        /// The alias is &lt;ApplicationData&gt;
        /// </summary>
        [Description("<ApplicationData>")]
        ApplicationData
 


    }

    /// <summary>
    /// <see cref="CADSharpTools.PDM.Extension.GetDataCardVariable(EPDM.Interop.epdm.IEdmFile5, string, string)">'s enum return value.</see>
    /// </summary>
    /// <remarks>You can get the enumerator description from <see cref="CADSharpTools.Core.MethodReturn{T, IConvertible}.Error"/> property.</remarks>
    public enum GetDataCardVariableReturn_e
    {
        /// <summary>
        /// Success.
        /// </summary>
        [Description("Success.")]
        Success,
        /// <summary>
        /// The data card variable is not found.
        /// </summary>
        [Description("The variable was not found.")]
        VariableNotFound

    }

    /// <summary>
    /// Contains all of the CADSharpToolsPDMException's message.
    /// </summary>
    /// <remarks>
    /// The enumerators' descriptions are stored in the <see cref="Exception.Message"/> property of <see cref="CADSharpTools.Core.CADSharpToolsPDMException"/>.
    /// </remarks>
    public enum CADSharpToolsPDMExceptionMessages_e
    {

        /// <summary>
        /// File must be checked out.
        /// </summary>
        [Description("File must be checked out.")]
        FileMustBeCheckedOut = 1,
        /// <summary>
        /// Unable to get the parent folder of the file object.
        /// </summary>
        [Description("Unable to get the parent folder of the file object.")]
        UnableToGetFolderOfFile,
        /// <summary>
        /// SQLHandler connection string property is null or empty.
        /// </summary>
        [Description("SQLHandler connection string property is null or empty.")]
        SQLHandlerConnectionStringNull,
        /// <summary>
        /// File has an unknown extension.
        /// </summary>
        [Description("File has an unknown extension.")]
        UnknownExtension,
        /// <summary>
        /// Only assembly files are supported by this method.
        /// </summary>
        [Description("Only assembly files are supported by this method.")]
        AssemblyOnly,

        /// <summary>
        /// No SOLIDWORKS material library was found.
        /// </summary>
        [Description("No SOLIDWORKS material library was found.")]
        MaterialDatabaseCountZero,
        /// <summary>
        /// Undefined measurement unit or incomplete implementation.
        /// </summary>
        [Description("Undefined measurement unit or incomplete implementation.")]
        UndefinedMeasurement,
        /// <summary>
        /// The specified file has no local copy.
        /// </summary>
        [Description("The specified file has no local copy.")]
        NoLocalCopy,

        /// <summary>
        /// The application class failed to launch SOLIDWORKS.
        /// </summary>
        [Description("The application class failed to launch SOLIDWORKS.")]
        FailedToLaunchSW,


    }

    /// <summary>
    ///  Length units.
    /// </summary>
    public enum LengthUnits_e
    {
        /// <summary>
        /// Meter
        /// </summary>
        Meter,
        /// <summary>
        /// Millimeter
        /// </summary>
        Millimeter,
        /// <summary>
        /// Inch
        /// </summary>
        Inch,
        /// <summary>
        /// Foot
        /// </summary>
        Foot,
        /// <summary>
        /// Yard
        /// </summary>
        Yard,
        /// <summary>
        /// Kilometer
        /// </summary>
        Kilometer
    }

    /// <summary>
    /// Login PDM return values
    /// </summary>
    /// <remarks>The members of this enum are decorated with a user-friendly message using the <see cref="DescriptionAttribute"/>. Use this message to display meaningful errors to the user.</remarks>
    public enum LoginRet_e
    {
        /// <summary>
        /// Logged in successfully.
        /// </summary>
        [HResult(0)]
        [Description("The method executes successfully.")]
        S_OK,
        /// <summary>
        /// Attempted to login to the vault twice.
        /// </summary>
        [HResult(-2147220987)]
        [Description("You are already logged in to this vault.")]
        E_EDM_ALREADY_LOGGED_IN,
        /// <summary>
        /// Incorrect vault name.
        /// </summary>
        [HResult(-2147220986)]
        [Description("The database could not be opened (Maybe the vault name was incorrect?).")]
        E_EDM_CANT_OPEN_DATABASE,
        /// <summary>
        /// Canceled by user.
        /// </summary>
        [HResult(-2147220954)]
        [Description("The user clicked Cancel instead of logging in.")]
        E_EDM_CANCELLED_BY_USER

    }
}
