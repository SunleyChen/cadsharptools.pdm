﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CADSharpTools.DocumentManager.Models
{
    /// <summary>
   /// A visible referenced document is a referenced document of a component that is a visible, fully-resolved and is not a toolbox document.
   /// </summary>
    public class swDocManVisibleReferencedDocument
    {
        /// <summary>
        /// Returns the full path name of the visible documents and all referenced documents.
        /// </summary>
        /// <returns>Full pathname and referenced configurations.</returns>
        public override string ToString()
        {
            return $"{this.PathName} [{string.Join(", ", this.ReferencedConfigurationNames)}]";
        }

        /// <summary>
        /// Internal ID of the reference.
        /// </summary>
        public int ID { get; private set; }
        /// <summary>
        /// Path name of the referenced ID
        /// </summary>
        public string PathName { get; private set; }
        /// <summary>
        /// Referenced configuration names
        /// </summary>
        public string[] ReferencedConfigurationNames { get; private set; } = new string[] { };

        /// <summary>
        /// Add a referenced configuration name to the document's list of configuration names.
        /// </summary>
        /// <param name="configurationName"></param>
        public void AddConfigurationName(string configurationName)
        {
            var internalList = ReferencedConfigurationNames.ToList();
            if (internalList.Exists(x => x == configurationName) == false)
                internalList.Add(configurationName);

            ReferencedConfigurationNames = internalList.ToArray();
        }
        /// <summary>
        /// Creates a new instance of the visible reference class. 
        /// </summary>
        /// <param name="pathName"></param>
        public swDocManVisibleReferencedDocument(string pathName)
        {
            this.PathName = pathName;
            ID = pathName.GetHashCode();
        }
    }

}
