﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using SolidWorks.Interop.swdocumentmgr;
using CADSharpTools.DocumentManager.Models;
using CADSharpTools.Core;

namespace CADSharpTools.DocumentManager
{

    /// <summary>
    /// Extension of class of the SOLIDWORKS Document Manager.
    /// </summary>
    /// <remarks>Careful considerations before using <see cref="swDocMan"/>: To avoid outdated references issues, please always update the references of your files by rebuilding them and saving them locally. <see cref="swDocMan"/> relies on the references information saved within your files. For PDM, please the Updates References command.
    /// </remarks>
    public class swDocMan
    {
       private ISwDMApplication4 app = default(ISwDMApplication4);
        /// <summary>
        /// Creates a new instance of the DocMan class. 
        /// </summary>
        /// <param name="licenseKey">SOLIDWORKS Document Manager API key.</param>
        public swDocMan(string licenseKey)
        {
            var factory = new SwDMClassFactory();
            app = factory.GetApplication(licenseKey) as ISwDMApplication4;
        }

        
        private List<int> hashlist = new List<int>();
      

        private void GetVisibleDocumentsRecursively(SwDMComponent2 swDmComponent, ref List<swDocManVisibleReferencedDocument> visibleReferenceParts, busyIndicatorCallBack callback = null)
        {
            var searchOption = app.GetSearchOptionObject();
            var swDmComponentDocOpenError = default(SwDmDocumentOpenError);
            if (swDmComponent != null)
            {
                if (swDmComponent.IsHidden() == false &&
                    swDmComponent.IsSuppressed() == false &&
                    swDmComponent.DocumentType == SwDmDocumentType.swDmDocumentAssembly
                    )
                {
                    // get all components 
                    string pathName = (swDmComponent as SwDMComponent10).PathName;
                    var error = default(SwDmDocumentOpenError);
                    var doc = app.GetDocument(pathName, SwDmDocumentType.swDmDocumentAssembly, true, out error) as SwDMDocument15;
                    if (doc != null)
                    {
                        var swDmConfiguration = doc.ConfigurationManager.GetConfigurationByName(swDmComponent.ConfigurationName) as SwDMConfiguration2;
                        // returns the top level components 
                        object componentsObj = swDmConfiguration.GetComponents();
                        if (componentsObj != null)
                        {
                            object[] components = componentsObj as object[];
                            if (components != null)
                            foreach (var component in components)
                            {
                                    if (callback.CancelRequested)
                                        return;

                                 GetVisibleDocumentsRecursively(component as SwDMComponent2, ref visibleReferenceParts,callback);
                            }
                        }
                        doc.CloseDoc();
                    }
                    
                }
                else if (swDmComponent.IsHidden() == false &&
                    swDmComponent.IsSuppressed() == false &&
                    swDmComponent.DocumentType == SwDmDocumentType.swDmDocumentPart
                    )
                {
                    var swDmComponent4 = swDmComponent as ISwDMComponent4;
                    var swDmComponent6 = swDmComponent as ISwDMComponent6;

                    var swDmComponentDocument = swDmComponent4.GetDocument2(true, searchOption, out swDmComponentDocOpenError);

                    if (swDmComponentDocOpenError != SwDmDocumentOpenError.swDmDocumentOpenErrorNone)
                        return;


                    if (swDmComponentDocument == null)
                        return;

                    if (swDmComponentDocument.ToolboxPart == SwDmToolboxPartType.swDmNotAToolboxPart)
                    {

                        string componentPathName = swDmComponent6.PathName;
                        string Name = System.IO.Path.GetFileNameWithoutExtension(componentPathName);
                        string referencedConfiguration = swDmComponent.ConfigurationName;
                        int hashedIdentity = $"{componentPathName}{referencedConfiguration}".GetHashCode();

                        if (hashlist.Exists(x => x == hashedIdentity) == false)
                        {
                            var foundVisibleReferenceDocument = visibleReferenceParts.Find(x => x.ID == componentPathName.GetHashCode());
                            if (foundVisibleReferenceDocument != null)
                                foundVisibleReferenceDocument.AddConfigurationName(referencedConfiguration);
                            else
                            {
                                var visibleReference = new swDocManVisibleReferencedDocument(componentPathName);
                                visibleReference.AddConfigurationName(referencedConfiguration);
                                visibleReferenceParts.Add(visibleReference);
                                hashlist.Add(hashedIdentity);
                                // set callback 
                                if (callback != null)
                                {
                                    callback.Current++;
                                    callback.Message = $"Found {callback.Current} visible referenced parts so far. Last found: {System.IO.Path.GetFileName(visibleReference.PathName)} [{referencedConfiguration}].";
                                }
                            }

                        }
                        
                    }

                    swDmComponentDocument.CloseDoc();
                }


            }
        }


        /// <summary>
        /// Get all visible referenced parts that are not toolbox.
        /// </summary>
        /// <param name="pathName"></param>
        /// <param name="configurationName"></param>
        /// <param name="callback">Used to report status on large assemblies. Optional.</param>
        /// <returns>An array of <see cref="swDocManVisibleReferencedDocument"/></returns>
        public MethodReturn<swDocManVisibleReferencedDocument[]> GetAllVisibleReferencedPartsFromAssembly(string pathName, string configurationName = "", busyIndicatorCallBack callback = null)
        {
            try
            {

                var visibleReferenceList = new List<swDocManVisibleReferencedDocument>();
                var error = default(SwDmDocumentOpenError);
                var doc = app.GetDocument(pathName, SwDmDocumentType.swDmDocumentAssembly, true, out error) as SwDMDocument15;

                switch (error)
                {
                    case SwDmDocumentOpenError.swDmDocumentOpenErrorFail:
                        return new MethodReturn<swDocManVisibleReferencedDocument[]>(null, new Exception($"Document Manager Error: Generic error - {pathName}"));
                    case SwDmDocumentOpenError.swDmDocumentOpenErrorNonSW:
                        return new MethodReturn<swDocManVisibleReferencedDocument[]>(null, new Exception($"Document Manager Error: File is a SW file. error - {pathName}"));
                    case SwDmDocumentOpenError.swDmDocumentOpenErrorFileNotFound:
                        return new MethodReturn<swDocManVisibleReferencedDocument[]>(null, new Exception($"Document Manager Error: Not found - {pathName}"));
                    case SwDmDocumentOpenError.swDmDocumentOpenErrorNoLicense:
                        break;
                    case SwDmDocumentOpenError.swDmDocumentOpenErrorFutureVersion:
                        return new MethodReturn<swDocManVisibleReferencedDocument[]>(null, new Exception($"Document Manager Error: File is a future version. Latest supported file version: {SOLIDWORKS.Application.ConvertSWRevisionNumberToYear(app.GetLatestSupportedFileVersion())} - PathName {pathName}"));
                    default:
                        break;
                }


                if (doc == null)
                    return new MethodReturn<swDocManVisibleReferencedDocument[]>(null, new Exception($"Generic error. Failed to open {pathName}"));

                // if configuration name is empty, then get active configuration 
                if (string.IsNullOrWhiteSpace(configurationName))
                    configurationName = doc.ConfigurationManager.GetActiveConfigurationName();


                var swDmConfiguration = doc.ConfigurationManager.GetConfigurationByName(configurationName) as SwDMConfiguration2;
               

                // returns the top level components 
                object componentsObj = swDmConfiguration.GetComponents();
                if (componentsObj == null)
                    return new MethodReturn<swDocManVisibleReferencedDocument[]>(null, new Exception($"Failed to get references. Referenced configuration [{configurationName}] is dirty and requires rebuilding. Please rebuild it and save the document - PathName {pathName}"));

                
 

                object[] components = componentsObj as object[];
                if (components == null || components.Length == 0)
                    return new MethodReturn<swDocManVisibleReferencedDocument[]>(null, new Exception($"Warning: assembly has no components. Filename: {pathName}"));

                if (callback != null)
                    callback.IsIndeterminate = true; 

                    foreach (var component in components)
                {
                    var swDmComponent = component as SwDMComponent2;
                  
                    GetVisibleDocumentsRecursively(swDmComponent, ref visibleReferenceList,callback);


                }

                doc.CloseDoc();
                hashlist.Clear();
                
                return new MethodReturn<swDocManVisibleReferencedDocument[]>(visibleReferenceList.ToArray());

            }
            catch (COMException e)
            {
                hashlist.Clear();
                return new MethodReturn<swDocManVisibleReferencedDocument[]>(null, new Exception($"Document Manager Error: {e.Message}"));
            }
               
                   
        }

        /// <summary>
        /// Returns datetime of when the part was last saved.
        /// </summary>
        /// <param name="pathName">Local path.</param>
        /// <returns>Last save date time.</returns>
        public MethodReturn<DateTime> GetLastSaved(string pathName)
        {
         
            var error = default(SwDmDocumentOpenError);
            var doc = app.GetDocument(pathName, SwDmDocumentType.swDmDocumentAssembly, true, out error) as SwDMDocument19;

            switch (error)
            {
                case SwDmDocumentOpenError.swDmDocumentOpenErrorFail:
                    return new MethodReturn<DateTime>(DateTime.MinValue, new Exception($"Document Manager Error: Generic error - {pathName}"));
                case SwDmDocumentOpenError.swDmDocumentOpenErrorNonSW:
                    return new MethodReturn<DateTime>(DateTime.MinValue, new Exception($"Document Manager Error: File is a SW file. error - {pathName}"));
                case SwDmDocumentOpenError.swDmDocumentOpenErrorFileNotFound:
                    return new MethodReturn<DateTime>(DateTime.MinValue, new Exception($"Document Manager Error: Not found - {pathName}"));
                case SwDmDocumentOpenError.swDmDocumentOpenErrorNoLicense:
                    break;
                case SwDmDocumentOpenError.swDmDocumentOpenErrorFutureVersion:
                    return new MethodReturn<DateTime>(DateTime.MinValue, new Exception($"Document Manager Error: File is a future version. Latest supported file version: {SOLIDWORKS.Application.ConvertSWRevisionNumberToYear(app.GetLatestSupportedFileVersion())} - PathName {pathName}"));
                default:
                    break;
            }

            var dateTime = doc.LastSavedDate2;
            doc.CloseDoc();
            return new MethodReturn<DateTime>(UnixTimeStampToDateTime(dateTime));
        }


        /// <summary>
        /// Returns whether a document needs to be rebuilt.
        /// </summary>
        /// <param name="pathName">Local path.</param>
        /// <param name="configurationName">Configuration name.</param>
        /// <returns>True if document needs to be rebuilt, false if not.</returns>
        public MethodReturn<DateTime> GetLastModified(string pathName, string configurationName)
        {

            var error = default(SwDmDocumentOpenError);
            var doc = app.GetDocument(pathName, SwDmDocumentType.swDmDocumentAssembly, true, out error) as SwDMDocument15;

            switch (error)
            {
                case SwDmDocumentOpenError.swDmDocumentOpenErrorFail:
                    return new MethodReturn<DateTime>(DateTime.MinValue, new Exception($"Document Manager Error: Generic error - {pathName}"));
                case SwDmDocumentOpenError.swDmDocumentOpenErrorNonSW:
                    return new MethodReturn<DateTime>(DateTime.MinValue, new Exception($"Document Manager Error: File is a SW file. error - {pathName}"));
                case SwDmDocumentOpenError.swDmDocumentOpenErrorFileNotFound:
                    return new MethodReturn<DateTime>(DateTime.MinValue, new Exception($"Document Manager Error: Not found - {pathName}"));
                case SwDmDocumentOpenError.swDmDocumentOpenErrorNoLicense:
                    break;
                case SwDmDocumentOpenError.swDmDocumentOpenErrorFutureVersion:
                    return new MethodReturn<DateTime>(DateTime.MinValue, new Exception($"Document Manager Error: File is a future version. Latest supported file version: {SOLIDWORKS.Application.ConvertSWRevisionNumberToYear(app.GetLatestSupportedFileVersion())} - PathName {pathName}"));
                default:
                    break;
            }

            SwDMConfigurationMgr swCfgMgr;
            swCfgMgr = doc.ConfigurationManager;
            SwDMConfiguration config = swCfgMgr.GetConfigurationByName(configurationName);

            DateTime dateTime = default(DateTime);

            if (config != null)
               dateTime = UnixTimeStampToDateTime(config.GetLastUpdateStamp());


            doc.CloseDoc();
            return new MethodReturn<DateTime>(dateTime);
        }
        /// <summary>
        /// Returns whether configuration needs to be rebuit or not.
        /// </summary>
        /// <param name="pathName">Path name.</param>
        /// <param name="configurationName">Configuration name.</param>
        /// <returns>True or false.</returns>
        public MethodReturn<bool> IsDirty(string pathName, string configurationName)
        {

            var error = default(SwDmDocumentOpenError);
            var doc = app.GetDocument(pathName, SwDmDocumentType.swDmDocumentAssembly, true, out error) as SwDMDocument15;

            switch (error)
            {
                case SwDmDocumentOpenError.swDmDocumentOpenErrorFail:
                    return new MethodReturn<bool>(false, new Exception($"Document Manager Error: Generic error - {pathName}"));
                case SwDmDocumentOpenError.swDmDocumentOpenErrorNonSW:
                    return new MethodReturn<bool>(false, new Exception($"Document Manager Error: File is a SW file. error - {pathName}"));
                case SwDmDocumentOpenError.swDmDocumentOpenErrorFileNotFound:
                    return new MethodReturn<bool>(false, new Exception($"Document Manager Error: Not found - {pathName}"));
                case SwDmDocumentOpenError.swDmDocumentOpenErrorNoLicense:
                    break;
                case SwDmDocumentOpenError.swDmDocumentOpenErrorFutureVersion:
                    return new MethodReturn<bool>(false, new Exception($"Document Manager Error: File is a future version. Latest supported file version: {SOLIDWORKS.Application.ConvertSWRevisionNumberToYear(app.GetLatestSupportedFileVersion())} - PathName {pathName}"));
                default:
                    break;
            }

            SwDMConfigurationMgr swCfgMgr;
            swCfgMgr = doc.ConfigurationManager;
            var config = swCfgMgr.GetConfigurationByName(configurationName) as SwDMConfiguration11;

            bool isDirty = default(bool);

            if (config != null)
                isDirty = config.IsDirty();


            doc.CloseDoc();
            return new MethodReturn<bool>(isDirty);
        }
  


        internal static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }

    

}
