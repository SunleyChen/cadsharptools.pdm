﻿ 
namespace CADSharpTools
{
    internal static class Consts
    {
        public const string NoSelection = "No selection has been made.";
        public const string None = "None.";
        public const string RedColor = "#F22613";
        public const string GreenColor = "#26A65B";
        public const string Canceled = "Canceled.";
        public const string Complete = "Complete.";
        public const string CompleteWithError = "Completed with errors.";
        public const string Default = "Ready!";
        public const string LoadingFileWithColon = "Loading file:";
        public const string LoadingDocumentsElipsis = "Loading documents...";
        public const string OneDocumentLoaded = "One document was successfully added.";
        public const string MultipleDocumentLoaded = "documents were successfully added.";
        public const string CannotAddDuplicateDocument = "This document already exists in the list.";
        public const string SettingsNetException = "Fatal error occurred while getting setting value.";
        public const string NetException = "Fatal error has occurred.";
        public const string ConnectingToVaultElipsis = "Connecting to vault...";
        public const string ConnectingToVaultFailure = "Failed to connect to vault.";
        public const string Empty = "Empty.";

    }
}
