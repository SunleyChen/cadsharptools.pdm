﻿using System;
using System.Data.SqlClient;
using CADSharpTools.Core;
namespace CADSharpTools.Database
{
    /// <summary>
    /// Provides a singleton handler for SQL queries with SQL server.
    /// </summary>
    /// <remarks>
    /// To use the SQLHandler class:
    /// <ul>
    /// <li>Set the <see cref="ConnectionString"/> property.</li>
    /// <li>Invoke <see cref="Open"/> to connect to the database.</li>
    /// <li>Perform your SQL transaction by invoking <see cref="PerformSQLTransaction(Func{SqlConnection, MethodReturn{bool}})"/> if you don't require a return or <see cref="PerformSQLTransaction{T}(Func{SqlConnection, MethodReturn{T}})"/> if you require a return value.</li>
    /// <li>Close connection when done. You may reinvoke <see cref="Open"/> to reopen the connection.</li>
    /// <li>Dispose when application is exiting.</li>
    /// </ul>
    /// </remarks>
    public class SQLHandler
    {
        /// <summary>
        /// Singleton's connection string.
        /// </summary>
        public static string ConnectionString { get; private set; }
        /// <summary>
        /// Singleton's SqlConnection object.
        /// </summary>
        public static SqlConnection Connection { get; private set; } = default(SqlConnection);
        private SQLHandler()
        {

        }
        /// <summary>
        /// Sets <see cref="ConnectionString"/> property.
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        public static void SetConnectionString(string connectionString)
        {
            ConnectionString = connectionString; 
        }

       /// <summary>
       /// Opens a connection to the database.
       /// </summary>
        public static void Open()
        {
            if (string.IsNullOrWhiteSpace(ConnectionString))
            {
                throw new NullReferenceException("ConnectionString property is null.");
            }

            if (Connection == null)
            {                
                Connection = new SqlConnection(ConnectionString);
                Connection.Open(); 
            }
            else
            {
                if (Connection.State == System.Data.ConnectionState.Closed)
                {
                    Connection.Open();
                }
            }
            
        }

        /// <summary>
        /// Closes the connection. Connection can be reopened.
        /// </summary>
        public static void Close()
        {
            if (Connection != null)
            {
                if (Connection.State == System.Data.ConnectionState.Open)
                {
                    Connection.Close(); 
                }
                 
            }
        }
        /// <summary>
        /// Closes and disposes of the connection. 
        /// </summary>
        public static void Dispose()
        {
            Connection.Dispose();
            Connection = null; 
        }
        /// <summary>
        /// Performs a SQL transaction.
        /// </summary>
        /// <param name="Transaction">Func that receives a <see cref="SqlConnection"/> object and return a <see cref="MethodReturn"/>of <see cref="bool"/>. </param>
        /// <returns>MethodReturn of boolean: True if the transaction occurred without exceptions, false if not.</returns>
        public static MethodReturn<bool> PerformSQLTransaction(Func<SqlConnection,MethodReturn<bool>> Transaction)
        {
            if (string.IsNullOrWhiteSpace(ConnectionString))
            {
                throw new NullReferenceException("ConnectionString property is null.");
            }

            return Transaction.Invoke(Connection);            
        }


        /// <summary>
        /// Performs a SQL transaction. Generic variation.
        /// </summary>
        /// <param name="Transaction">Func that receives a <see cref="SqlConnection"/> object and return a <see cref="MethodReturn{T}"/>. </param>
        /// <returns>MethodReturn of boolean: True if the transaction occurred without exceptions, false if not.</returns>
        public static MethodReturn<T> PerformSQLTransaction<T>(Func<SqlConnection, MethodReturn<T>> Transaction)
        {
            if (string.IsNullOrWhiteSpace(ConnectionString))
            {
                throw new NullReferenceException("ConnectionString property is null.");
            }

            return Transaction.Invoke(Connection);
        }


    }
}
