﻿namespace CADSharpTools.Diagnostics
{
    using System;
    using System.IO;
    using System.Reflection;
    using CADSharpTools.Core;
    using Microsoft.VisualBasic;
    using SolidWorks.Interop.sldworks;


    /// <summary>
    /// Logger class.
    /// </summary>
    public class Logger
    {

        /// <summary>
        /// Message type.
        /// </summary>
        public enum MessageType
        {
            /// <summary>
            /// Information
            /// </summary>
            Information,
            /// <summary>
            /// Warning
            /// </summary>
            Warning,
            /// <summary>
            /// Program error
            /// </summary>
            ProgramError
        }

        private const string messageTypePrefix_Warning = "WARNING: ";
        private const string messageTypePrefix_Error = "ERROR: ";

        private string logPath;
        private bool addTimeStamps;

        /// <summary>
        /// Gets or sets the log folder.
        /// </summary>
        public DirectoryInfo LogsFolder { get; private set; }

        /// <summary>
        ///  Gets the log folder path and creates it if does not exist. Does not catch UnauthorizedAccessExceptions.
        /// </summary>
        /// <param name="logsFolder">Logs folder</param>
        /// <param name="addTimeStamps">True to add a time stamp</param>
        /// <param name="logFileName">Filename of the log file with extension</param>
        public Logger(DirectoryInfo logsFolder, string logFileName, bool addTimeStamps = false)
        {
            this.LogsFolder = logsFolder;
            this.addTimeStamps = addTimeStamps;
            string folder = this.LogsFolder.FullName;
            if (string.IsNullOrWhiteSpace(logFileName))
            logPath = Path.Combine(folder, "log" + " " + DateTime.Now.ToString("MM-dd-yy HH_mm_ss") +  ".txt");
            else
                logPath = Path.Combine(folder, logFileName);


            if (Directory.Exists(folder) == false)
                Directory.CreateDirectory(folder);
        }

        /// <summary>
        ///      Prints a message to the log file.
        ///      </summary>
        ///      <param name="message"></param>
        ///      <param name="type">Dictates what prefix is used with the message.</param>
        ///      <param name="ex">Exception to log.</param>
        ///      <param name="tabCount">Tab count.</param>
        ///      <returns>Message print to the log file.</returns>
        public string Print(string message, MessageType type = MessageType.Information, Exception ex = null, int tabCount = 0)
        {
            switch (type)
            {
                case MessageType.Warning:
                    {
                        message = messageTypePrefix_Warning + message;
                        break;
                    }

                case MessageType.ProgramError:
                    {
                        message = messageTypePrefix_Error + message;
                        break;
                    }
            }

            if (ex != null)
                message = message + Constants.vbNewLine + ex.Message + Constants.vbNewLine + ex.StackTrace;

            string tabs = null;
            for (var i = 1; i <= tabCount; i++)
                tabs = tabs + Constants.vbTab;

            string timeStamp = "[" + DateTime.Now.ToString("HH:mm:ss") + "] ";
            if (!addTimeStamps)
                timeStamp = null;

            using (StreamWriter myWriter = new StreamWriter(logPath, true))
            {
                myWriter.WriteLine(timeStamp + tabs + message);
            }

            return message;
        }

        /// <summary>
        ///      Print the program name, program version, job start time, and, optionally, the SolidWorks version.
        ///      </summary>
        ///      <param name="swApp">Pointer to the instance of SolidWorks you want the version of.</param>
        public void PrintStartInfo(object swApp = null)
        {
            Print(Assembly.GetCallingAssembly().GetAssemblyNameAndVersion());
            Print("Processing start time: " + DateTime.Now.ToString());

            // The argument should not be declared as SldWorks otherwise any project that uses this function
            // will need to reference the sldworks interop.
            if (swApp != null)
            {
                SldWorks app = (SldWorks)swApp;
                Print("SolidWorks version: " + app.RevisionNumber());
            }
 
        }

        /// <summary>
        ///      Prints the finish time and, optionally, the number of warnings that occurred during processing.
        ///      </summary>
        ///      <param name="printWarnings"></param>
        ///      <param name="warnings"></param>
        public void PrintEndInfo(bool printWarnings, int warnings = 0)
        {
            Print("Processing finish time: " + DateTime.Now.ToString());
            if (printWarnings)
                Print("Processing completed with " + warnings.ToString() + " warnings.");
            else
                Print("Processing completed.");
        }
         
    }

}
