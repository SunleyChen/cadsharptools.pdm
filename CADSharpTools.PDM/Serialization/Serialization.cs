﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using CADSharpTools.Core;
namespace CADSharpTools.Serialization
{
    /// <summary>
    /// Serialization class for CADSharpTools. This class uses the <i>good old</i> <see cref="XmlSerializer"/> class.
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    /// <remarks><see cref="CTSerialization{T}"/> uses <see cref="XmlSerializer"/>. This imposes limitation on what kind of objects you can serialize. Your implementation must have properties with public setters. To avoid this issue, use <see cref="CTContractSerialization{T}"/>.</remarks>
    /// <example>
    /// This example shows how to use the CTSerialization class.
    /// <code>
    /// public class Person
    /// {
    ///    public string FirstName { get; set; }
    ///    public string LastName { get; set; }
    ///    public uint Age { get; set; }
    ///    private Person()
    ///    {
    ///    }
    ///    public override string ToString()
    ///    {
    ///        return string.Format("{0} {1}", this.FirstName, this.LastName);
    ///    }
    /// }
    /// public class Program
    /// {
    ///    static void Main(string[] args)
    ///    {
    ///        // Create new instance of the CTSerialization
    ///        var cTSerializer = new CTSerialization&lt;Person &gt;();
    ///        // Create a new person 
    ///        var Person = new Person() { FirstName = "Amen", LastName = "JLILI", Age = 28 };
    ///        // Serialize the person 
    ///        var serializedPersonReturn = cTSerializer.Serialize(Person);
    ///        if (serializedPersonReturn.IsError)
    ///        {
    ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", serializedPersonReturn.Exception.Message));
    ///            Console.ReadKey();
    ///            return;
    ///        }
    ///        var serializedPerson = serializedPersonReturn.Value;
    ///        // Print XML 
    ///        System.Diagnostics.Debug.Print(serializedPerson);
    ///        // Deserialize XML 
    ///        var reserializedPersonReturn = CTSerialization&lt;Person&gt;.Deserialize(serializedPerson);
    ///        if (reserializedPersonReturn.IsError)
    ///        {
    ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", reserializedPersonReturn.Exception.Message));
    ///            Console.ReadKey();
    ///            return;
    ///        }
    ///        var reserializedPerson = reserializedPersonReturn.Value;
    ///        // Print full name 
    ///        System.Diagnostics.Debug.Print(reserializedPerson.ToString());
    ///        // Save to XML file
    ///        string fileName = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop), "person.xml");
    ///        cTSerializer.SaveToXML(reserializedPerson, fileName);
    ///        Console.ReadKey();
    ///    }
    ///  } 
    /// </code>
    ///</example>
    public class CTSerialization<T> where T : class
        {
            /// <summary>
            /// Creates a new instance of the CTSerialization class. 
            /// </summary>
            public CTSerialization()
            {

            }

            /// <summary>
            /// Serializes a type into an XML string. 
            /// </summary>
            /// <typeparam name="T">Object type to serialize.</typeparam>
            /// <returns>MethodReturn of type <see cref="String"/>. Use <see cref="MethodReturn{T}.Value"></see> to get the returned object.</returns>
            /// <remarks>You need to create an instance of <see cref="CTSerialization{T}"/> to access this method.</remarks>
            /// <example>
            /// This example shows how to use the CTSerialization class.
            /// <code>
            /// public class Person
            /// {
            ///    public string FirstName { get; set; }
            ///    public string LastName { get; set; }
            ///    public uint Age { get; set; }
            ///    private Person()
            ///    {
            ///    }
            ///    public override string ToString()
            ///    {
            ///        return string.Format("{0} {1}", this.FirstName, this.LastName);
            ///    }
            /// }
            /// public class Program
            /// {
            ///    static void Main(string[] args)
            ///    {
            ///        // Create new instance of the CTSerialization
            ///        var cTSerializer = new CTSerialization&lt;Person &gt;();
            ///        // Create a new person 
            ///        var Person = new Person() { FirstName = "Amen", LastName = "JLILI", Age = 28 };
            ///        // Serialize the person 
            ///        var serializedPersonReturn = cTSerializer.Serialize(Person);
            ///        if (serializedPersonReturn.IsError)
            ///        {
            ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", serializedPersonReturn.Exception.Message));
            ///            Console.ReadKey();
            ///            return;
            ///        }
            ///        var serializedPerson = serializedPersonReturn.Value;
            ///        // Print XML 
            ///        System.Diagnostics.Debug.Print(serializedPerson);
            ///        // Deserialize XML 
            ///        var reserializedPersonReturn = CTSerialization&lt;Person&gt;.Deserialize(serializedPerson);
            ///        if (reserializedPersonReturn.IsError)
            ///        {
            ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", reserializedPersonReturn.Exception.Message));
            ///            Console.ReadKey();
            ///            return;
            ///        }
            ///        var reserializedPerson = reserializedPersonReturn.Value;
            ///        // Print full name 
            ///        System.Diagnostics.Debug.Print(reserializedPerson.ToString());
            ///        // Save to XML file
            ///        string fileName = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop), "person.xml");
            ///        cTSerializer.SaveToXML(reserializedPerson, fileName);
            ///        Console.ReadKey();
            ///    }
            ///  } 
            /// </code>
            ///</example>
            public MethodReturn<string> Serialize(T obj)
            {



                try
                {
                    string ret = string.Empty;
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                    using (StringWriter textWriter = new StringWriter())
                    {
                        xmlSerializer.Serialize(textWriter, obj);
                        ret = textWriter.ToString();

                    }
                    return new MethodReturn<string>(ret);
                }
                catch (Exception e)
                {

                    return new MethodReturn<string>(string.Empty, true, e.GetRootException().Message);
                }



            }



            /// <summary>
            /// De-serializes an XML string into the specified type. 
            /// </summary>
            /// <param name="rawXMLString">XML string.</param>
            /// <returns>MethodReturn of the specified type. Use <see cref="MethodReturn{T}.Value"></see> to get the returned object.</returns>
            /// <example>
            /// This example shows how to use the CTSerialization class.
            /// <code>
            /// public class Person
            /// {
            ///    public string FirstName { get; set; }
            ///    public string LastName { get; set; }
            ///    public uint Age { get; set; }
            ///    private Person()
            ///    {
            ///    }
            ///    public override string ToString()
            ///    {
            ///        return string.Format("{0} {1}", this.FirstName, this.LastName);
            ///    }
            /// }
            /// public class Program
            /// {
            ///    static void Main(string[] args)
            ///    {
            ///        // Create new instance of the CTSerialization
            ///        var cTSerializer = new CTSerialization&lt;Person &gt;();
            ///        // Create a new person 
            ///        var Person = new Person() { FirstName = "Amen", LastName = "JLILI", Age = 28 };
            ///        // Serialize the person 
            ///        var serializedPersonReturn = cTSerializer.Serialize(Person);
            ///        if (serializedPersonReturn.IsError)
            ///        {
            ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", serializedPersonReturn.Exception.Message));
            ///            Console.ReadKey();
            ///            return;
            ///        }
            ///        var serializedPerson = serializedPersonReturn.Value;
            ///        // Print XML 
            ///        System.Diagnostics.Debug.Print(serializedPerson);
            ///        // Deserialize XML 
            ///        var reserializedPersonReturn = CTSerialization&lt;Person&gt;.Deserialize(serializedPerson);
            ///        if (reserializedPersonReturn.IsError)
            ///        {
            ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", reserializedPersonReturn.Exception.Message));
            ///            Console.ReadKey();
            ///            return;
            ///        }
            ///        var reserializedPerson = reserializedPersonReturn.Value;
            ///        // Print full name 
            ///        System.Diagnostics.Debug.Print(reserializedPerson.ToString());
            ///        // Save to XML file
            ///        string fileName = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop), "person.xml");
            ///        cTSerializer.SaveToXML(reserializedPerson, fileName);
            ///        Console.ReadKey();
            ///    }
            ///  } 
            /// </code>
            ///</example>
            public static MethodReturn<T> Deserialize(string rawXMLString)
            {
                T ret = default(T);
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                using (StringReader textReader = new StringReader(rawXMLString))
                {
                    ret = (T)xmlSerializer.Deserialize(textReader);
                }

                return new MethodReturn<T>(ret);
            }
            /// <summary>
            /// Saves an object of a specified type to an XML file.
            /// </summary>
            /// <typeparam name="T">Type to serialize.</typeparam>
            /// <param name="obj">Object to serialize.</param>
            /// <param name="fileName">complete pathname of the file to be saved.</param>
            /// <returns>MethodReturn of the specified type. Use <see cref="MethodReturn{T}.Value"></see> to get the returned object.</returns>
            /// <example>
            /// This example shows how to use the CTSerialization class.
            /// <code>
            /// public class Person
            /// {
            ///    public string FirstName { get; set; }
            ///    public string LastName { get; set; }
            ///    public uint Age { get; set; }
            ///    private Person()
            ///    {
            ///    }
            ///    public override string ToString()
            ///    {
            ///        return string.Format("{0} {1}", this.FirstName, this.LastName);
            ///    }
            /// }
            /// public class Program
            /// {
            ///    static void Main(string[] args)
            ///    {
            ///        // Create new instance of the CTSerialization
            ///        var cTSerializer = new CTSerialization&lt;Person &gt;();
            ///        // Create a new person 
            ///        var Person = new Person() { FirstName = "Amen", LastName = "JLILI", Age = 28 };
            ///        // Serialize the person 
            ///        var serializedPersonReturn = cTSerializer.Serialize(Person);
            ///        if (serializedPersonReturn.IsError)
            ///        {
            ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", serializedPersonReturn.Exception.Message));
            ///            Console.ReadKey();
            ///            return;
            ///        }
            ///        var serializedPerson = serializedPersonReturn.Value;
            ///        // Print XML 
            ///        System.Diagnostics.Debug.Print(serializedPerson);
            ///        // Deserialize XML 
            ///        var reserializedPersonReturn = CTSerialization&lt;Person&gt;.Deserialize(serializedPerson);
            ///        if (reserializedPersonReturn.IsError)
            ///        {
            ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", reserializedPersonReturn.Exception.Message));
            ///            Console.ReadKey();
            ///            return;
            ///        }
            ///        var reserializedPerson = reserializedPersonReturn.Value;
            ///        // Print full name 
            ///        System.Diagnostics.Debug.Print(reserializedPerson.ToString());
            ///        // Save to XML file
            ///        string fileName = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop), "person.xml");
            ///        cTSerializer.SaveToXML(reserializedPerson, fileName);
            ///        Console.ReadKey();
            ///    }
            ///  } 
            /// </code>
            ///</example>
            public MethodReturn<bool> SaveToXML(T obj, string fileName)
            {
                var ret = this.Serialize(obj);

                if (ret.IsError)
                    return new MethodReturn<bool>(false, true, "Failed to save to disk. " + ret.Error);

                var xmlStr = ret.Value;
                try
                {
                    System.IO.File.WriteAllText(fileName, xmlStr);
                    return new MethodReturn<bool>(true);
                }
                catch (Exception e)
                {
                    return new MethodReturn<bool>(false, true, e.GetRootException().Message);
                }
            }
        }


        /// <summary>
        /// Serialization class for CADSharpTools. Use this class for objects that contain properties with private setters.
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <example>
        /// This example shows how to use the CTContractSerialization class.
        /// <code>
        /// public class Person
        /// {
        ///    [DataMember]
        ///    public string FirstName { get; private set; }
        ///    [DataMember]
        ///    public string LastName { get; private set; }
        ///    [DataMember]
        ///    public uint Age { get; private set; }
        ///    private Person()
        ///    {
        ///    }
        ///    public static Person CreateInstance()
        ///    {
        ///       var p = new Person;
        ///       p.LastName = "Jlili";
        ///       p.FirstName = "Amen";
        ///       p.Age = 29;
        ///       return p;
        ///    }
        ///    public override string ToString()
        ///    {
        ///        return string.Format("{0} {1}", this.FirstName, this.LastName);
        ///    }
        /// }
        /// public class Program
        /// {
        ///    static void Main(string[] args)
        ///    {
        ///        // Create new instance of the CTContractSerialization
        ///        var cTSerializer = new CTContractSerialization&lt;Person &gt;();
        ///        // Create a new person 
        ///        var Person = Person.CreateInstance();
        ///        // Serialize the person 
        ///        var serializedPersonReturn = cTSerializer.Serialize(Person);
        ///        if (serializedPersonReturn.IsError)
        ///        {
        ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", serializedPersonReturn.Exception.Message));
        ///            Console.ReadKey();
        ///            return;
        ///        }
        ///        var serializedPerson = serializedPersonReturn.Value;
        ///        // Print XML 
        ///        System.Diagnostics.Debug.Print(serializedPerson);
        ///        // Deserialize XML 
        ///        var reserializedPersonReturn = CTContractSerialization&lt;Person&gt;.Deserialize(serializedPerson);
        ///        if (reserializedPersonReturn.IsError)
        ///        {
        ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", reserializedPersonReturn.Exception.Message));
        ///            Console.ReadKey();
        ///            return;
        ///        }
        ///        var reserializedPerson = reserializedPersonReturn.Value;
        ///        // Print full name 
        ///        System.Diagnostics.Debug.Print(reserializedPerson.ToString());
        ///        // Save to XML file
        ///        string fileName = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop), "person.xml");
        ///        cTSerializer.SaveToXML(reserializedPerson, fileName);
        ///        Console.ReadKey();
        ///    }
        ///  } 
        /// </code>
        ///</example>
        ///<remarks>The specified type must have its properties that contain a private setter decorated with <see cref="DataMemberAttribute"/>.</remarks>
        public class CTContractSerialization<T> where T : class
        {
            /// <summary>
            /// Creates a new instance of the CTContractSerialization class. 
            /// </summary>
            public CTContractSerialization()
            {

            }




            /// <summary>
            /// Serializes a type into an XML string. 
            /// </summary>
            /// <typeparam name="T">Object type to serialize.</typeparam>
            /// <returns>MethodReturn of type <see cref="String"/>. Use <see cref="MethodReturn{T}.Value"></see> to get the returned object.</returns>
            /// <remarks>You need to create an instance of <see cref="CTContractSerialization{T}"/> to access this method.</remarks>
            /// <example>
            /// This example shows how to use the CTContractSerialization class.
            /// <code>
            /// public class Person
            /// {
            ///    [DataMember]
            ///    public string FirstName { get; private set; }
            ///    [DataMember]
            ///    public string LastName { get; private set; }
            ///    [DataMember]
            ///    public uint Age { get; private set; }
            ///    private Person()
            ///    {
            ///    }
            ///    public static Person CreateInstance()
            ///    {
            ///       var p = new Person;
            ///       p.LastName = "Jlili";
            ///       p.FirstName = "Amen";
            ///       p.Age = 29;
            ///       return p;
            ///    }
            ///    public override string ToString()
            ///    {
            ///        return string.Format("{0} {1}", this.FirstName, this.LastName);
            ///    }
            /// }
            /// public class Program
            /// {
            ///    static void Main(string[] args)
            ///    {
            ///        // Create new instance of the CTContractSerialization
            ///        var cTSerializer = new CTContractSerialization&lt;Person &gt;();
            ///        // Create a new person 
            ///        var Person = Person.CreateInstance();
            ///        // Serialize the person 
            ///        var serializedPersonReturn = cTSerializer.Serialize(Person);
            ///        if (serializedPersonReturn.IsError)
            ///        {
            ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", serializedPersonReturn.Exception.Message));
            ///            Console.ReadKey();
            ///            return;
            ///        }
            ///        var serializedPerson = serializedPersonReturn.Value;
            ///        // Print XML 
            ///        System.Diagnostics.Debug.Print(serializedPerson);
            ///        // Deserialize XML 
            ///        var reserializedPersonReturn = CTContractSerialization&lt;Person&gt;.Deserialize(serializedPerson);
            ///        if (reserializedPersonReturn.IsError)
            ///        {
            ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", reserializedPersonReturn.Exception.Message));
            ///            Console.ReadKey();
            ///            return;
            ///        }
            ///        var reserializedPerson = reserializedPersonReturn.Value;
            ///        // Print full name 
            ///        System.Diagnostics.Debug.Print(reserializedPerson.ToString());
            ///        // Save to XML file
            ///        string fileName = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop), "person.xml");
            ///        cTSerializer.SaveToXML(reserializedPerson, fileName);
            ///        Console.ReadKey();
            ///    }
            ///  } 
            /// </code>
            ///</example>
            public MethodReturn<string> Serialize(T obj)
            {
                try
                {
                    string ret = string.Empty;
                    DataContractSerializer xmlSerializer = new DataContractSerializer(typeof(T));
                    var memoryStream = new MemoryStream();
                    xmlSerializer.WriteObject(memoryStream, obj);
                var str = string.Empty;
                memoryStream.Position = 0;
                using (StreamReader reader = new StreamReader(memoryStream))
                {
                    str = reader.ReadToEnd();
                }
    
                ret = XElement.Parse(str).ToString().Replace("\0", "");
                    return new MethodReturn<string>(ret);
                }
                catch (Exception e)
                {

                    return new MethodReturn<string>(string.Empty, true, e.GetRootException().Message);
                }



            }



            /// <summary>
            /// De-serializes an XML string into the specified type. 
            /// </summary>
            /// <param name="rawXMLString">XML string.</param>
            /// <returns>MethodReturn of the specified type. Use <see cref="MethodReturn{T}.Value"></see> to get the returned object.</returns>
            /// <example>
            /// This example shows how to use the CTContractSerialization class.
            /// <code>
            /// public class Person
            /// {
            ///    [DataMember]
            ///    public string FirstName { get; private set; }
            ///    [DataMember]
            ///    public string LastName { get; private set; }
            ///    [DataMember]
            ///    public uint Age { get; private set; }
            ///    private Person()
            ///    {
            ///    }
            ///    public static Person CreateInstance()
            ///    {
            ///       var p = new Person;
            ///       p.LastName = "Jlili";
            ///       p.FirstName = "Amen";
            ///       p.Age = 29;
            ///       return p;
            ///    }
            ///    public override string ToString()
            ///    {
            ///        return string.Format("{0} {1}", this.FirstName, this.LastName);
            ///    }
            /// }
            /// public class Program
            /// {
            ///    static void Main(string[] args)
            ///    {
            ///        // Create new instance of the CTContractSerialization
            ///        var cTSerializer = new CTContractSerialization&lt;Person &gt;();
            ///        // Create a new person 
            ///        var Person = Person.CreateInstance();
            ///        // Serialize the person 
            ///        var serializedPersonReturn = cTSerializer.Serialize(Person);
            ///        if (serializedPersonReturn.IsError)
            ///        {
            ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", serializedPersonReturn.Exception.Message));
            ///            Console.ReadKey();
            ///            return;
            ///        }
            ///        var serializedPerson = serializedPersonReturn.Value;
            ///        // Print XML 
            ///        System.Diagnostics.Debug.Print(serializedPerson);
            ///        // Deserialize XML 
            ///        var reserializedPersonReturn = CTContractSerialization&lt;Person&gt;.Deserialize(serializedPerson);
            ///        if (reserializedPersonReturn.IsError)
            ///        {
            ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", reserializedPersonReturn.Exception.Message));
            ///            Console.ReadKey();
            ///            return;
            ///        }
            ///        var reserializedPerson = reserializedPersonReturn.Value;
            ///        // Print full name 
            ///        System.Diagnostics.Debug.Print(reserializedPerson.ToString());
            ///        // Save to XML file
            ///        string fileName = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop), "person.xml");
            ///        cTSerializer.SaveToXML(reserializedPerson, fileName);
            ///        Console.ReadKey();
            ///    }
            ///  } 
            /// </code>
            ///</example>
            public static MethodReturn<T> Deserialize(string rawXMLString)
            {
                T ret = default(T);
                DataContractSerializer xmlSerializer = new DataContractSerializer(typeof(T));
                using (Stream stream = new MemoryStream())
                {
                    byte[] data = System.Text.Encoding.UTF8.GetBytes(rawXMLString);
                    stream.Write(data, 0, data.Length);
                    stream.Position = 0;

                    ret = xmlSerializer.ReadObject(stream) as T;

                    return new MethodReturn<T>(ret);
                }
            }
            /// <summary>
            /// Saves an object of a specified type to an XML file.
            /// </summary>
            /// <typeparam name="T">Type to serialize.</typeparam>
            /// <param name="obj">Object to serialize.</param>
            /// <param name="fileName">complete pathname of the file to be saved.</param>
            /// <returns>MethodReturn of the specified type. Use <see cref="MethodReturn{T}.Value"></see> to get the returned object.</returns>
            /// <example>
            /// This example shows how to use the <see cref="CTContractSerialization{T}"/> class.
            /// <code>
            /// public class Person
            /// {
            ///    [DataMember]
            ///    public string FirstName { get; private set; }
            ///    [DataMember]
            ///    public string LastName { get; private set; }
            ///    [DataMember]
            ///    public uint Age { get; private set; }
            ///    private Person()
            ///    {
            ///    }
            ///    public static Person CreateInstance()
            ///    {
            ///       var p = new Person;
            ///       p.LastName = "Jlili";
            ///       p.FirstName = "Amen";
            ///       p.Age = 29;
            ///       return p;
            ///    }
            ///    public override string ToString()
            ///    {
            ///        return string.Format("{0} {1}", this.FirstName, this.LastName);
            ///    }
            /// }
            /// public class Program
            /// {
            ///    static void Main(string[] args)
            ///    {
            ///        // Create new instance of the CTContractSerialization
            ///        var cTSerializer = new CTContractSerialization&lt;Person &gt;();
            ///        // Create a new person 
            ///        var Person = Person.CreateInstance();
            ///        // Serialize the person 
            ///        var serializedPersonReturn = cTSerializer.Serialize(Person);
            ///        if (serializedPersonReturn.IsError)
            ///        {
            ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", serializedPersonReturn.Exception.Message));
            ///            Console.ReadKey();
            ///            return;
            ///        }
            ///        var serializedPerson = serializedPersonReturn.Value;
            ///        // Print XML 
            ///        System.Diagnostics.Debug.Print(serializedPerson);
            ///        // Deserialize XML 
            ///        var reserializedPersonReturn = CTContractSerialization&lt;Person&gt;.Deserialize(serializedPerson);
            ///        if (reserializedPersonReturn.IsError)
            ///        {
            ///            System.Diagnostics.Debug.Print(string.Format("Exception caught. Message = {0}", reserializedPersonReturn.Exception.Message));
            ///            Console.ReadKey();
            ///            return;
            ///        }
            ///        var reserializedPerson = reserializedPersonReturn.Value;
            ///        // Print full name 
            ///        System.Diagnostics.Debug.Print(reserializedPerson.ToString());
            ///        // Save to XML file
            ///        string fileName = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop), "person.xml");
            ///        cTSerializer.SaveToXML(reserializedPerson, fileName);
            ///        Console.ReadKey();
            ///    }
            ///  } 
            /// </code>
            ///</example>
            public MethodReturn<bool> SaveToXML(T obj, string fileName)
            {
                var ret = this.Serialize(obj);

                if (ret.IsError)
                    return new MethodReturn<bool>(false, true, "Failed to save to disk. " + ret.Error);

                var xmlStr = ret.Value;
                try
                {
                    System.IO.File.WriteAllText(fileName, xmlStr);
                    return new MethodReturn<bool>(true);
                }
                catch (Exception e)
                {
                    return new MethodReturn<bool>(false, true, e.GetRootException().Message);
                }
            }
        }
    }

