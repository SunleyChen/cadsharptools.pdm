﻿using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Xml;
using CADSharpTools.Core;
using CADSharpTools.Exceptions;

namespace CADSharpTools.SOLIDWORKS
{

    /// <summary>
    /// Extension class for SOLIDWORKS API objects. 
    /// </summary>
    public static class Extension
    {

            /// <summary>
            /// Reevaluates equations and rebuild document.
            /// </summary>
            /// <param name="modelDoc">ModelDoc2 object.</param>
            public static void Evaluate(this ModelDoc2 modelDoc)
            {

            if (modelDoc.GetType() != (int)swDocumentTypes_e.swDocDRAWING)
            {
                // evaluate equations 
                var equationMgr = modelDoc.GetEquationMgr();
                var equationsNeedsUpdate = new List<int>();
                int equationsCount = equationMgr.GetCount();
                if (equationsCount > 0)
                {
                    for (int i = 0; i < equationsCount; i++)
                    {
                        var equationValueRet = equationMgr.Value[i];
                        var status = equationMgr.Status;
                        if (status == -1)
                            equationsNeedsUpdate.Add(i);
                    }
                    // evaluate equations 
                    if (equationsNeedsUpdate.Count > 0)
                        equationMgr.EvaluateAll();

                    var modelDocExtension = modelDoc.Extension as ModelDocExtension;
                    var rebuildRet = modelDocExtension.NeedsRebuild;
                    if (rebuildRet)

                    {
                        var configurationNames = modelDoc.GetConfigurationNames() as object[];
                        foreach (var _configurationName in configurationNames)
                        {


                            modelDoc.ShowConfiguration2(_configurationName.ToString());
                            modelDoc.ForceRebuild3(false);


                        }
                    }

                }
            }
            }


            /// <summary>
            /// Returns the mass and the unit of the mass for the specified model.
            /// </summary>
            /// <param name="model">Model object.</param>
            /// <returns>Model</returns>
            public static KeyValuePair<double, swUnitsMassPropMass_e> GetModelMass(this ModelDoc2 model)
            {
            var modelDocExt = model.Extension;
            var data = default(double[]);
                var status = default(int);
                data = modelDocExt.GetMassProperties2(0, out status, true) as double[];
            int massIndex = 5;
            swMassPropertiesStatus_e swStatus = (swMassPropertiesStatus_e)status;
            switch (swStatus)
            {
                case swMassPropertiesStatus_e.swMassPropertiesStatus_OK:
                    {
                        // mass in kilo 
                        var currentMassUnit = (swUnitsMassPropMass_e)model.GetUserPreferenceIntegerValue((int)swUserPreferenceIntegerValue_e.swUnitsMassPropMass);
                        double mass = data[massIndex];
                        var val = CADSharpTools.SOLIDWORKS.Measure.ConvertMass(swUnitsMassPropMass_e.swUnitsMassPropMass_Kilograms, currentMassUnit,mass);
                        return new KeyValuePair<double, swUnitsMassPropMass_e>(val,currentMassUnit);
                    }
                case swMassPropertiesStatus_e.swMassPropertiesStatus_UnknownError:
                        return new KeyValuePair<double, swUnitsMassPropMass_e>(-1, swUnitsMassPropMass_e.swUnitsMassPropMass_Grams);
                case swMassPropertiesStatus_e.swMassPropertiesStatus_NoBody:
                    return new KeyValuePair<double, swUnitsMassPropMass_e>(-1, swUnitsMassPropMass_e.swUnitsMassPropMass_Grams);
                default:
                    return new KeyValuePair<double, swUnitsMassPropMass_e>(-1, swUnitsMassPropMass_e.swUnitsMassPropMass_Grams);
            }

        }

            /// <summary>
            /// Returns an array of all SOLIDWORKS material names from all SOLIDWORKS material libraries.
            /// </summary>
            /// <param name="swApp">SldWorks object.</param>
            /// <returns>An array of material names.</returns>
            public static Task<MethodReturn<string[]>> GetSOLIDWORKSMaterialsAsync(this SldWorks swApp)
            {
                if (swApp.GetMaterialDatabaseCount() == 0)
                    return Task.Run(() => { return new MethodReturn<string[]>(new string[] { }, CADSharpToolsPDMException.CreateInstance(CADSharpTools_e.CADSharpToolsPDMExceptionMessages_e.MaterialDatabaseCountZero), true); });

                return Task.Run(() => {
                    var materials = new List<string>();


                    var mdatabaseObjects = swApp.GetMaterialDatabases() as object[];
                    foreach (var mdatabaseObject in mdatabaseObjects)
                    {
                        string databasePath = mdatabaseObject as string;
                        string[] materialsarray = CADSharpTools.SOLIDWORKS.Extension.GetMaterialNamesFromDataBase(databasePath);
                        materials.AddRange(materialsarray);
                    }


                    return new MethodReturn<string[]>(materials.ToArray());

                });
            }
        /// <summary>
        /// Returns an array of all SOLIDWORKS material names from all SOLIDWORKS material libraries.
        /// </summary>
        /// <param name="swApp">SldWorks object.</param>
        /// <returns>An array of material names.</returns>
        public static MethodReturn<string[]> GetSOLIDWORKSMaterials(this SldWorks swApp)
        {
            if (swApp.GetMaterialDatabaseCount() == 0)
                 return new MethodReturn<string[]>(new string[] { }, CADSharpToolsPDMException.CreateInstance(CADSharpTools_e.CADSharpToolsPDMExceptionMessages_e.MaterialDatabaseCountZero));


                var materials = new List<string>();


                var mdatabaseObjects = swApp.GetMaterialDatabases() as object[];
                foreach (var mdatabaseObject in mdatabaseObjects)
                {
                    string databasePath = mdatabaseObject as string;
                    string[] materialsarray = CADSharpTools.SOLIDWORKS.Extension.GetMaterialNamesFromDataBase(databasePath);
                    materials.AddRange(materialsarray);
                }


                return new MethodReturn<string[]>(materials.ToArray());

        }

     

        /// <summary>
        /// Gets an array of SOLIDWORKS material names from a SOLIDWORKS material database.
        /// </summary>
        /// <param name="pathName">Pathname of the SOLIDWORKS material database.</param>
        /// <returns>An array of material names</returns>
        public static string[] GetMaterialNamesFromDataBase(string pathName)
        {
            List<string> mats = new List<string>();
            if (System.IO.File.Exists(pathName))
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.DtdProcessing = DtdProcessing.Parse;
                XmlReader reader = XmlReader.Create(pathName, settings);
                while (reader.Read())
                {
                    if (reader.Name == "material")
                    {
                        string m = reader.GetAttribute("name");
                        if (!string.IsNullOrEmpty(m))
                            mats.Add(m);
                    }
                }

            }

            return mats.ToArray();
        }


        private static MethodReturn<MathVector> TransformVectorToTop(this Component2 swComponent, MathUtility swMathUtility, double[] Arr)
        {
            if (swComponent.GetSuppression() != (int)swComponentSuppressionState_e.swComponentSuppressed)
                throw new Exception("TranformPointToMainAssembly failed because the component is suppressed. Cannot get MathTransform of a supppressed component.");
            MathVector swMathVector = default(MathVector);
            while (swComponent != null)
            {
                if (swMathVector != null)
                    Arr = swMathVector.ArrayData as double[];

                MathTransform swMathTransform = default(MathTransform);
                swMathTransform = swComponent.Transform2;

                swMathVector = swMathUtility.CreateVector(Arr) as MathVector;
                swMathVector = swMathVector.MultiplyTransform(swMathTransform) as MathVector;

                swComponent = swComponent.GetParent() as Component2;

            }

            return new MethodReturn<MathVector>(swMathVector);

        }

        /// <summary>
        /// Transforms a math vector from the default coordinate system of a model to that of a drawing view 
        /// </summary>
        /// <param name=""></param>
        /// <param name="swMathUtility"></param>
        /// <param name="swView"></param>
        /// <param name="Arr"></param>
        /// <returns></returns>
        private static MethodReturn<MathVector> TransformVectorFromModelToDrawing(this View swView, MathUtility swMathUtility, double[] Arr)
        {
            ModelDoc2 swReferencedModel = swView.ReferencedDocument;
            if (swReferencedModel == null)
                throw new Exception("Referenced model is null");

            MathTransform swMathTransform = default(MathTransform);
            swMathTransform = swView.ModelToViewTransform;

            MathVector swMathVector = swMathUtility.CreateVector(Arr) as MathVector;
            swMathVector = swMathVector.MultiplyTransform(swMathTransform) as MathVector;


            return new MethodReturn<MathVector>(swMathVector);
        }


        /// <summary>
        /// Gets an array of selected SOLIDWORKS objects.
        /// </summary>
        /// <typeparam name="T">SOLIDWORKS API object type.</typeparam>
        /// <param name="swModel">Model document object.</param>
        /// <param name="swSelectType">Selection type of the object to be selected. Parameter supports flags but method ignores that.</param>
        /// <param name="count">Count of the selected objects to return.</param>
        /// <param name="selectionMark">Selection mark. This parameter is optional. If not specified, the selection mark is -1.</param>
        /// <returns>An array of selected SOLIDWORKS objects.</returns>
        /// <remarks>
        /// Methods returns an empty of the specified SOLIDWORKS object type if no selection is found.
        /// </remarks>
        public static MethodReturn<T[]> GetSelectedObjects<T>(this ModelDoc2 swModel, swSelectType_e swSelectType, int count, int selectionMark = -1)
        {
            try
            {
            var list = new List<T>();
            var selectionMgr = swModel.SelectionManager as SelectionMgr;
            int selectedobjectsCount = selectionMgr.GetSelectedObjectCount2(selectionMark);
            if (selectedobjectsCount == 0)
                return new MethodReturn<T[]>(new T[] { }, Consts.NoSelection);
            else
            {
                for (int i = 1; i <= selectedobjectsCount; i++)
                {
                    T obj = (T)selectionMgr.GetSelectedObject6(i, selectionMark);
                    list.Add(obj);

                }
            }
                return new MethodReturn<T[]>(list.ToArray());
            }
            catch (Exception e)
            {
                return new MethodReturn<T[]>(null, e, true);
            }          
        }

        /// <summary>
        /// Marshals an array of objects into SW objects.
        /// </summary>
        /// <param name="SwObjects">SOLIDWORKS objects.</param>
        /// <returns>Array of BStrWrapper.</returns>
        public static BStrWrapper[] ObjectArrayToBStrWrapperArray(object[] SwObjects)
        {
            int arraySize;
            arraySize = SwObjects.GetUpperBound(0);
            BStrWrapper[] dispwrap = new BStrWrapper[arraySize + 1];
            int arrayIndex;

            for (arrayIndex = 0; arrayIndex < arraySize + 1; arrayIndex++)
            {
                dispwrap[arrayIndex] = new BStrWrapper((string)(SwObjects[arrayIndex]));
            }

            return dispwrap;

        }

        /// <summary>
        /// Gets the SOLIDWORKS document type.
        /// </summary>
        /// <param name="path">Complete path of the SOLIDWORKS file.</param>
        /// <returns>Document type enum.</returns>
        public static swDocumentTypes_e GetSOLIDWORKSDocumentType(string path)
        {
            string extension = System.IO.Path.GetExtension(path);
            if (extension.ToLower().Contains("sldprt"))
            {
                return swDocumentTypes_e.swDocPART;
            }
            else if (extension.ToLower().Contains("sldasm"))
            {
                return swDocumentTypes_e.swDocASSEMBLY;
            }
            else if (extension.ToLower().Contains("slddrw"))
                return swDocumentTypes_e.swDocDRAWING;

            return swDocumentTypes_e.swDocNONE;
        }
    


        /// <summary>
        /// Gets the parent sheet of the specified feature.
        /// </summary>
        /// <param name="feature">Feature object.</param>
        /// <returns>Sheet object if found, null if not.</returns>
        public static Sheet GetParentSheet(this Feature feature)
        {

            var swFeature = default(Feature);
            swFeature = feature;
            while (swFeature != null)
            {
                if (swFeature.GetTypeName2() == "SHEET")
                {
                    return swFeature.GetSpecificFeature2() as Sheet;
                }
                swFeature = swFeature.GetOwnerFeature();
                
            }

            return null;

        }


        /// <summary>
        /// Returns an array of all references from Pack and Go.
        /// </summary>
        /// <param name="swModel">Model object.</param>
        /// <returns>An array of pathnames starting with the model's pathname.</returns>
        public static string[] GetAllReferences(this ModelDoc2 swModel)
            {
                PackAndGo packAndGo = swModel.Extension.GetPackAndGo();
                object swReferencesPathNamesObject = null;
                packAndGo.GetDocumentNames(out swReferencesPathNamesObject);
                string[] swReferencesPathNamesArray = swReferencesPathNamesObject as string[];
                return swReferencesPathNamesArray; 
            }

   

        /// <summary>
        /// Deletes all properties from a SOLIDWORKS document. Set toggle parameters to delete custom, configuration-specific or both.
        /// </summary>
        /// <param name="swModel">SOLIDWORKS Model object</param>
        /// <param name="configurationSpecificToggle">True to delete configuration-specific properties. Throws an exception if document is drawing and parameter is set to true.</param>
        /// <param name="customPropertiesToggle">True to delete custom properties.</param>
        public static void DeleteAllProperties(this ModelDoc2 swModel, bool configurationSpecificToggle, bool customPropertiesToggle)
            {
                if (swModel.GetType() == (int)swDocumentTypes_e.swDocDRAWING
                    && configurationSpecificToggle)
                    throw new Exception("Drawings do not contain configuration-specific properties. Unable to perform delete operation.");

                // delete custom properties 
                if (customPropertiesToggle)
                {
                    string[] customPropertyNames = swModel.Extension.CustomPropertyManager[""].GetNames() as string[];
                    if (customPropertyNames != null)
                    {
                        foreach (var customPropertyName in customPropertyNames)
                        {
                            swModel.Extension.CustomPropertyManager[""].Delete(customPropertyName);
                        }
                    }
                }
                // delete configuration specific properties 
                if ((swModel.GetType() == (int)swDocumentTypes_e.swDocPART
                   || swModel.GetType() == (int)swDocumentTypes_e.swDocASSEMBLY
                   ) && configurationSpecificToggle)
                {
                    string[] configurationNames = swModel.GetConfigurationNames() as string[];
                    foreach (var configurationName in configurationNames)
                    {
                        string[] customPropertyNames = swModel.Extension.CustomPropertyManager[configurationName].GetNames() as string[];
                        if (customPropertyNames != null)
                        {
                            foreach (var customPropertyName in customPropertyNames)
                            {
                                swModel.Extension.CustomPropertyManager[configurationName].Delete(customPropertyName);
                            }
                        }

                    }
                }
            }

        /// <summary>
        /// Returns whether specified component is a toolbox component or not.
        /// </summary>
        /// <param name="swComponent">Component object.</param>
        /// <returns>False if the specified component is a not toolbox one, true if not.</returns>
        public static bool IsNotToolBox(this Component2 swComponent)
            {
                if (swComponent.GetSuppression() == (int)swComponentSuppressionState_e.swComponentResolved)
                {
                    ModelDoc2 swModel = swComponent.GetModelDoc2() as ModelDoc2;
                    if (swModel == null)
                        return true;

                    var ToolboxType = (swToolBoxPartType_e)swModel.Extension.ToolboxPartType;
                    switch (ToolboxType)
                    {
                        case swToolBoxPartType_e.swNotAToolboxPart:
                            return true;
                            break;
                        case swToolBoxPartType_e.swToolboxStandardPart:
                            return false;
                            break;
                        case swToolBoxPartType_e.swToolboxCopiedPart:
                            return false;
                            break;
                        default:
                            return true;
                            break;
                    }
                }
                return true;

            }

        /// <summary>
        /// Returns an array of all children components from the specified component.
        /// </summary>
        /// <param name="swComponent">Component object.</param>
        /// <returns>MethodReturn of an array of Component2</returns>
        /// <remarks>If the method fails, the return object will return the found components in <see cref="MethodReturn{T}.Value"/>.</remarks>
        public static MethodReturn<Component2[]> GetAllChildren(this Component2 swComponent)
            {
                List<Component2> ComponentList = new List<Component2>();
                Action<Component2> GetChildrenRecursive = default(Action<Component2>);
                GetChildrenRecursive = (Component2 Component) =>
                {

                    var swComponentToAdded = default(Component2);

                    if (Component.IGetChildrenCount() > 0)
                    {
                        var objs = Component.GetChildren() as object[];
                        foreach (var obj in objs)
                        {
                            swComponentToAdded = obj as Component2;
                            GetChildrenRecursive(swComponentToAdded);
                        }
                    }
                    else
                        ComponentList.Add(Component);

                };

                try
                {
                    if (swComponent != null)
                        GetChildrenRecursive(swComponent);
                }
                catch (Exception e)
                {
                    return new MethodReturn<Component2[]>(ComponentList.ToArray(), e, true);
                }

                return new MethodReturn<Component2[]>(ComponentList.ToArray());

            }



            private static IEnumerable<Enum> GetFlags(Enum input)
            {
                foreach (Enum value in Enum.GetValues(input.GetType()))
                    if (input.HasFlag(value))
                        yield return value;
            }
        private static string[] GetDescriptions(IEnumerable<Enum> enums)
        {
            var list = new List<string>();
            foreach (var e in enums)
            {
                string error = GetEnumDescription(e);
                if (error != GetEnumDescription(swOpenError.swNoError) && error != GetEnumDescription(swOpenWarning.swNoError))
                list.Add(error);
            }
            return list.ToArray();
        }


        /// <summary>
        /// Opens the specified SOLIDWORKS document silently.
        /// </summary>
        /// <param name="swApp">SldWorks object.</param>
        /// <param name="filename">Complete pathname of the SOLIDWORKS document.</param>
        /// <param name="configurationName">Configuration name.</param>
        /// <returns>MethodReturn of ModelDoc2.</returns>
        public static MethodReturn<ModelDoc2> OpenDocument(this SldWorks swApp, string filename, string configurationName = "")
            { 
                swOpenError ErrorsEnum = default(swOpenError);
                swOpenWarning WarningsEnum = default(swOpenWarning);
                int Error = 0;
                int Warning = 0;
                swDocumentTypes_e type = default(swDocumentTypes_e);
                string extension = System.IO.Path.GetExtension(filename).ToLower();
            if (extension.Contains("prt"))
                type = swDocumentTypes_e.swDocPART;
            else if (extension.Contains("asm"))
                type = swDocumentTypes_e.swDocASSEMBLY;
            else if (extension.Contains("drw"))
                type = swDocumentTypes_e.swDocDRAWING;
            else
                throw CADSharpToolsPDMException.CreateInstance(CADSharpTools_e.CADSharpToolsPDMExceptionMessages_e.UnknownExtension);
                try
                {
                    ModelDoc2 Document = swApp.OpenDoc6(filename, (int)type, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, configurationName, ref Error, ref Warning) as ModelDoc2;
                    ErrorsEnum = (swOpenError)Error;
                    WarningsEnum = (swOpenWarning)Warning;

                var errors = GetDescriptions(GetFlags(ErrorsEnum));
               
                var warnings = GetDescriptions(GetFlags(WarningsEnum));

                if (errors.Length == 0)
                    errors = new string[] { "No errors." };


                if (warnings.Length == 0)
                    warnings = new string[] { "No warnings." };

                string message = string.Format("Errors:\r\n{0}\r\nWarnings:\r\n{1}\r\n", string.Join("\r\n-",errors), string.Join("\r\n-", warnings));

                if (Error != 0 && Warning != 0)
                    {
                          if (Document == null)
                            return new MethodReturn<ModelDoc2>(null, true, message);
                    }
                    else if (Error != 0 && Warning == 0)
                    {
                        if (Document == null)
                            return new MethodReturn<ModelDoc2>(null, true, message);


                    }
                    else if (Error == 0 && Warning != 0)
                    {
                        if (Document == null)
                            return new MethodReturn<ModelDoc2>(null, true, message);

                    }

                    if (Document == null)
                    {
                        new MethodReturn<ModelDoc2>(null, true, string.Format("Unable to open the document.{0}",message));
                    }
                    return new MethodReturn<ModelDoc2>(Document);
                }
                catch (Exception e)
                {
                    return new MethodReturn<ModelDoc2>(null, true, e.Message + " StackTrace: " + e.StackTrace.ToString());
                }
            }

      
        /// <summary>
        /// Opens the specified SOLIDWORKS document silently.
        /// </summary>
        /// <param name="swApp">SldWorks object.</param>
        /// <param name="filename">Complete pathname of the SOLIDWORKS document.</param>
        /// <returns>MethodReturn of ModelDoc2.</returns>
        public static Task<MethodReturn<ModelDoc2>> OpenDocumentAsync(this SldWorks swApp, string filename)
            {
                return Task.Run(() =>
                {
                    return OpenDocument(swApp, filename);
                });
            }

        /// <summary>
        /// get the entire selectByID2 (for a feature) that exists in a sub-assembly component
        /// </summary>
        /// <param name="selectByIDString">Subassembly selection string</param>
        /// <param name="rawString">Atlatl component hierarchy string</param>
        /// <param name="levelSeperator">Level separator</param>
        /// <param name="instanceSeperator">Instance separator</param>
        /// <returns></returns>
        private static string GetComponentFeatureSelectByID2String(string selectByIDString, string rawString, char levelSeperator, char instanceSeperator)
            {

                string[] rawLevels = rawString.Split(levelSeperator);
                if (rawLevels == null)
                    throw new Exception($"Failed to split levels of {rawString}");


                string featureName = rawLevels[rawLevels.Length - 1];
                // if there is one then the inserted component is a part
                if (rawLevels.Length == 1)
                    return featureName;

                // skip the last element to all levels
                string[] levels = rawLevels.ToList<string>().Reverse<string>().Skip<string>(1).Reverse<string>().ToArray();

                // create subassembly component object 
                var subAssembly = new Component();
                subAssembly.Name = levels[0];
                // add components 
                levels.ToList<string>().Skip(1).ToList<string>().ForEach((x) =>
                {
                    string[] arr = x.Split(instanceSeperator);
                    if (arr == null)
                        throw new Exception($"Failed to split level into a component name and an instance - String = {x} - Instance Seperator = {instanceSeperator} ");
                    if (arr.Length == 1)
                        throw new Exception($"Failed to split level into a component name and an instance - String = {x} - Instance Seperator = {instanceSeperator} ");

                    string componentName = string.Empty;
                    int componentInstance = 0;

                    // parse into instance number
                    bool ret = int.TryParse(arr[arr.Length - 1], out componentInstance);
                    if (ret == false)
                        throw new Exception($"Failed to parse string into an instance number - String = {x} - Instance Seperator = {instanceSeperator} ");
                    // get component name 
                    // concatenate in case the instance separator is used in the component name 
                    for (int i = 0; i <= arr.Length - 2; i++)
                    {
                        componentName = componentName + arr[i];
                    }

                    var newComponent = new Component() { Name = componentName, InstanceCount = componentInstance };
                    // enqueue component 
                    subAssembly.EnqueueComponent(newComponent);
                });

                string retString = string.Empty;
                var iteratorComponent = subAssembly;
                while (iteratorComponent.HasChild())
                {

                    retString = $"{retString}/{iteratorComponent.Child.GetFullName()}@{iteratorComponent.Name}";
                    iteratorComponent = iteratorComponent.Child;
                }

                string trimmedRetString = retString.Trim('/');
                if (string.IsNullOrWhiteSpace(trimmedRetString))
                    return $"{featureName}";
                else
                    return $"{featureName}@{selectByIDString}/{trimmedRetString}";

            }

            internal static string GetEnumDescription(Enum value)
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());

                DescriptionAttribute[] attributes =
                    (DescriptionAttribute[])fi.GetCustomAttributes(
                    typeof(DescriptionAttribute),
                    false);

                if (attributes != null &&
                    attributes.Length > 0)
                    return attributes[0].Description;
                else
                    return value.ToString();
            }
            private enum swOpenWarning
            {
                [Description("No Warning")]
                swNoError = 0,
                [Description("The document is already open. ")]
                swFileLoadWarning_AlreadyOpen = 128,
                [Description("The document was defined in the context of another existing document that is not loaded. ")]
                swFileLoadWarning_BasePartNotLoaded = 64,
                [Description("The document is opened silently and swOpenDocOptions_AutoMissingConfig is specified")]
                swFileLoadWarning_ComponentMissingReferencedConfig = 32768,
                [Description("Some dimensions are referenced to the models incorrectly; these dimensions are highlighted in red in the drawing.")]
                swFileLoadWarning_DimensionsReferencedIncorrectlyToModels = 16384,
                [Description("Warning appears if the internal ID of the document does not match the internal ID saved with the referencing document")]
                swFileLoadWarning_IdMismatch = 1,
                [Description("An attempt has been made to open an invisible document with a linked design table that was modified externally, and the design table cannot be updated because the document cannot be displayed; you must make the document visible to open it and update the design table.")]
                swFileLoadWarning_InvisibleDoc_LinkedDesignTableUpdateFail = 65536,
                [Description("The design table is missing")]
                swFileLoadWarning_MissingDesignTable = 131072,
                [Description("The document needs to be rebuilt")]
                swFileLoadWarning_NeedsRegen = 32,
                [Description("The document is read only")]
                swFileLoadWarning_ReadOnly = 2,
                [Description("The revolved feature dimensions were created in SOLIDWORKS 99 or earlier and are not synchronized with their corresponding dimensions in the sketch")]
                swFileLoadWarning_RevolveDimTolerance = 4096,
                [Description("The document is being used by another user")]
                swFileLoadWarning_SharingViolation = 4,
                [Description("The document is view only and a configuration other than the default configuration is set")]
                swFileLoadWarning_ViewOnlyRestrictions = 512



            }
        private enum swOpenError
            {
                [Description("No Error")]
                swNoError = 0,
                [Description("Unable to locate the file; the file is not loaded or the referenced file (that is, component) is suppressed")]
                swFileNotFoundError = 2,
                [Description(" A document with the same name is already open")]
                swFileWithSameTitleAlreadyOpen = 65536,
                [Description("The document was saved in a future version of SOLIDWORKS")]
                swFutureVersion = 8192,
                [Description("Another error was encountered (SOLIDWORKS Generic Error)")]
                swGenericError = 1,

                [Description("File type argument is not valid")]
                swInvalidFileTypeError = 1024,
                [Description("File encrypted by Liquid Machines")]
                swLiquidMachineDoc = 131072,
                [Description("File is open and blocked because the system memory is low, or the number of GDI handles has exceeded the allowed maximum")]
                swLowResourcesError = 262144,
                [Description("File contains no display data")]
                swNoDisplayData = 524288
            }
        }

        internal class Component
        {

            public int InstanceCount { get; set; }
            public string Name { get; set; }
            public Component Child { get; set; }
            public Component Parent { get; set; }

            public bool HasChild()
            {
                return Child == null ? false : true;
            }
            public bool HasParent()
            {
                return Parent == null ? false : true;
            }

            /// <summary>
            /// returns the full name of the component
            /// </summary>
            /// <returns></returns>
            public string GetFullName()
            {
                return $"{Name}-{InstanceCount}";
            }
            /// <summary>
            /// Enqueues a component to the lowest component and set parent
            /// </summary>
            /// <param name="component"></param>
            public void EnqueueComponent(Component component)
            {
                // get the lowest child 
                var lowestComponent = this.Child;
                while (lowestComponent != null)
                {
                    if (lowestComponent.Child != null)
                        lowestComponent = lowestComponent.Child;
                    else
                        break;
                }

                // if lowest component is null
                if (lowestComponent == null)
                {
                    this.Child = component;
                    component.Parent = this;
                }
                else
                {
                    lowestComponent.Child = component;
                    component.Parent = lowestComponent;
                }
            }
        }


    
}

