﻿using CADSharpTools.Core;
using SolidWorks.Interop.sldcostingapi;
using SolidWorks.Interop.sldworks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace CADSharpTools.SOLIDWORKS.Costing
{
    /// <summary>
    /// Extension class for the SOLIDWORKS costing API objects.
    /// </summary>
    public static class Extension
    {
        /// <summary>
        /// Defines costing analysis types.
        /// </summary>
        public enum CostingType
        {
            /// <summary>
            /// Machining
            /// </summary>
            [Description("Machining")]
            Machining,
            /// <summary>
            /// Plastic
            /// </summary>
            [Description("Plastic")]
            Plastic,
            /// <summary>
            /// Sheet Metal
            /// </summary>
            [Description("Sheet metal")]
            SheetMetal,

            /// <summary>
            /// 3D printing
            /// </summary>
            [Description("3D printing")]
            _3DPrinting
        }

        /// <summary>
        /// Gets the costing type for the specified costing object.
        /// </summary>
        /// <param name="swCostingAnalysis">Costing object.</param>
        /// <returns>Costing type as defined in <see cref="CostingType"/></returns>
        public static CostingType GetCostingType(this CostAnalysis swCostingAnalysis)
        {

            var specificInstances = new List<KeyValuePair<CostingType, object>>();
            specificInstances.Add(new KeyValuePair<CostingType, object>(CostingType.Machining, swCostingAnalysis.GetSpecificAnalysis() as CostAnalysisMachining));
            specificInstances.Add(new KeyValuePair<CostingType, object>(CostingType.Plastic, swCostingAnalysis.GetSpecificAnalysis() as CostAnalysisPlastic));
            specificInstances.Add(new KeyValuePair<CostingType, object>(CostingType.SheetMetal, swCostingAnalysis.GetSpecificAnalysis() as CostAnalysisSheetMetal));
            specificInstances.Add(new KeyValuePair<CostingType, object>(CostingType._3DPrinting, swCostingAnalysis.GetSpecificAnalysis() as CostAnalysis3dPrinting));


            var specificInstance = specificInstances.First((x) => x.Value != null);

            return specificInstance.Key;
        }
        /// <summary>
        /// Gets the material class name for the specified material name.
        /// </summary>
        /// <param name="costAnalysis">Machining costing object.</param>
        /// <param name="materialName">Material name.</param>
        /// <returns>costing type.</returns>
        public static string GetMaterialClassName(this CostAnalysisMachining costAnalysis, string materialName)
        {
            if (costAnalysis.GetMaterialClassesCount() > 0)
            {
                string[] materialClasses = costAnalysis.GetMaterialClasses() as string[];
                foreach (var materialClass in materialClasses)
                {
                    if (costAnalysis.GetMaterialCount(materialClass) > 0)
                    {

                        string[] materials = costAnalysis.GetMaterials(materialClass);

                        foreach (var material in materials)
                        {
                            if (material == materialName)
                                return materialClass;
                        }
                    }
                }
            }


            return string.Empty;
        }

        /// <summary>
        /// Runs costing operation and returns costing data.
        /// </summary>
        /// <param name="swModel">Model object.</param>
        /// <param name="pathName">Costing template pathname.</param>
        /// <returns>A dictionary of operations, their estimated time, combined time and cost.</returns>
        /// <remarks>If the part's material exists in the specified template, the method will set the material class and name accordingly and run costing twice to get an accurate costing estimation.</remarks>
        public static Dictionary<string, object> GetCostingProperties(this ModelDoc2 swModel, string pathName)
        {
            CostManager swCosting = default(CostManager);
            CostPart swCostingPart = default(CostPart);
            object swCostingModel = null;
            CostAnalysis swCostingAnalysis = default(CostAnalysis);
            CostFeature swCostingFeat = default(CostFeature);
            CostFeature swCostingSubFeat = default(CostFeature);
            CostFeature swCostingNextSubFeat = default(CostFeature);
            Dictionary<string, object> rets = new Dictionary<string, object>();
            ModelDocExtension swModelDocExt = default(ModelDocExtension);
            swModelDocExt = (ModelDocExtension)swModel.Extension;
            //Get CostingManager
            swCosting = (CostManager)swModelDocExt.GetCostingManager();
            swCostingModel = (object)swCosting.CostingModel;
            swCostingPart = (CostPart)swCostingModel;
            if (swCosting == null)
            {

                return rets;
            }
            // Get Costing part
            swCostingModel = (object)swCosting.CostingModel;
            swCostingPart = (CostPart)swCostingModel;
            // Create common Costing analysis
            swCostingAnalysis = swCostingPart.CreateCostAnalysis(pathName);
            if (swCostingAnalysis == null)
            {

                return new Dictionary<string, object>();
            }

            // get material 
            string configurationName = swModel.ConfigurationManager.ActiveConfiguration.Name;
            string sMatDB = string.Empty;
            PartDoc partDoc = swModel as PartDoc;
            string materialName = partDoc.GetMaterialPropertyName2(configurationName, out sMatDB);

           


            if (string.IsNullOrWhiteSpace(materialName) == false)
            {

                var costingType = swCostingAnalysis.GetCostingType();
                switch (costingType)
                {
                    case CostingType.Machining:
                        {
                            var machiningCostAnalysis = swCostingAnalysis.GetSpecificAnalysis() as CostAnalysisMachining;

                            string costingMaterialName = machiningCostAnalysis.CurrentMaterial;
                            if (costingMaterialName == materialName)
                                break;

                            string className = machiningCostAnalysis.GetMaterialClassName(materialName);
                            if (string.IsNullOrEmpty(className) == false)
                            {
                                machiningCostAnalysis.CurrentMaterialClass = className;
                                machiningCostAnalysis.CurrentMaterial = materialName;
                                swCostingAnalysis = (CostAnalysis)swCostingPart.CreateCostAnalysis(pathName);
                            }
                        }
                        break;
                    case CostingType.Plastic:
                        break;
                    case CostingType.SheetMetal:
                        {
                             
                        }
                        break;
                    case CostingType._3DPrinting:
                        break;
                    default:
                        break;
                }


            }

            // Get common Costing analysis data
            rets.Add("Total manufacturing cost", Math.Round(swCostingAnalysis.GetManufacturingCost(), 2));
            rets.Add("Material cost", Math.Round(swCostingAnalysis.GetMaterialCost(), 2));
            rets.Add("Total cost to charge", Math.Round(swCostingAnalysis.GetTotalCostToCharge(), 2));
            rets.Add("Total cost to manufacture", Math.Round(swCostingAnalysis.GetTotalCostToManufacture(), 2));
            rets.Add("Lot size", swCostingAnalysis.LotSize);
            rets.Add("Total quantity", swCostingAnalysis.TotalQuantity);
            swCostingFeat = (CostFeature)swCostingAnalysis.GetFirstCostFeature();
            while ((swCostingFeat != null))
            {
                Console.WriteLine($"Feature: {swCostingFeat.Name} Time:{swCostingFeat.CombinedTime.FormatTimeFromMinutes()}");

                double cost = Math.Round(swCostingFeat.CombinedCost, 2);
                if (cost > 0)
                {
                    rets.Add(swCostingFeat.Name.Replace(" ", "") + "-CombinedCost", cost);
                    double combinedTime = 0;
                    swCostingSubFeat = swCostingFeat.GetFirstSubFeature();
                    // has subfeatures 
                    bool hasSubFeatures = false;
                    while ((swCostingSubFeat != null))
                    {
                        Console.WriteLine($" Sub-Feature: {swCostingSubFeat.Name} Time:{swCostingSubFeat.CombinedTime.FormatTimeFromMinutes()}");
                        var swCostingSubSubFeat = swCostingSubFeat.GetFirstSubFeature();
                        while (swCostingSubSubFeat != null)
                        {
                            Console.WriteLine($"  SubSubFeature: {swCostingSubSubFeat.Name} Time:{swCostingSubSubFeat.CombinedTime.FormatTimeFromMinutes()}");

                            if (swCostingSubSubFeat.CombinedTime > 0)
                                combinedTime += swCostingSubSubFeat.CombinedTime;
                            hasSubFeatures = true;
                            swCostingSubSubFeat = swCostingSubSubFeat.GetNextFeature();
                        }

                        if (hasSubFeatures == false)
                        {

                            if (swCostingSubFeat.CombinedTime > 0)
                                combinedTime += swCostingSubFeat.CombinedTime;
                        }
                        swCostingNextSubFeat = (CostFeature)swCostingSubFeat.GetNextFeature();
                        swCostingSubFeat = null;
                        swCostingSubFeat = (CostFeature)swCostingNextSubFeat;
                        swCostingNextSubFeat = null;
                    }
                    // format time in hh:mm:ss
                    double timeInSeconds = combinedTime * 60;
                    if (timeInSeconds > 0)
                    {
                        string formattedTime = $"{TimeSpan.FromSeconds(timeInSeconds).Hours.ToString("D2")}:{TimeSpan.FromSeconds(timeInSeconds).Minutes.ToString("D2")}:{TimeSpan.FromSeconds(timeInSeconds).Seconds.ToString("D2")}";
                        rets.Add(swCostingFeat.Name.Replace(" ", "") + "-CombinedTime", formattedTime);
                    }
                }
                swCostingFeat = swCostingFeat.GetNextFeature() as CostFeature;
            }

            return rets;
        }
    }
}
