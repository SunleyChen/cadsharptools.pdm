﻿using CADSharpTools_e;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolidWorks.Interop.swconst;
namespace CADSharpTools.SOLIDWORKS
{
    /// <summary>
    /// Measure class contains utility functions for converting between unit systems.
    /// </summary>
    public static class Measure
    {
        /// <summary>
        /// Converts a length from one unit of measurement to another.
        /// </summary>
        /// <param name="from">Source unit.</param>
        /// <param name="to">Target unit.</param>
        /// <param name="value">value of the measurement in the source unit.</param>
        /// <returns>Converted value in the target unit.</returns>
        /// <exception cref="CADSharpTools.Commons.CADSharpToolsPDMException">Thrown when there is an incomplete implementation.</exception>
        public static double ConvertLength(swLengthUnit_e from, swLengthUnit_e to, double value)
        {
            switch (from)
            {
                case swLengthUnit_e.swMETER:
                    switch (to)
                    {
                        case swLengthUnit_e.swMETER:
                            return value;
                        case swLengthUnit_e.swMIL:
                            break;
                        case swLengthUnit_e.swINCHES:
                            return value / 0.0254;
                        case swLengthUnit_e.swFEET:
                            return 3.28084 * value;
                        default:
                            break;
                    }
                    break;
                case swLengthUnit_e.swMIL:
                    break;
                case swLengthUnit_e.swINCHES:
                    {
                        switch (to)
                        {
                            case swLengthUnit_e.swMETER:
                                return value * 0.0254;
                            case swLengthUnit_e.swMIL:
                                break;
                            case swLengthUnit_e.swINCHES:
                                return value; 
                            case swLengthUnit_e.swFEET:
                                return 0.0833333 * value;
                            default:
                                break;
                        }
                    }
                    break;
                case swLengthUnit_e.swFEET:
                    break;
                default:
                    break;
            }

            throw CADSharpTools.Exceptions.CADSharpToolsPDMException.CreateInstance(CADSharpTools_e.CADSharpToolsPDMExceptionMessages_e.UndefinedMeasurement);
        }



        /// <summary>
        /// Converts a mass from one unit of measurement to another.
        /// </summary>
        /// <param name="from">Source unit.</param>
        /// <param name="to">Target unit.</param>
        /// <param name="value">value of the measurement in the source unit.</param>
        /// <returns>Converted value in the target unit.</returns>
        /// <exception cref="CADSharpTools.Commons.CADSharpToolsPDMException">Thrown when there is an incomplete implementation.</exception>
        public static double ConvertMass(swUnitsMassPropMass_e from, swUnitsMassPropMass_e to, double value)
        {
            switch (from)
            {
                case swUnitsMassPropMass_e.swUnitsMassPropMass_Milligrams:
                    switch (to)
                    {
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Milligrams:
                            return value;
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Grams:
                            return value / 1000;
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Kilograms:
                            return value / 1000000;
                            
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Pounds:
                            return value / (2.2046 * 1000000);
                        default:
                            break;
                    }
                    break;
                case swUnitsMassPropMass_e.swUnitsMassPropMass_Grams:
                    switch (to)
                    {
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Milligrams:
                            return value * 1000;
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Grams:
                            return value;
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Kilograms:
                            return value / 1000;
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Pounds:
                            return value * 0.00220462;
                        default:
                            break;
                    }

                    break;
                case swUnitsMassPropMass_e.swUnitsMassPropMass_Kilograms:

                    switch (to)
                    {
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Milligrams:
                            return value * 1000 * 1000;
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Grams:
                            return value * 1000;
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Kilograms:
                            return value;
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Pounds:
                            return value * 2.20462;
                        default:
                            break;
                    }
                    break;
                case swUnitsMassPropMass_e.swUnitsMassPropMass_Pounds:
                    switch (to)
                    {
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Milligrams:
                            return 453592 * value; 
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Grams:
                            return 453.592 * value;
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Kilograms:
                            return value / 2.20462;
                        case swUnitsMassPropMass_e.swUnitsMassPropMass_Pounds:
                            return value;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            throw CADSharpTools.Exceptions.CADSharpToolsPDMException.CreateInstance(CADSharpTools_e.CADSharpToolsPDMExceptionMessages_e.UndefinedMeasurement);
        }
    }

   
}
