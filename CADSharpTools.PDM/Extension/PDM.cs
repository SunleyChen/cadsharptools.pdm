﻿using CADSharpTools_e;
using EPDM.Interop.epdm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using CADSharpTools.Core;
using CADSharpTools.Exceptions;
using CADSharpTools.Attributes;
using System.ComponentModel;

namespace CADSharpTools.PDM
{
    /// <summary>
    /// Utility class.
    /// </summary>
    public static class Utility
    {
        /// <summary>
        /// Gets an array of vault names.
        /// </summary>
        /// <param name="loggedIn">Only get vault user is logged to.</param>
        /// <returns>An array of string</returns>
        public static string[] GetVaultNames(bool loggedIn)
        {
            var vault = new EdmVault5();
            var vault11 = vault as IEdmVault11;
            var vaultViews = default(EdmViewInfo[]);
            vault11.GetVaultViews(out vaultViews, loggedIn);
            if (vaultViews != default(EdmViewInfo[]))
            {
                var names = new List<string>();
                Array.ForEach<EdmViewInfo>(vaultViews, x=> names.Add(x.mbsVaultName));
                return names.ToArray();
            }
            return new string[] { };
        }
    } 

    /// <summary>
    /// Extension class for SOLIDWORKS PDM API objects.
    /// </summary>
    public static class Extension
    {

        /// <summary>
        /// Attempt to login to the vault.
        /// </summary>
        /// <param name="vault">Vault object.</param>
        /// <param name="vaultName">Vault name.</param>
        /// <param name="parentHandle">Parent handle.</param>
        /// <returns></returns>
        public static MethodReturn<LoginRet_e> TryLoginAuto(this IEdmVault5 vault, string vaultName, int parentHandle)
        {
            try
            {
                vault.LoginAuto(vaultName, parentHandle);
            }
            catch (COMException e)
            {
                var comhResult = e.HResult;
                var members = Enum.GetValues(typeof(LoginRet_e));
                foreach (LoginRet_e member in members)
                {
                    var attributes = member.GetType().GetMember(member.ToString()).First().GetCustomAttributes(false);
                    var hRAttribute = Array.Find<object>(attributes, (x) => x is HResultAttribute) as HResultAttribute;
                    var description = Array.Find<object>(attributes, (x) => x is DescriptionAttribute) as DescriptionAttribute;
                    var hRHesult = hRAttribute.HResult;
                    if (hRHesult == comhResult)
                    {
                        return new MethodReturn<LoginRet_e>(member, new Exception(description.Description));
                    }
                }



            }

            return new MethodReturn<LoginRet_e>(LoginRet_e.S_OK);
        }

        /// <summary>
        /// Gets whether the file with the specified version and location needs to be rebuilt in its associated CAD program.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <param name="version">Version. 0 to get the one in the local cache.</param>
        /// <param name="folderId">Folder ID.</param>
        /// <returns>True if document needs regeneration false if not.</returns>
        /// <remarks>This method compare references to determine if the document needs rebuilding. More information, <externalLink>
        /// <linkText>
        /// check this SOLIDWORKS API thread.
        /// </linkText>
        /// <linkUri>
        /// https://forum.solidworks.com/thread/105368
        /// </linkUri>
        /// </externalLink></remarks>
        public static bool NeedsRegeneration2(this IEdmFile5 file, int version, int folderId)

        {

            if ((file as IEdmFile7).NeedsRegeneration(version, folderId))

            {

                return true;

            }



            IEdmReference5 reference = file.GetReferenceTree(folderId, version);



            return TraverseReference(reference);

        }



        private static bool TraverseReference(IEdmReference5 reference)

        {

            string prjName = "";

            IEdmPos5 pos = reference.GetFirstChildPosition(ref prjName, true, false, 0);

            while (!pos.IsNull)

            {

                IEdmReference5 child = reference.GetNextChild(pos);

                if (TraverseReference(child))

                {

                    return true;

                }



                IEdmFile5 refFile = child.File;

                int versionRef = child.VersionRef;

                int curVer = refFile.CurrentVersion;





                if (curVer != versionRef)

                {

                    return true;

                }

            }



            return false;

        }




        #region Miscellaneous 
        private static string GetPathName()
        {
            return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }
        /// <summary>
        /// Returns a setting node value from a xml configuration file. File must be stored in the CADSharpTools.PDM directory
        /// </summary>
        /// <param name="settingName"></param>
        /// <param name="configurationFileName"></param>
        /// <returns></returns>
        public static MethodReturn<string> GetConfigurationSetting(string settingName, string configurationFileName)
        {
            string configurationPathName = string.Format("{0}/{1}.xml", GetPathName(), configurationFileName);
            if (!System.IO.File.Exists(configurationPathName))
                return new MethodReturn<string>("", true, "Unable to find the configuration xml file.");
            string name = settingName;
            string value = string.Empty;
            try
            {
                string s = System.IO.File.ReadAllText(configurationPathName);
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(s);
                XmlNodeList resources = xml.SelectNodes("data/resource");
                SortedDictionary<string, string> dictionary = new SortedDictionary<string, string>();
                foreach (XmlNode node in resources)
                {
                    dictionary.Add(node.Attributes["key"].Value, node.InnerText);
                }
                value = dictionary[name];
            }
            catch (Exception e)
            {

                return new MethodReturn<string>("", true, "A fatal error has occurred while trying to get connection settings. Details: " + e.Message + System.Environment.NewLine + ". Please contact technical support.");

            }
            return new MethodReturn<string>(value);
        }

        #endregion

        /// <summary>
        /// Returns an array of referenced documents by the specified PDM file. 
        /// </summary>
        /// <param name="file">File object.</param>
        /// <param name="folder">Folder object.</param>
        /// <returns>Array of referenced documents pathnMES</returns>
        /// <remarks>
        /// <ul>
        /// <li>Structural members are ignored.</li>
        /// <li>Broken or missing references are ignored.</li>
        /// </ul>
        /// </remarks>
        public static string[] GetPDMFileReferencedDocuments(this IEdmFile5 file, IEdmFolder5 folder)
        {
            var Vault = file.Vault as IEdmVault5;

            IEdmReference5 reference = file.GetReferenceTree(folder.ID);
            Dictionary<string, string> filesDictionary = new Dictionary<string, string>();
            GetReferencesRecursively(Vault, reference, file.GetLocalPath(folder.ID), ref filesDictionary);

            var references = new List<string>();
            foreach (var item in filesDictionary)
                references.Add(item.Key);

            return references.ToArray();
        }
        /// <summary>
        /// Returns an array of IEdmReference5 object from a specified file.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <returns>An array of IEdmReference5 object.</returns>
        public static IEdmReference5[] GetAllReferences(this IEdmFile5 file)
        {
            var vault = file.Vault;
            var dictionary = new Dictionary<IEdmReference5, int>();
            var getReferences = default(Action<IEdmReference5, string, Dictionary<IEdmReference5, int>>);
            getReferences = new Action<IEdmReference5, string, Dictionary<IEdmReference5, int>>(
                (IEdmReference5 reference, string filePath, Dictionary<IEdmReference5, int> references)
                =>
                {


                    bool Top = false;
                    if (reference == null)
                    {
                        //This is the first time this function is called for this 
                        //reference tree; i.e., this is the root
                        Top = true;
                        IEdmFile5 File = null;
                        IEdmFolder5 ParentFolder = null;
                        File = vault.GetFileFromPath(filePath, out ParentFolder);
                        //Get the reference tree for this file
                        reference = File.GetReferenceTree(ParentFolder.ID);
                        getReferences(reference, "", references);
                    }
                    else
                    {
                        //Execute this code when this function is called recursively; 
                        //i.e., this is not the top-level IEdmReference in the tree
                        //Recursively traverse the references
                        IEdmPos5 pos = default(IEdmPos5);
                        IEdmReference9 Reference2 = (IEdmReference9)reference;
                        pos = Reference2.GetFirstChildPosition3("A", Top, true, (int)EdmRefFlags.EdmRef_File, "", 0);
                        IEdmReference5 @ref = default(IEdmReference5);
                        while ((!pos.IsNull))
                        {
                            @ref = reference.GetNextChild(pos);

                            dictionary.Add(@ref, 0);
                        }
                    }
                }
                );

            getReferences(null, GetLocalPath(file).Value, dictionary);


            return dictionary.Keys.ToList<IEdmReference5>().ToArray();
        }
        /// <summary>
        /// Gets or gets whether a reference shows in the bill of materials tab.
        /// </summary>
        /// <param name="reference">Reference object.</param>
        /// <param name="toggle">True to show the reference, false to hide.</param>
        /// <returns></returns>
        public static MethodReturn<bool> ShowInBillOfMaterials(this IEdmReference5 reference, bool toggle)
        {
            try
            {
                var reference8 = reference as IEdmReference8;
                if (toggle == false)
                    reference8.RefCountEdited = -1;
                else
                {
                    reference8.RefCountEdited = reference8.RefCount;
                }
            }
            catch (COMException comException)
            {
                return new MethodReturn<bool>(false, true, comException.Message);
            }
            catch (Exception exception)
            {
                return new MethodReturn<bool>(false, true, exception.Message);
            }

            return new MethodReturn<bool>(true);


        }
        private static void GetReferencesRecursively(IEdmVault5 Vault, IEdmReference5 Reference, string FilePath, ref Dictionary<string, string> FilesDictionary)
        {


            if (Vault == null)
                throw new NullReferenceException("Vault property is null.");
            bool Top = false;
            if (Reference == null)
            {
                //This is the first time this function is called for this 
                //reference tree; i.e., this is the root
                Top = true;
                IEdmFile5 File = null;
                IEdmFolder5 ParentFolder = null;
                File = Vault.GetFileFromPath(FilePath, out ParentFolder);
                //Get the reference tree for this file
                Reference = File.GetReferenceTree(ParentFolder.ID);


                GetReferencesRecursively(Vault, Reference, "", ref FilesDictionary);
            }
            else
            {
                //Execute this code when this function is called recursively; 
                //i.e., this is not the top-level IEdmReference in the tree
                //Recursively traverse the references
                IEdmPos5 pos = default(IEdmPos5);
                IEdmReference9 Reference2 = (IEdmReference9)Reference;
                pos = Reference2.GetFirstChildPosition3("A", Top, true, (int)EdmRefFlags.EdmRef_File, "", 0);
                IEdmReference5 @ref = default(IEdmReference5);
                while ((!pos.IsNull))
                {
                    @ref = Reference.GetNextChild(pos);

                    // check if file exists 
                    if (System.IO.File.Exists(@ref.FoundPath))
                    {
                        // check if file exists 
                        if (FilesDictionary.ContainsKey(@ref.FoundPath) == false)
                        {
                            FilesDictionary.Add(@ref.FoundPath, default(int).ToString());
                            GetReferencesRecursively(Vault, @ref, "", ref FilesDictionary);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Returns the local path from the PDM file object.
        /// </summary>
        /// <param name="file">File object</param>
        /// <returns>MethodReturn of the local path.</returns>
        /// <remarks>The method does not require the PDM object ID of the file's folder as opposed to the built-in <see cref="IEdmFile5.GetLocalPath(int)"></see>.</remarks>
        /// <example>
        /// This example shows how to print the local path of the first file found at the root of folder of a PDM vault.
        /// <code>
        ///using CADSharpTools.Commons;
        ///using EPDM.Interop.epdm;
        ///using System;
        ///using System.Diagnostics;
        ///namespace CADSharpTools.PDM.Tests
        ///{
        ///    class Test
        ///    {
        ///        public enum EdmLoginFlags
        ///        {
        ///            Nothing,
        ///            WebClient
        ///        }
        ///        [STAThread]
        ///        static void Main(string[] args)
        ///        {
        ///            //create vault object 
        ///            var swPDMVault = new EdmVault5();
        ///            var swPDMVaultEx = swPDMVault as IEdmVault13;
        ///            //required per PDM EULA in out-of-process applications
        ///            swPDMVaultEx.LoginEx("Admin", "****", "CADSharp LLC", (int)EdmLoginFlags.Nothing);
        ///            // get root folder 
        ///            var swPDMRootFolder = swPDMVaultEx.RootFolder;
        ///            // get first file position
        ///            var swPDMFirstFilePos = swPDMRootFolder.GetFirstFilePosition();
        ///            if (swPDMFirstFilePos.IsNull == false)
        ///            {
        ///                var swPDMFirstFile = swPDMRootFolder.GetNextFile(swPDMFirstFilePos);
        ///                // debug print local path
        ///                var localPathRet = swPDMFirstFile.GetLocalPath();
        ///                if (localPathRet.IsError == false)
        ///                    Debug.Print($"First local path: {localPathRet.Value}");
        ///                else
        ///                    Debug.Print($"Error occurred while getting local path : {localPathRet.Error}");
        ///            }
        ///            else
        ///            {
        ///                Debug.Print($"Vault has no files at the root folder.");
        ///            }
        ///        }
        ///    }
        ///}
        /// </code>
        /// </example>
        /// <exception cref="CADSharpToolsPDMException" >Thrown when unable to get the parent folder of the specified file object.</exception>
        /// <exception cref="NullReferenceException" >Thrown when file parameter is null.</exception>
        public static MethodReturn<string> GetLocalPath(this IEdmFile5 file)
        {
            try
            {
                if (file == null)
                    throw new NullReferenceException("file parameter cannot be null.");


                var folder = default(IEdmFolder5);
                var Pos = file.GetFirstFolderPosition();
                while (!Pos.IsNull)
                {
                    folder = file.GetNextFolder(Pos);
                    if (folder != null)
                        break;
                }

                if (folder == null)
                {
                    throw CADSharpToolsPDMException.CreateInstance(CADSharpToolsPDMExceptionMessages_e.UnableToGetFolderOfFile);
                }
                return new MethodReturn<string>(file.GetLocalPath(folder.ID));
            }
            catch (Exception e)
            {
                return new MethodReturn<string>(string.Empty, true, e.Message);
            }



        }
        /// <summary>
        /// Returns the folder object from the specified file otherwise return null and catch any thrown exceptions.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <returns>Folder object.</returns>
        /// <exception cref="NullReferenceException" >Thrown when file parameter is null.</exception>
        public static MethodReturn<IEdmFolder5> TryGetFolder(this IEdmFile5 file)
        {
            try
            {
                if (file == null)
                    throw new NullReferenceException("file parameter cannot be null.");


                var folder = default(IEdmFolder5);
                var Pos = file.GetFirstFolderPosition();
                while (!Pos.IsNull)
                {
                    folder = file.GetNextFolder(Pos);
                    if (folder != null)
                        break;
                }

                if (folder == null)
                {
                    throw CADSharpToolsPDMException.CreateInstance(CADSharpToolsPDMExceptionMessages_e.UnableToGetFolderOfFile);
                }
                return new MethodReturn<IEdmFolder5>(folder);
            }
            catch (Exception e)
            {
                return new MethodReturn<IEdmFolder5>(default(IEdmFolder5), e, true);
            }



        }
        /// <summary>
        /// Returns the folder object from the specified file object.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <returns>Folder object.</returns>
        /// <exception cref="NullReferenceException" >Thrown when<ul>
        /// <li>
        ///  file parameter is null.
        /// </li>
        /// <li>
        ///  The specified file has no local copy.
        /// </li>
        /// </ul></exception>
        public static IEdmFolder5 GetFolder(this IEdmFile5 file)
        {

            if (file == null)
                throw new NullReferenceException("file parameter cannot be null.");


            var folder = default(IEdmFolder5);
            var Pos = file.GetFirstFolderPosition();

            if (Pos.IsNull)
            {
                throw CADSharpToolsPDMException.CreateInstance(CADSharpToolsPDMExceptionMessages_e.NoLocalCopy);
            }


            while (!Pos.IsNull)
            {
                folder = file.GetNextFolder(Pos);
                if (folder != null)
                    break;
            }

            if (folder == null)
            {
                throw CADSharpToolsPDMException.CreateInstance(CADSharpToolsPDMExceptionMessages_e.UnableToGetFolderOfFile);
            }


            return folder;




        }



        /// <summary>
        /// Returns the SOLIDWORKS drawing that exists in the same directory and has the same filename as the specified PDM file. 
        /// </summary>
        /// <param name="file">File object.</param>
        /// <returns>MethodReturn of PDM file object.</returns>
        /// <remarks>If the drawing file object does not exist, the method return a <see cref="MethodReturn{IEdmFile5}"/> with <see cref="MethodReturn{T}.Value"/> as null.</remarks>
        /// <exception cref="CADSharpToolsPDMException" >Thrown when unable to get the parent folder of the specified file object.</exception>
        /// <exception cref="NullReferenceException" >Thrown when file parameter is null.</exception>
        public static MethodReturn<IEdmFile5> GetDrawing(this IEdmFile5 file)
        {
            try
            {
                if (file == null)
                    throw new NullReferenceException("file parameter cannot be null.");
                string localPath = file.GetLocalPath().Value;
                string fileName = System.IO.Path.GetFileNameWithoutExtension(localPath);
                var folder = default(IEdmFolder5);
                var Pos = file.GetFirstFolderPosition();
                while (!Pos.IsNull)
                {
                    folder = file.GetNextFolder(Pos);
                    if (folder != null)
                        break;
                }

                if (folder == null)
                {
                    throw CADSharpToolsPDMException.CreateInstance(CADSharpToolsPDMExceptionMessages_e.UnableToGetFolderOfFile);
                }
                IEdmFile5 drawing = default(IEdmFile5);
                IEdmPos5 pos = folder.GetFirstFilePosition();
                while (pos.IsNull == false)
                {
                    string drawinglocalPath = drawing.GetLocalPath().Value;
                    drawing = folder.GetNextFile(pos);
                    string drawingName = System.IO.Path.GetFileNameWithoutExtension(drawinglocalPath);
                    if (drawingName.ToLower() == fileName.ToLower() && System.IO.Path.GetExtension(drawinglocalPath).ToLower() == ".slddrw")
                    {
                        return new MethodReturn<IEdmFile5>(drawing);
                    }
                }

                return new MethodReturn<IEdmFile5>(null, true, $"Could not find drawing with the same as {fileName}.");
            }
            catch (Exception e)
            {
                return new MethodReturn<IEdmFile5>(null, e, true);
            }



        }
        /// <summary>
        /// Returns an array of all vault variable names.
        /// </summary>
        /// <param name="vault">Vault object.</param>
        /// <returns>Array of variable names</returns>
        public static MethodReturn<string[]> GetVaultVariableNames(this IEdmVault5 vault)
        {

            try
            {
                var variables = new List<string>();
                if (vault == null)
                    throw new NullReferenceException("vault parameter cannot be null.");

                var variable = default(IEdmVariable5);
                var variableMgr = vault as IEdmVariableMgr5;
                var pos = variableMgr.GetFirstVariablePosition();
                while (pos.IsNull == false)
                {
                    variable = variableMgr.GetNextVariable(pos);
                    variables.Add(variable.Name);
                }


                return new MethodReturn<string[]>(variables.ToArray());
            }
            catch (COMException e)
            {
                return new MethodReturn<string[]>(new string[] { }, e, true);
            }
        }
        /// <summary>
        /// Returns an array of all users in the specified vault.
        /// </summary>
        /// <param name="vault">Vault object.</param>
        /// <returns>MethodReturn of IEdmUser5[].</returns>
        /// <remarks>This method catches all COMExceptions and reports them in the Exception property of the return type.</remarks>
        public static MethodReturn<IEdmUser5[]> GetAllUsers(this IEdmVault5 vault)
        {

            var userIterator = default(IEdmUser5);
            var userList = new List<IEdmUser5>();
            var userMgr = vault as IEdmUserMgr5;
            try
            {
                var userPos = userMgr.GetFirstUserPosition() as IEdmPos5;
                while (userPos.IsNull == false)
                {
                    userIterator = userMgr.GetNextUser(userPos);
                    if (userIterator != null)
                        userList.Add(userIterator);
                }

            }
            catch (COMException e)
            {

                return new MethodReturn<IEdmUser5[]>(userList.ToArray(), e, true);
            }


            return new MethodReturn<IEdmUser5[]>(userList.ToArray());
        }


        /// <summary>
        /// Returns an array of all workflows in the specified vault.
        /// </summary>
        /// <param name="vault">Vault object.</param>
        /// <returns>MethodReturn of IEdmWorkflow6[].</returns>
        /// <remarks>This method catches all COMExceptions and reports them in the Exception property of the return type.</remarks>
        public static MethodReturn<IEdmWorkflow6[]> GetAllWorkflows(this IEdmVault5 vault)
        {

            var workflowIterator = default(IEdmWorkflow6);
            var workflowList = new List<IEdmWorkflow6>();
            var workflowMgr = vault as IEdmWorkflowMgr6;
            try
            {
                var workflowPos = workflowMgr.GetFirstWorkflowPosition() as IEdmPos5;
                while (workflowPos.IsNull == false)
                {
                    workflowIterator = workflowMgr.GetNextWorkflow(workflowPos);
                    if (workflowIterator != null)
                        workflowList.Add(workflowIterator);
                }

            }
            catch (COMException e)
            {

                return new MethodReturn<IEdmWorkflow6[]>(workflowList.ToArray(), e, true);
            }


            return new MethodReturn<IEdmWorkflow6[]>(workflowList.ToArray());
        }

        /// <summary>
        /// Returns an array of all workflow transitions from the specified workflow.
        /// </summary>
        /// <param name="vault">Workflow object.</param>
        /// <returns>MethodReturn of IEdmTransition5[].</returns>
        /// <remarks>This method catches all COMExceptions and reports them in the Exception property of the return type.</remarks>
        public static MethodReturn<IEdmTransition5[]> GetAllWorkflowTransitions(this IEdmWorkflow6 workflow)
        {

            var transition = default(IEdmTransition5);
            var transitions = new List<IEdmTransition5>();
            try
            {
                var workflowTransitionPos = workflow.GetFirstTransitionPosition() as IEdmPos5;
                while (workflowTransitionPos.IsNull == false)
                {
                    transition = workflow.GetNextTransition(workflowTransitionPos);
                    if (transition != null)
                        transitions.Add(transition);
                }

            }
            catch (COMException e)
            {

                return new MethodReturn<IEdmTransition5[]>(transitions.ToArray(), e, true);
            }


            return new MethodReturn<IEdmTransition5[]>(transitions.ToArray());
        }


        /// <summary>
        /// Returns an array of all workflow states from the specified workflow.
        /// </summary>
        /// <param name="vault">Workflow object.</param>
        /// <returns>MethodReturn of IEdmState5[].</returns>
        /// <remarks>This method catches all COMExceptions and reports them in the Exception property of the return type.</remarks>
        public static MethodReturn<IEdmState5[]> GetAllWorkflowStates(this IEdmWorkflow6 workflow)
        {

            var State = default(IEdmState5);
            var States = new List<IEdmState5>();
            try
            {
                var workflowStatePos = workflow.GetFirstStatePosition() as IEdmPos5;
                while (workflowStatePos.IsNull == false)
                {
                    State = workflow.GetNextState(workflowStatePos);
                    if (State != null)
                        States.Add(State);
                }

            }
            catch (COMException e)
            {

                return new MethodReturn<IEdmState5[]>(States.ToArray(), e, true);
            }


            return new MethodReturn<IEdmState5[]>(States.ToArray());
        }


        /// <summary>
        /// Returns the full name of the specified user. 
        /// </summary>
        /// <param name="user">IEdmUser5 object.</param>
        /// <returns>Full name of the user.</returns>
        public static string GetFullName(this IEdmUser5 user)
        {
            var user6 = user as IEdmUser6;
            return user6.FullName;

        }



        /// <summary>
        /// Returns an array of all user groups in the specified vault.
        /// </summary>
        /// <param name="vault">Vault object.</param>
        /// <returns>MethodReturn of IEdmUserGroup5[].</returns>
        /// <remarks>This method catches all COMExceptions and reports them in the Exception property of the return type.</remarks>
        public static MethodReturn<IEdmUserGroup5[]> GetAllUserGroups(this IEdmVault5 vault)
        {

            var userIterator = default(IEdmUserGroup5);
            var usergroupList = new List<IEdmUserGroup5>();
            var userMgr = vault as IEdmUserMgr5;
            try
            {
                var usergroupPos = userMgr.GetFirstUserGroupPosition() as IEdmPos5;
                while (usergroupPos.IsNull == false)
                {
                    userIterator = userMgr.GetNextUserGroup(usergroupPos);
                    if (userIterator != null)
                        usergroupList.Add(userIterator);
                }

            }
            catch (COMException e)
            {

                return new MethodReturn<IEdmUserGroup5[]>(usergroupList.ToArray(), e, true);
            }

            return new MethodReturn<IEdmUserGroup5[]>(usergroupList.ToArray());
        }
        /// <summary>
        /// Returns an array of PDM files.
        /// </summary>
        /// <param name="folder">Folder object.</param>
        /// <param name="includeSubFolders">True to include subfolders, false for top level only.</param>
        /// <returns>MedthodReturn of IEdmFile5[]</returns>
        public static MethodReturn<IEdmFile5[]> GetAllFiles(this IEdmFolder5 folder, bool includeSubFolders = false)
        {
            var fileList = new List<IEdmFile5>();
            var foldersList = new List<IEdmFolder5>();
            foldersList.Add(folder);

            Action<List<IEdmFolder5>, IEdmFolder5, bool> traverseSubFolder = default(Action<List<IEdmFolder5>, IEdmFolder5, bool>);
            traverseSubFolder = (List<IEdmFolder5> folderList, IEdmFolder5 Folder, bool _includeSubFolders)
            =>
            {
                IEdmFolder5 subFolder = default(IEdmFolder5);
                IEdmPos5 pos = Folder.GetFirstSubFolderPosition();
                while (pos.IsNull == false)
                {
                    if (_includeSubFolders == false)
                        return;

                        subFolder = Folder.GetNextSubFolder(pos);
                    folderList.Add(subFolder);
                        traverseSubFolder(folderList, subFolder, _includeSubFolders);
                }
            };

            Func<IEdmFolder5, IEdmFile5[]> GetAllFiles = default(Func<IEdmFolder5, IEdmFile5[]>);
            GetAllFiles = (IEdmFolder5 Folder)
            =>
            {
                var filesList = new List<IEdmFile5>();
                IEdmFile5 File = default(IEdmFile5);
                IEdmPos5 pos = Folder.GetFirstFilePosition();
                while (pos.IsNull == false)
                {
                    File = Folder.GetNextFile(pos);
                    filesList.Add(File);
                }

                return filesList.ToArray();
            };
            // get folders 
            traverseSubFolder(foldersList, folder, includeSubFolders);

            foreach (var folderItem in foldersList)
            {
                fileList.AddRange(GetAllFiles(folderItem));
            }


            return new MethodReturn<IEdmFile5[]>(fileList.ToArray());
        }


        /// <summary>
        /// Returns an array of PDM subfolders.
        /// </summary>
        /// <param name="folder">Folder object.</param>
        /// <param name="includeSubFolders">True to include all folders, false for top level subfolders only.</param>
        /// <returns>IEdmFolder5[]</returns>
        public static MethodReturn<IEdmFolder5[]> GetAllFolders(this IEdmFolder5 folder, bool includeSubFolders = false)
        {
            var FolderList = new List<IEdmFolder5>();
            Action<List<IEdmFolder5>, IEdmFolder5, bool> traverseSubFolder = default(Action<List<IEdmFolder5>, IEdmFolder5, bool>);
            traverseSubFolder = (List<IEdmFolder5> folderList, IEdmFolder5 Folder, bool _includeSubFolders)
            =>
            {
                IEdmFolder5 subFolder = default(IEdmFolder5);
                IEdmPos5 pos = Folder.GetFirstSubFolderPosition();
                while (pos.IsNull == false)
                {
                    subFolder = Folder.GetNextSubFolder(pos);
                    folderList.Add(subFolder);
                    if (_includeSubFolders == false)
                        continue;
                    traverseSubFolder(folderList, subFolder, _includeSubFolders);
                }
            };
            // get folders 
            traverseSubFolder(FolderList, folder, includeSubFolders);
            return new MethodReturn<IEdmFolder5[]>(FolderList.ToArray());
        }

        /// <summary>
        /// Returns an array of PDM files. If canceled, method returns found files. 
        /// </summary>
        /// <param name="folder">Folder object.</param>
        /// <param name="includeSubFolders">True to include sub-folders, false for top level only.</param>
        /// <param name="cancellationToken">CancellationToken object.</param>
        /// <returns>MedthodReturn of IEdmFile5[]</returns>
        /// <remarks>This method supports cancellation. Use this method for directory that contain a large number of files.</remarks>
        public static Task<MethodReturn<IEdmFile5[]>> GetAllFilesAsync(this IEdmFolder5 folder, CancellationToken cancellationToken, bool includeSubFolders = false)
        {
            return Task.Run(() => {
                var fileList = new List<IEdmFile5>();
                var foldersList = new List<IEdmFolder5>();
                foldersList.Add(folder);

                Action<List<IEdmFolder5>, IEdmFolder5, bool> traverseSubFolder = default(Action<List<IEdmFolder5>, IEdmFolder5, bool>);
                traverseSubFolder = (List<IEdmFolder5> folderList, IEdmFolder5 Folder, bool _includeSubFolders)
                =>
                {

                    if (cancellationToken.IsCancellationRequested)
                    {
                        return;
                    }

                    IEdmFolder5 subFolder = default(IEdmFolder5);
                    IEdmPos5 pos = Folder.GetFirstSubFolderPosition();
                    while (pos.IsNull == false)
                    {
                        if (cancellationToken.IsCancellationRequested)
                        {
                            return;
                        }
                        if (_includeSubFolders == false)
                            return;

                        subFolder = Folder.GetNextSubFolder(pos);
                        folderList.Add(subFolder);
                            traverseSubFolder(folderList, subFolder, _includeSubFolders);
                    }
                };

                Func<IEdmFolder5, IEdmFile5[]> GetAllFiles = default(Func<IEdmFolder5, IEdmFile5[]>);
                GetAllFiles = (IEdmFolder5 Folder)
                =>
                {
                    var filesList = new List<IEdmFile5>();

                    if (cancellationToken.IsCancellationRequested)
                    {
                        return filesList.ToArray();
                    }


                    IEdmFile5 File = default(IEdmFile5);
                    IEdmPos5 pos = Folder.GetFirstFilePosition();
                    while (pos.IsNull == false)
                    {
                        if (cancellationToken.IsCancellationRequested)
                        {
                            return filesList.ToArray();
                        }
                        File = Folder.GetNextFile(pos);
                        filesList.Add(File);
                    }

                    return filesList.ToArray();
                };
                // get folders 
                traverseSubFolder(foldersList, folder, includeSubFolders);

                foreach (var folderItem in foldersList)
                {
                    fileList.AddRange(GetAllFiles(folderItem));
                }


                return new MethodReturn<IEdmFile5[]>(fileList.ToArray());
            });
        }
        /// <summary>
        /// Undo a check out operation on the specified file.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <param name="parentWnd">Handle of the parent window.</param>
        /// <returns>Method return of true or false. True if the undo check out operation was successful, false if not.</returns>
        /// <exception cref="NullReferenceException" >Thrown when file parameter is null.</exception>
        public static MethodReturn<bool> UndoCheckOut(this IEdmFile5 file, int parentWnd = 0)
        {
            if (file == null)
                throw new NullReferenceException("file parameter cannot be null");
            try
            {
                file.UndoLockFile(parentWnd, true);
                return new MethodReturn<bool>(true);
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                return new MethodReturn<bool>(false, true, ex.Message);
            }
        }
        /// <summary>
        /// Returns the most recent modified date time for the specified file. 
        /// </summary>
        /// <param name="file">File object.</param>
        /// <returns>MethodReturn of DateTime.</returns>
        /// <exception cref="NullReferenceException" >Thrown when file parameter is null.</exception>
        /// <remarks>File must be cached.</remarks>
        public static MethodReturn<DateTime> GetFileLastModifiedDateTime(this IEdmFile5 file)
        {
            if (file == null)
                throw new NullReferenceException("file parameter cannot be null");
            try
            {
                string filePath = file.Name;
                var dateTime = (DateTime)file.GetLocalFileDate(filePath);

                    return new MethodReturn<DateTime>(dateTime);
             }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                return new MethodReturn<DateTime>(DateTime.MinValue, true, ex.Message);
            }

        }
        /// <summary>
        /// Returns the most recent check in date time for the specified file.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <returns>MethodReturn of DateTime.</returns>
        /// <exception cref="NullReferenceException" >Thrown when file parameter is null.</exception>
        public static MethodReturn<DateTime> GetLastCheckInDateTime(this IEdmFile5 file)
        {
            if (file == null)
                throw new NullReferenceException("file parameter cannot be null");
            try
            {
                List<DateTime> dates = new List<DateTime>();
                IEdmEnumeratorVersion5 enumVersion;
                enumVersion = (IEdmEnumeratorVersion5)file;
                IEdmPos5 pos = default(IEdmPos5);
                pos = enumVersion.GetFirstVersionPosition();
                IEdmVersion5 version = default(IEdmVersion5);
                while (!pos.IsNull)
                {
                    version = enumVersion.GetNextVersion(pos);
                    dates.Add((DateTime)version.VersionDate);
                }

                dates.Sort();
                if (dates.Count > 0)
                    return new MethodReturn<DateTime>(dates[dates.Count - 1]);
                else
                    return new MethodReturn<DateTime>(DateTime.MinValue, "Could find any date.");
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                return new MethodReturn<DateTime>(DateTime.MinValue, true, ex.Message);
            }

        }

        /// <summary>
        /// Check the specified file into the vault using the default behavior.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <param name="Phwnd">Parent window handle.</param>
        /// <param name="comment">comment.</param>
        /// <param name="lockOptions">Sets the options of the check in operations.</param>
        /// <returns>MethodReturn of boolean. True if the operation is successful, false if not.</returns>
        /// <exception cref="NullReferenceException" >Thrown when file parameter is null.</exception>
        public static MethodReturn<bool> CheckIn(this IEdmFile5 file, int Phwnd = 0, EdmUnlockFlag lockOptions = EdmUnlockFlag.EdmUnlock_Simple, string comment = "")
        {
            if (file == null)
                throw new NullReferenceException("file parameter cannot be null");
            try
            {
               
                file.UnlockFile(Phwnd, comment, (int)lockOptions, null);
                return new MethodReturn<bool>(true);
            }
            catch (COMException e)
            {
                return new MethodReturn<bool>(false, true, e.Message);
            }


        }
        /// <summary>
        /// Check out the specified file from the vault using the default behavior. 
        /// </summary>
        /// <param name="file">File object.</param>
        /// <param name="Phwnd">Parent window handle.</param>
        /// <param name="comment">Comment.</param>
        /// <returns>Method return of boolean. True if the check in operation was successful, false if not.</returns>
        /// <exception cref="CADSharpToolsPDMException" >Thrown when unable to get the parent folder of the specified file object.</exception>
        /// <exception cref="NullReferenceException" >Thrown when file parameter is null.</exception>
        public static MethodReturn<bool> CheckOut(this IEdmFile5 file, int Phwnd = 0, string comment = "")
        {
            if (file == null)
                throw new NullReferenceException("file parameter cannot be null");
            try
            {
                var vault = file.Vault as IEdmVault5;
                var userManager = vault as IEdmUserMgr5;
                var currentLoggedUser = userManager.GetLoggedInUser();
                // verify if file is checked out 
                if (file.IsLocked)
                {
                    int fileOwningUserID = file.LockedByUserID;
                    if (fileOwningUserID == currentLoggedUser.ID)
                        return new MethodReturn<bool>(true);
                    else
                        return new MethodReturn<bool>(false, true, "File is checked out by another user. User name is " + file.LockedByUser.Name);
                }



                var folder = default(IEdmFolder5);
                var Pos = file.GetFirstFolderPosition();
                while (!Pos.IsNull)
                {
                    folder = file.GetNextFolder(Pos);
                    if (folder != null)
                        break;
                }

                if (folder == null)
                {
                    throw CADSharpToolsPDMException.CreateInstance(CADSharpToolsPDMExceptionMessages_e.UnableToGetFolderOfFile);
                }

                file.LockFile(folder.ID, Phwnd, (int)EdmLockFlag.EdmLock_Simple);
                return new MethodReturn<bool>(true);
            }
            catch (COMException e)
            {
                return new MethodReturn<bool>(false, true, e.Message);
            }


        }


        /// <summary>
        /// Gets the value of the vault variable for the specified file.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <param name="variable">Vault variable name.</param>
        /// <param name="configurationName">Configuration name. Default is "@".</param>
        ///<remarks>Use this method to get data from files that are not cached (have no local copy) instead of using <see cref="GetDataCardVariable(IEdmFile5, string, string)"/>.</remarks>
        /// <example> 
        ///  This example shows how to login to the vault and get the value of a vault variable called "Part Number" for the first file.
        /// <code>        
        ///public enum EdmLoginFlags
        ///{
        ///    Nothing,
        ///    WebClient
        ///}
        ///
        ///public static void Example()
        ///{
        ///    //create vault object 
        ///    var swPDMVault = new EdmVault5();
        ///    var swPDMVaultEx = swPDMVault as IEdmVault13;
        ///
        ///    //required per PDM EULA in out-of-process applications              
        ///    swPDMVaultEx.LoginEx("Admin", "****", "CADSharp LLC", (int)EdmLoginFlags.Nothing);
        ///
        ///    // get root folder 
        ///    var swPDMRootFolder = swPDMVaultEx.RootFolder;
        ///
        ///    // get first file position
        ///    var swPDMFirstFilePos = swPDMRootFolder.GetFirstFilePosition();
        ///    if (swPDMFirstFilePos.IsNull == false)
        ///    {
        ///        var swPDMFirstFile = swPDMRootFolder.GetNextFile(swPDMFirstFilePos);
        ///
        ///        // get the part number of the first file 
        ///        var variableReturn = swPDMFirstFile.GetVariableFromDb("Part Number");
        ///        if (variableReturn.IsError)
        ///        {
        ///            Debug.Print(variableReturn.Error);
        ///            return;
        ///        }
        ///
        ///        var partNumberValue = variableReturn.Value.ToString();
        ///        Debug.Print($"Part Number variable value: {partNumberValue}");
        ///    }
        ///    else
        ///    {
        ///        Debug.Print($"Vault has no files at the root folder.");
        ///    }
        ///}
        ///</code>
        ///</example>
        /// <exception cref="CADSharpToolsPDMException" >Thrown when unable to get the parent folder of the specified file object.</exception>
        public static MethodReturn<object> GetVariableFromDb(this IEdmFile5 file, string variable, string configurationName = "@")
        {
            if (file == null)
                throw new ArgumentNullException("File parameter is null");



            // get the variable object
            IEdmEnumeratorVariable7 enumVariable = default(IEdmEnumeratorVariable7);
            enumVariable = (file as IEdmFile7).GetEnumeratorVariable() as IEdmEnumeratorVariable7;
            // Value must be an object 
            var Value = new Object();
            try
            {
                if ((file.IsSldAssembly() || file.IsSldPart()) == false)
                    configurationName = null;
                bool ret = true;
                ret = enumVariable.GetVarFromDb(variable, configurationName, out Value);
                var enumVariable8 = enumVariable as IEdmEnumeratorVariable8;
                enumVariable8.CloseFile(true);

                if (ret == false)
                    return new MethodReturn<object>(null, new Exception($"The variable {variable} was not found."));
                
            }
            catch (COMException e)
            {
                return new MethodReturn<object>(null, e, true);
            }

            // return variable's variable 
            return new MethodReturn<object>(Value);
        }
        /// <summary>
        /// Set the value of a vault variable.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <param name="variable">Variable name.</param>
        /// <param name="Value">Value of the variable.</param>
        /// <param name="comment">Comment.</param>
        /// <param name="configurationName">Configuration name.</param>
        /// <returns>MethodReturn</returns>
        public static MethodReturn SetVariableToDb(this IEdmFile5 file, string variable, object Value, string comment, string configurationName = "@")
        {
            if (file == null)
                throw new ArgumentNullException("File parameter is null");
            if (file.IsLocked == false)
                file.CheckOut();

            // get the variable object
            IEdmEnumeratorVariable7 enumVariable = default(IEdmEnumeratorVariable7);
            enumVariable = (file as IEdmFile7).GetEnumeratorVariable() as IEdmEnumeratorVariable7;
            // Value must be an object 
            try
            {
                if ((file.IsSldAssembly() || file.IsSldPart()) == false)
                    configurationName = null;
                enumVariable.SetVar(variable, configurationName, ref Value, false);
                var enumVariable8 = enumVariable as IEdmEnumeratorVariable8;
                enumVariable8.CloseFile(true);
                file.CheckIn(0,EdmUnlockFlag.EdmUnlock_Simple,comment);
            }
            catch (COMException e)
            {
                return new MethodReturn(e, true);
            }

            // return variable's variable 
            return new MethodReturn();
        }

        /// <summary>
        /// Gets the values of the specified file in bulk.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="variableNames"></param>
        /// <param name="configurationNames"></param>
        /// <returns></returns>
        /// <remarks>This method is optimized for batch processing multiple files.</remarks>
        public static MethodReturn<KeyValuePair<string, object>[]> GetVariablesFromDB(this IEdmFile5 file, string[] variableNames, string[] configurationNames)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Sets the value for the specified variable and refreshes the data card preview.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <param name="variableName">variable name.</param>
        /// <param name="configurationName">Configuration name.</param>
        /// <param name="value">Value of the variable.</param>
        /// <param name="allConfigurations">Apply the value to all configurations.</param>
        /// <returns>MethodReturn</returns>
        /// <remarks>Specified file must be checked out.</remarks>
        public static MethodReturn SetDataCardVariable2(this IEdmFile5 file, string variableName, object value, string configurationName = "@", bool allConfigurations = false)
        {
            try
            {


                if (file.IsLocked == false)
                    throw CADSharpToolsPDMException.CreateInstance(CADSharpToolsPDMExceptionMessages_e.FileMustBeCheckedOut);

                if ((file.IsSldAssembly() || file.IsSldPart()) == false)
                    configurationName = null;


                var vault = file.Vault as IEdmVault7;
                IEdmBatchUpdate2 Update = default(IEdmBatchUpdate2);
                Update = (IEdmBatchUpdate2)vault.CreateUtility(EdmUtility.EdmUtil_BatchUpdate);

                //Get the IDs of the file and folder card variables to update
                IEdmVariableMgr5 VariableMgr = default(IEdmVariableMgr5);
                VariableMgr = file.Vault as IEdmVariableMgr5;
                int variableId = VariableMgr.GetVariable(variableName).ID;
                int folderId = file.GetFolder().ID;
                //Search for all text files in the vault
                IEdmSearch5 Search = default(IEdmSearch5);
                Search = (IEdmSearch5)vault.CreateUtility(EdmUtility.EdmUtil_Search);
                Search.StartFolderID = folderId;
                Search.FileName = file.Name;
                IEdmSearchResult5 Result = default(IEdmSearchResult5);
                Result = Search.GetFirstResult();
                while ((Result != null))
                {
                    var flags = default(EdmBatchFlags);
                    if (allConfigurations)
                        flags = EdmBatchFlags.EdmBatch_RefreshPreview | EdmBatchFlags.EdmBatch_AllConfigs;
                    else
                        flags = EdmBatchFlags.EdmBatch_RefreshPreview;
                    Update.SetVar(Result.ID, variableId, ref value, configurationName, (int)flags);
                    Result = Search.GetNextResult();
                }
                EdmBatchError2[] errors = new EdmBatchError2[] { };
                var ret = Update.CommitUpdate(out errors, null);
                if (ret == 0)
                    return new MethodReturn();
                else
                {
                    string errorName = string.Empty;
                    string errorDescription = string.Empty;
                    vault.GetErrorString(errors[0].mlErrorCode, out errorName, out errorDescription);
                    return new MethodReturn(new Exception($"{errorName} - {errorDescription}"));
                }

            }
            catch (COMException e)
            {
                return new MethodReturn(e,true);
            }
           
        }


        /// <summary>
        /// Returns whether the specified file has a local copy.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <param name="folderID">Folder ID</param>
        /// <returns>True if local copy exists, false if not.</returns>
        public static bool HasLocalCopy(this IEdmFile5 file, int folderID)
        {
         
            return file.GetLocalVersionNo(folderID) == -1 ? false : true;
        }

        /// <summary>
        /// Gets the value of the vault data card variable for the specified checked out file.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <param name="variable">Vault variable name.</param>
        /// <param name="configurationName">Configuration name. Default is "@".</param>
        /// <returns>MethodReturn of the data card variable's value and the GetDataCardVariableReturn_e operation return.</returns>
        ///<remarks><ul> 
        ///<li>
        ///You have to call IEdmFile5::GetFileCopy get a local copy of a file before using this method to read variables. This is not required if there is a local copy. Use <see cref="HasLocalCopy(IEdmFile5,int)"/> to check whether the specified file object has a local copy or not.
        /// </li>
        ///<li>
        /// Use <see cref="GetVariableFromDb(IEdmFile5, string, string)"/> to get data from files that are not cached (have no local copies).
        /// </li>
        /// </ul></remarks>
        /// <example> 
        ///  This example shows how to login to the vault and get the value of a vault variable called "Part Number" for the first file.
        /// <code>        
        ///public enum EdmLoginFlags
        ///{
        ///    Nothing,
        ///    WebClient
        ///}
        ///
        ///public static void Example()
        ///{
        ///    //create vault object 
        ///    var swPDMVault = new EdmVault5();
        ///    var swPDMVaultEx = swPDMVault as IEdmVault13;
        ///
        ///    //required per PDM EULA in out-of-process applications              
        ///    swPDMVaultEx.LoginEx("Admin", "****", "CADSharp LLC", (int)EdmLoginFlags.Nothing);
        ///
        ///    // get root folder 
        ///    var swPDMRootFolder = swPDMVaultEx.RootFolder;
        ///
        ///    // get first file position
        ///    var swPDMFirstFilePos = swPDMRootFolder.GetFirstFilePosition();
        ///    if (swPDMFirstFilePos.IsNull == false)
        ///    {
        ///        var swPDMFirstFile = swPDMRootFolder.GetNextFile(swPDMFirstFilePos);
        ///
        ///        // get the part number of the first file 
        ///        var variableReturn = swPDMFirstFile.GetDataCardVariable("Part Number");
        ///        if (variableReturn.IsError)
        ///        {
        ///            Debug.Print(variableReturn.Error);
        ///            return;
        ///        }
        ///
        ///        var partNumberValue = variableReturn.Value.ToString();
        ///        Debug.Print($"Part Number variable value: {partNumberValue}");
        ///    }
        ///    else
        ///    {
        ///        Debug.Print($"Vault has no files at the root folder.");
        ///    }
        ///}
        ///</code>
        ///</example>
        /// <exception cref="CADSharpToolsPDMException" >Thrown when: <ul>
        /// <li>unable to get the parent folder of the specified file object.</li>
        /// <li>the specified file has no local copy.</li>
        /// </ul></exception>
        public static MethodReturn<object, CADSharpTools_e.GetDataCardVariableReturn_e> GetDataCardVariable(this IEdmFile5 file, string variable, string configurationName = "@")
        {
            if (file == null)
            throw new ArgumentNullException("File parameter is null");
           
            var folder = default(IEdmFolder5);
            var Pos = file.GetFirstFolderPosition();
            while (!Pos.IsNull)
            {
                folder = file.GetNextFolder(Pos);
                if (folder != null)
                    break;
            }

          
            if (folder == null)
            {
                throw CADSharpToolsPDMException.CreateInstance(CADSharpToolsPDMExceptionMessages_e.UnableToGetFolderOfFile);
            }
            // get the variable object
            IEdmEnumeratorVariable5 enumVariable = default(IEdmEnumeratorVariable5);
            enumVariable = file.GetEnumeratorVariable();
            // Value must be an object 
            var Value = new Object();
            // Set Variable 
            try
            {
                if ((file.IsSldAssembly() || file.IsSldPart()) == false)
                    configurationName = null;

                enumVariable.GetVar(variable, configurationName, out Value);
                var enumVariable8 = enumVariable as IEdmEnumeratorVariable8;
                enumVariable8.CloseFile(true); 
            }
            catch (COMException e)
            {
                return new MethodReturn<object, CADSharpTools_e.GetDataCardVariableReturn_e>(null, e, true, CADSharpTools_e.GetDataCardVariableReturn_e.VariableNotFound);
            }

            // return variable's variable 
            return new MethodReturn<object, CADSharpTools_e.GetDataCardVariableReturn_e>(Value);
        }

        /// <summary>
        /// Returns if the file is a SOLIWORKS part document.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <returns>True if file is a part, false if not.</returns>
        public static bool IsSldPart(this IEdmFile5 file)
        {
            string localPath = file.GetLocalPath().Value;
            string extension = System.IO.Path.GetExtension(localPath).ToLower();
            if (extension.Contains("sldprt"))
                return true;
            else
                return false;
        }
        /// <summary>
        /// Returns if the file is a SOLIWORKS assembly document.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <returns>True if file is an assembly, false if not.</returns>
        public static bool IsSldAssembly(this IEdmFile5 file)
        {
            string localPath = file.GetLocalPath().Value;
            string extension = System.IO.Path.GetExtension(localPath).ToLower();
            if (extension.Contains("sldasm"))
                return true;
            else
                return false;
        }
        /// <summary>
        /// Returns if the file is a SOLIWORKS drawing document.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <returns>True if file is a drawing, false if not.</returns>
        public static bool IsSldDrawing(this IEdmFile5 file)
        {
            string localPath = file.GetLocalPath().Value;
            string extension = System.IO.Path.GetExtension(localPath).ToLower();
            if (extension.Contains("slddrw"))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Sets the value of the vault data card variable for the specified file.
        /// </summary>
        /// <param name="file">File object.</param>
        /// <param name="variable">Vault variable name.</param> 
        /// <param name="configurationName">Configuration name. Default is "@".</param>
        /// <param name="value">New value of the data card variable.</param>
        /// <returns>MethodReturn of boolean: True if successful, false if not.</returns>     
        /// <exception cref="CADSharpToolsPDMException" >Thrown when:
        /// <ul>
        /// <li>
        /// unable to get the parent folder of the specified file object.
        /// </li>
        /// <li>
        /// the file is checked in.
        /// </li>
        /// </ul>
        /// </exception>
        /// <exception cref="NullReferenceException" >Thrown when file parameter is null.</exception>
        public static MethodReturn<bool> SetDataCardVariable(this IEdmFile5 file, string variable, string configurationName = "@", object value = null)
        {
            if (file == null)
                throw new NullReferenceException("file parameter cannot be null.");

            if (!file.IsLocked)
                throw CADSharpToolsPDMException.CreateInstance(CADSharpToolsPDMExceptionMessages_e.FileMustBeCheckedOut);

            var folder = default(IEdmFolder5);
            var Pos = file.GetFirstFolderPosition();
            while (!Pos.IsNull)
            {
                folder = file.GetNextFolder(Pos);
                if (folder != null)
                    break;
            }

            if (folder == null)
            {
                throw CADSharpToolsPDMException.CreateInstance(CADSharpToolsPDMExceptionMessages_e.UnableToGetFolderOfFile);
            }
            // get the variable object
            IEdmEnumeratorVariable5 enumVariable = default(IEdmEnumeratorVariable5);
            enumVariable = file.GetEnumeratorVariable(file.GetLocalPath(folder.ID));


            // Set Variable 
            enumVariable.SetVar(variable, configurationName, value);
            // return variable's variable 
            // save changes 
            IEdmEnumeratorVariable8 enumVariable8 = (IEdmEnumeratorVariable8)enumVariable;
            enumVariable8.CloseFile(true);
            return new MethodReturn<bool>(true);
        }


      

        /// <summary>
        /// Attempts to get a PDM object. 
        /// </summary>
        /// <typeparam name="T">Specified type.</typeparam>
        /// <param name="Vault">Vault object.</param>
        /// <param name="swPDMType">PDM type of the object.</param>
        /// <param name="ID">ID of the object.</param>
        /// <returns>MethodReturn of the specified PDM object type.</returns>
        /// <remarks>The TryGetObject method is like the GetObject method, except the TryGetObject method does not throw an exception if the object does not exist. </remarks>
        public static MethodReturn<T> TryGetObject<T>(this IEdmVault5 Vault, EdmObjectType swPDMType, int ID = 0) where T : class
        {
            try
            {
                T returnObject = Vault.GetObject(swPDMType, ID) as T;
                return new MethodReturn<T>(returnObject);
            }
            catch (COMException e)
            {
                return new MethodReturn<T>(null, e, true);
            }
        }
        
        /// <summary>
        /// Batch check-in an array of PDM files.
        /// </summary>
        /// <param name="Vault">Vault object.</param>
        /// <param name="files">array of IEdmFile5.</param>
        /// <param name="Handle">Handle of the parent window.</param>
        /// <returns>MethodReturn of boolean: True if successful, false if not.</returns>
        /// <remarks>Method is not thread-safe. Use this method only in the main thread of your application.</remarks>
        public static MethodReturn<bool> BatchCheckIn(this IEdmVault5 Vault, IEdmFile5[] files, EdmUnlockBuildTreeFlags locks,  int Handle)
        {           
            // create batch unlocker object 
            var batchUnlocker = (IEdmBatchUnlock2)(Vault as IEdmVault8).CreateUtility(EdmUtility.EdmUtil_BatchUnlock) as IEdmBatchUnlock2;
            // create the selection list 
            var list = new List<EdmSelItem>();
            var selectedFile = default(EdmSelItem);
            foreach (var file in files)
            {
                var ppoRetParentFolder = default(IEdmFolder5);
                string localPath = file.GetLocalPath().Value;
                var aFile = Vault.GetFileFromPath(localPath, out ppoRetParentFolder);
                selectedFile = new EdmSelItem();
                IEdmPos5 aPos = aFile.GetFirstFolderPosition();
                IEdmFolder5 aFolder = aFile.GetNextFolder(aPos);
                selectedFile.mlDocID = aFile.ID;
                selectedFile.mlProjID = aFolder.ID;
                list.Add(selectedFile);
            }

            var vault1 = new EdmVault5();
            vault1 = (EdmVault5)Vault;
            // do not change this - the array must init as null
            EdmSelItem[] ppoSelection = null;

            ppoSelection = list.ToArray();
            batchUnlocker.AddSelection(vault1, ref ppoSelection);

            //EdmUnlockBuildTreeFlags.Eubtf_ShowCloseAfterCheckinOption + (int)EdmUnlockBuildTreeFlags.Eubtf_MayUnlock

            //create tree 
            batchUnlocker.CreateTree(Handle, (int)locks);
            // unlock file 


            batchUnlocker.UnlockFiles(Handle,null);
            return new MethodReturn<bool>(true);
         
    }
        /// <summary>
        /// Batch check-out an array of PDM files.
        /// </summary>
        /// <param name="Vault">Vault object.</param>
        /// <param name="files">array of filenames.</param>
        /// <param name="Handle">Handle of the parent window.</param>
        /// <returns>MethodReturn of boolean: True if successful, false if not.</returns>
        /// <remarks>Method is not thread-safe. Use this method only in the main thread of your application.</remarks>
        public static MethodReturn<bool> BatchCheckIn(this IEdmVault5 Vault, string[] files, int Handle)
        {
            // create batch unlocker object 
            var batchUnlocker = (IEdmBatchUnlock2)(Vault as IEdmVault8).CreateUtility(EdmUtility.EdmUtil_BatchUnlock) as IEdmBatchUnlock2;
            // create the selection list 
            var list = new List<EdmSelItem>();
            var selectedFile = default(EdmSelItem);
            foreach (var file in files)
            {
                var ppoRetParentFolder = default(IEdmFolder5);
                var aFile = Vault.GetFileFromPath(file, out ppoRetParentFolder);
                selectedFile = new EdmSelItem();
                IEdmPos5 aPos = aFile.GetFirstFolderPosition();
                IEdmFolder5 aFolder = aFile.GetNextFolder(aPos);
                selectedFile.mlDocID = aFile.ID;
                selectedFile.mlProjID = aFolder.ID;
                list.Add(selectedFile);
            }

            var vault1 = new EdmVault5();
            vault1 = (EdmVault5)Vault;
            // do not change this - the array must init as null
            EdmSelItem[] ppoSelection = null;

            ppoSelection = list.ToArray();
            batchUnlocker.AddSelection(vault1, ref ppoSelection);

            //create tree 
            batchUnlocker.CreateTree(Handle, (int)EdmUnlockBuildTreeFlags.Eubtf_ShowCloseAfterCheckinOption + (int)EdmUnlockBuildTreeFlags.Eubtf_MayUnlock);
            // unlock file 
            batchUnlocker.UnlockFiles(Handle, null);
            return new MethodReturn<bool>(true);

        }
        
        /// <summary>
        /// Batch check out an array of PDM files.
        /// </summary>
        /// <param name="Vault">Vault object.</param>
        /// <param name="files">array of filenames.</param>
        /// <param name="Handle">Handle of the parent window.</param>
        /// <returns>MethodReturn of boolean: True if successful, false if not.</returns>
        /// <remarks>Method is not thread-safe. Use this method only in the main thread of your application.</remarks>
        public static bool BatchCheckOutFiles(this IEdmVault5 Vault, string[] files, int Handle)
        {
            // create batch locker object 
            var batchLocker = (IEdmBatchGet)(Vault as IEdmVault8).CreateUtility(EdmUtility.EdmUtil_BatchGet) as IEdmBatchGet;
            // create the selection list 
            var list = new List<EdmSelItem>();
            var selectedFile = default(EdmSelItem);
            foreach (var file in files)
            {
                var ppoRetParentFolder = default(IEdmFolder5);
                var aFile = Vault.GetFileFromPath(file, out ppoRetParentFolder);
                selectedFile = new EdmSelItem();
                IEdmPos5 aPos = aFile.GetFirstFolderPosition();
                IEdmFolder5 aFolder = aFile.GetNextFolder(aPos);
                selectedFile.mlDocID = aFile.ID;
                selectedFile.mlProjID = aFolder.ID;
                list.Add(selectedFile);
            }

            var vault1 = new EdmVault5();
            vault1 = (EdmVault5)Vault;
            // do not change this - the array must init as null
            EdmSelItem[] ppoSelection = null;

            ppoSelection = list.ToArray();
            batchLocker.AddSelection(vault1, ref ppoSelection);

            //create tree 
            batchLocker.CreateTree(Handle, (int)EdmGetCmdFlags.Egcf_Lock);
            // lock file 
            batchLocker.GetFiles(Handle, null);
            return true;
        }
        
        
        /// <summary>
        /// Batch check out an array of PDM files.
        /// </summary>
        /// <param name="Vault">Vault object.</param>
        /// <param name="files">array of IEdmFile5.</param>
        /// <param name="getCmds">Get commands.</param>
        /// <param name="Handle">Handle of the parent window.</param>
        /// <returns>MethodReturn of boolean: True if successful, false if not.</returns>
        /// <remarks>Method is not thread-safe. Use this method only in the main thread of your application.</remarks>
        public static bool BatchCheckOutFiles(this IEdmVault5 Vault, IEdmFile5[] files, EdmGetCmdFlags getCmds, int Handle)
        {
            // create batch locker object 
            var batchLocker = (IEdmBatchGet)(Vault as IEdmVault8).CreateUtility(EdmUtility.EdmUtil_BatchGet) as IEdmBatchGet;
            // create the selection list 
            var list = new List<EdmSelItem>();
            var selectedFile = default(EdmSelItem);
            foreach (var file in files)
            {
                var ppoRetParentFolder = default(IEdmFolder5);
                string localPath = file.GetLocalPath().Value;
                var aFile = Vault.GetFileFromPath(localPath, out ppoRetParentFolder);
                selectedFile = new EdmSelItem();
                IEdmPos5 aPos = aFile.GetFirstFolderPosition();
                IEdmFolder5 aFolder = aFile.GetNextFolder(aPos);
                selectedFile.mlDocID = aFile.ID;
                selectedFile.mlProjID = aFolder.ID;
                list.Add(selectedFile);
            }

            var vault1 = new EdmVault5();
            vault1 = (EdmVault5)Vault;
            // do not change this - the array must init as null
            EdmSelItem[] ppoSelection = null;

            ppoSelection = list.ToArray();
            batchLocker.AddSelection(vault1, ref ppoSelection);

            //create tree 
            batchLocker.CreateTree(Handle, (int)getCmds);
            // lock file 
            batchLocker.GetFiles(Handle, null);
            return true;
        }
        /// <summary>
        /// Send the logged-in user a message.
        /// </summary>
        /// <param name="Vault">Vault object.</param>
        /// <param name="Handle">Handle of the parent window.</param>
        /// <param name="programName">Title of the Message dialog box.</param>
        /// <param name="message">Message</param>
        public static void MsgBox(this IEdmVault5 Vault, int Handle, string programName, string message)
        {
            Vault.MsgBox(Handle, message, EdmMBoxType.EdmMbt_OKOnly, programName);
        }
    }
}

 
///// <summary>
///// Vault variable class.
///// </summary>
//public class VaultVariable
//{
//    /// <summary>
//    /// Gets the variable's name.
//    /// </summary>
//    public string Name { get; private set; }

//    /// <summary>
//    /// Gets the variable's configuration-value pairs.
//    /// </summary>
//    public Dictionary<string, object> ConfigurationValueDictionary { get; private set; } = new Dictionary<string, object>();
//    /// <summary>
//    /// Gets associated file id.
//    /// </summary>
//    public int FileId { get; private set; } = int.MinValue;

//    /// <summary>
//    /// Creates a new instance of the vault variable class.
//    /// </summary>
//    public VaultVariable(Dictionary<string, object> configurationValueDictionary, int fileId, string variableName)
//    {
//        this.Name = Name;
//        this.ConfigurationValueDictionary = configurationValueDictionary;
//        this.FileId = fileId;

//    }

//}


