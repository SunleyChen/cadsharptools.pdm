﻿using System;

namespace CADSharpTools.Attributes
{
    /// <summary>
    /// Holds value of HResult for COM exceptions.
    /// </summary>
    public class HResultAttribute : Attribute 
    {
        /// <summary>
        /// HResult.
        /// </summary>
        public int HResult { get; set; }

        /// <summary>
        /// Creates a new instance of the HResult class.
        /// </summary>
        /// <param name="hResult">HResult.</param>
        public HResultAttribute(int hResult)
        {
            this.HResult = hResult;
        }
    }
}
