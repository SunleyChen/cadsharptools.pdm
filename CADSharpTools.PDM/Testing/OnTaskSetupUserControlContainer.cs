﻿using EPDM.Interop.epdm;
using System;
using System.Windows.Forms;


namespace CADSharpTools.Testing
{
    /// <summary>
    /// Shell container used for testing UserControls that created in the task window.
    /// </summary>
    /// <typeparam name="T">A UserControl that implements <see cref="ICmdLoadableUserControl"/>.</typeparam>
    public partial class OnTaskSetupUserControlContainer<T> : Form where T : UserControl, ICmdLoadableUserControl
     
    {
        /// <summary>
        /// UserControl.
        /// </summary>
        public T UserControl { get; private set; }
        /// <summary>
        /// EmdCmd.
        /// </summary>
        public EdmCmd Cmd { get; private set; }
         

        private OnTaskSetupUserControlContainer()
        {
            
        }

        /// <summary>
        /// Creates the shell for the UserControl
        /// </summary>
        /// <param name="userControl">UserControl.</param>
        /// <returns></returns>
        public static OnTaskSetupUserControlContainer<T> CreateShell(T userControl, ref EdmCmd Cmd)
        {
            var OnTaskSetupUserControlContainer = new OnTaskSetupUserControlContainer<T>();
            OnTaskSetupUserControlContainer.InitializeComponent();
            OnTaskSetupUserControlContainer.UserControl = userControl;
            OnTaskSetupUserControlContainer.Cmd = Cmd;


            return OnTaskSetupUserControlContainer;
        }

        private void OnTaskSetupUserControlContainer_Load(object sender, EventArgs e)
        {

        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            
                var panelControl = this.Controls.Find("Panel", false)[0] as System.Windows.Forms.TableLayoutPanel;
            if (UserControl != null)
            {
                panelControl.Controls.Remove(UserControl);
                var Cmd = this.Cmd;
                UserControl.StoreData(ref Cmd);

            }
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            if (UserControl != null)
            {
                var panelControl = this.Controls.Find("Panel", false)[0] as System.Windows.Forms.TableLayoutPanel;
                panelControl.Controls.Add(UserControl);
                var Cmd = this.Cmd;
                UserControl.LoadData(ref Cmd);

            }
        }
    }
}
