﻿using EPDM.Interop.epdm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CADSharpTools.Testing
{
    /// <summary>
    /// Implements the LoadData and StoreData methods for controls that are created in the implementation of the method that is hooked into <see cref="EdmCmdType.EdmCmd_TaskSetup"/>.
    /// </summary>
    public interface ICmdLoadableUserControl
    {
        /// <summary>
        /// Loads data from the task.
        /// </summary>
        /// <param name="cmd">EdmCmd specific to the <see cref="EdmCmdType.EdmCmd_TaskSetup"/> hook.</param>
        /// <remarks>Use <see cref="Testing.EdmObjectFactory.MockEdmCmd(EdmCmdType, IEdmVault5, string, int, int, int, EdmRefreshFlag, bool, bool, object)"/> to mock the <see cref="EdmCmd"/> interface.</remarks>
        void LoadData(ref EdmCmd cmd);
        /// <summary>
        /// Stores data back into the task.
        /// <param name="cmd">EdmCmd specific to the <see cref="EdmCmdType.EdmCmd_TaskSetup"/> hook.</param>
        /// <remarks>Use <see cref="Testing.EdmObjectFactory.MockEdmCmd(EdmCmdType, IEdmVault5, string, int, int, int, EdmRefreshFlag, bool, bool, object)"/> to mock the <see cref="EdmCmd"/> interface.</remarks>
        void StoreData(ref EdmCmd cmd);
    }
}
