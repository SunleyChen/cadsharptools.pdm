﻿using EPDM.Interop.epdm;
using System;
using System.Collections.Generic;
using CADSharpTools.Core;
using CADSharpTools.PDM;

namespace CADSharpTools.Testing
{
    /// <summary>PDM objects Factory class for testing.</summary>
    public static class EdmObjectFactory
    {
        /// <summary>
        /// Returns the vault object after attempting to auto-login to the vault.
        /// </summary>
        /// <param name="parentHandle">Handle of the window above which to show PDM vault login window.</param>
        /// <param name="vaultName">Name of the vault to login to.</param>
        /// <returns>Vault object.</returns>
        public static IEdmVault5 LoginToVault(string vaultName, int parentHandle = 0)
        {
            EdmVault5 edmVault5 = (EdmVault5)new EdmVault5Class();
            ((IEdmVault7)edmVault5).LoginAuto(vaultName, parentHandle);
            return (IEdmVault5)edmVault5;
        }

        /// <summary>Creates the file object using relative path.</summary>
        /// <param name="vault">Vault object.</param>
        /// <param name="relativePath">Relative path to the vault root folder.</param>
        /// <returns>IEdmFile5 object</returns>
        /// <remarks><ul>
        /// <li>
        /// Relative path is with regards to the vault's root folder.
        /// </li>
        /// <li>
        /// Only use this method to get the <see cref="IEdmFile5"/> of files that have local copies.
        /// </li>
        /// </ul></remarks>
        public static IEdmFile5 GetFileObjectFromRelativePath(
          IEdmVault5 vault,
          string relativePath)
        {
            IEdmFolder5 iedmFolder5 = (IEdmFolder5)null;
            return vault.GetFileFromPath(string.Format("{0}\\{1}", vault.RootFolder.LocalPath, (object)relativePath), out iedmFolder5);
        }
        /// <summary>Mocks the EdmCmd struct.</summary>
        /// <param name="cmdType">Command type.</param>
        /// <param name="extraObject">Pointer to an object containing command-specific information.</param>
        /// <param name="vault">Vault object.</param>
        /// <param name="parentHandle">parent handle.</param>
        /// <param name="cancel">True to prevent a command from running using EdmCmd_PreXxxx hooks, false to disallow canceling the command.</param>
        /// <param name="comment">Information specific to the type of command or hook.</param>
        /// <param name="refreshFlags">Flags that cause SolidWorks Enterprise PDM to refresh elements of the user interface. Bitmask.</param>
        ///<param name="CommandID">ID of the invoked command.</param>
        /// <param name="currentFolderID">ID of the active folder. Used for menu commands (meCmdType = EdmCmdType.EdmCmd_Menu).</param>
        /// <param name="silentMode">True to execute asynchronously in the background, false to run interactively from the user interface</param>
        /// <returns>Command struct.</returns>
        public static EdmCmd MockEdmCmd(
          EdmCmdType cmdType,
          IEdmVault5 vault,
          string comment = "",
          int parentHandle = 0,
          int currentFolderID = 0,
          int CommandID = 0,
          EdmRefreshFlag refreshFlags = EdmRefreshFlag.EdmRefresh_Nothing,
          bool cancel = false,
          bool silentMode = false,
          object extraObject = null
          )
        {
            EdmCmd edmCmd = default(EdmCmd);
            edmCmd.meCmdType = cmdType;
            edmCmd.mlCurrentFolderID = currentFolderID;
            edmCmd.mlCmdID = CommandID;
            edmCmd.mlEdmRefreshFlags = (int)refreshFlags;
            edmCmd.mbsComment = comment;
            edmCmd.mpoExtra = extraObject;
            edmCmd.mpoVault = vault;
            edmCmd.mbCancel = (short)(cancel == true ? 1 : 0);
            edmCmd.mbSilentMode = (short)(silentMode == true ? 1 : 0);
            edmCmd.mlParentWnd = parentHandle;
            return edmCmd;
        }

        /// <summary>
        /// Creates the EdmCmdData struct from the specified file.
        /// </summary>
        /// <param name="file">Must provided for all commands.</param>
        /// <param name="cmdType">Command type.</param>
        /// <param name="affectedConfigurationName">affected configuration. Must be provided for EdmCmd_PreState, EdmCmd_PostState</param>
        /// <param name="configurationNames">affected configuration names. Must be provided for EdmCmd_CardButton</param>
        /// <param name="sourceState">Must be provided for EdmCmd_PreState, EdmCmd_PostState</param>
        /// <param name="targetState">Must be provided for EdmCmd_PreState, EdmCmd_PostState</param>
        /// <param name="transition">Must be provided for EdmCmd_PreState, EdmCmd_PostState</param>
        /// <param name="fileCardId">Id of the affected file card.</param>
        /// <param name="extraObject">Extra object for the mpoExtra field in the Command struct.</param>
        /// <param name="mEdmCmdNode">Access a file upon changing state.</param>
        /// <param name="user">User performing the command. Must be provided for EdmCmd_PreState, EdmCmd_PostState</param>
        /// <returns></returns>
        public static EdmCmdData MockEdmCmdDataFromFile(
          IEdmFile5 file,
          EdmCmdType cmdType,
          object extraObject = null,
          string affectedConfigurationName = "Default",
          string[] configurationNames = null,
          IEdmState5 sourceState = null,
          IEdmState5 targetState = null,
          IEdmTransition5 transition = null,
          mEdmCmdNode mEdmCmdNode = null,
          int fileCardId = 0,
          IEdmUser5 user = null)
        {

            string localPath = string.Empty;
            int folderID = int.MinValue;

            EdmCmdData edmCmdData = default(EdmCmdData);
            edmCmdData = new EdmCmdData();
            switch (cmdType)
            {
                case EdmCmdType.EdmCmd_PostLock:
                    edmCmdData.mlObjectID1 = file.ID;
                    int id = file.GetFolder().ID;
                    edmCmdData.mlObjectID2 = id;
                    localPath = file.GetLocalPath().Value;
                    edmCmdData.mlObjectID3 = transition.ID;
                    edmCmdData.mbsStrData1 = localPath;
                    edmCmdData.mpoExtra = null;
                    break;
                case EdmCmdType.EdmCmd_PreLock:
                    edmCmdData.mlObjectID1 = file.ID;
                    folderID = file.GetFolder().ID;
                    edmCmdData.mlObjectID2 = folderID;
                    localPath = file.GetLocalPath().Value;
                    edmCmdData.mlObjectID3 = transition.ID;
                    edmCmdData.mbsStrData1 = localPath;
                    edmCmdData.mpoExtra = null;
                    break;

                case EdmCmdType.EdmCmd_PreState:
                    edmCmdData.mlObjectID1 = file.ID;
                    folderID = file.GetFolder().ID;
                    edmCmdData.mlObjectID2 = folderID;
                    string str1 = file.GetLocalPath().Value;
                    edmCmdData.mlObjectID3 = transition.ID;
                    edmCmdData.mlObjectID4 = user.ID;
                    edmCmdData.mbsStrData1 = str1;
                    edmCmdData.mbsStrData2 = targetState.Name;
                    edmCmdData.mlLongData1 = sourceState.ID;
                    edmCmdData.mlLongData2 = targetState.ID;
                    edmCmdData.mpoExtra = mEdmCmdNode;
                    break;
                case EdmCmdType.EdmCmd_PostState:
                    edmCmdData.mlObjectID1 = file.ID;
                    int id2 = file.GetFolder().ID;
                    edmCmdData.mlObjectID2 = id2;
                    string str2 = file.GetLocalPath().Value;
                    edmCmdData.mlObjectID3 = transition.ID;
                    edmCmdData.mlObjectID4 = user.ID;
                    edmCmdData.mbsStrData1 = str2;
                    edmCmdData.mbsStrData2 = targetState.Name;
                    edmCmdData.mlLongData1 = sourceState.ID;
                    edmCmdData.mlLongData2 = targetState.ID;
                    edmCmdData.mpoExtra = mEdmCmdNode;
                    break;
                case EdmCmdType.EdmCmd_TaskRun:
                    edmCmdData.mlObjectID1 = file.ID;
                    int id3 = file.GetFolder().ID;
                    edmCmdData.mlObjectID2 = id3;
                    string str3 = file.GetLocalPath().Value;
                    edmCmdData.mbsStrData1 = str3;
                    edmCmdData.mbsStrData2 = affectedConfigurationName;
                    edmCmdData.mpoExtra = extraObject;
                    edmCmdData.mlLongData1 = (int)file.ObjectType;
                    break;
                case EdmCmdType.EdmCmd_TaskLaunch:
                    edmCmdData.mlObjectID1 = file.ID;
                    int taskLaunchFolderID = file.GetFolder().ID;
                    edmCmdData.mlObjectID2 = taskLaunchFolderID;
                    string taskLaunchLocalPath = file.GetLocalPath().Value;
                    edmCmdData.mbsStrData1 = taskLaunchLocalPath;
                    edmCmdData.mbsStrData2 = affectedConfigurationName;
                    edmCmdData.mpoExtra = extraObject;
                    edmCmdData.mlLongData1 = (int)file.ObjectType;
                    break;
                case EdmCmdType.EdmCmd_CardButton:
                    if (file.ObjectType == EdmObjectType.EdmObject_File)
                    {
                        edmCmdData.mlObjectID1 = file.ID;
                        int folderId = file.GetFolder().ID;
                        edmCmdData.mlObjectID2 = folderId;
                        edmCmdData.mlObjectID3 = fileCardId;
                        string pathName = file.GetLocalPath().Value;
                        edmCmdData.mbsStrData1 = pathName;
                        edmCmdData.mbsStrData2 = affectedConfigurationName;
                        edmCmdData.mpoExtra = extraObject;
                        edmCmdData.mlLongData1 = (int)file.ObjectType;
                        
                    }
                    break;
            }
            return edmCmdData;
        }
        /// <summary>
        /// Returns the user object from the specified user name (login name) or id.
        /// </summary>
        /// <param name="vault">Vault object.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="userID">ID of the user.</param>
        /// <returns>IEdmUser5 object</returns>
        /// <remarks>
        /// <ul>
        /// <li>Use the user id instead of the user name if there are multiple users with the same name.</li>
        /// <li>The parameter userId takes precedence over the userName.</li>
        /// <li>When userName is specified, the method will return the first user with matching name.</li>
        /// </ul>
        /// </remarks>
        public static IEdmUser5 GetUserObject(IEdmVault5 vault, string userName, int userID = -2147483648)
        {
            if (userID != int.MinValue)
                return vault.GetObject((EdmObjectType)7, userID) as IEdmUser5;
            MethodReturn<IEdmUser5[]> allUsers = vault.GetAllUsers();
            if (allUsers.IsError)
                throw allUsers.Exception;
            foreach (IEdmUser5 iedmUser5 in allUsers.Value)
            {
                if (iedmUser5.Name == userName)
                    return iedmUser5;
            }
            return (IEdmUser5)null;
        }
        /// <summary>
        /// Returns the user group object from the specified user group name (login name) or id.
        /// </summary>
        /// <param name="vault">Vault object.</param>
        /// <param name="userGroupName">Name of the user group.</param>
        /// <param name="userGroupID">ID of the user group.</param>
        /// <returns>IEdmUserGroup5 object</returns>
        /// <remarks>
        /// <ul>
        /// <li>The parameter userGroupId takes precedence over the useGroupName.</li>
        /// <li>When userGroupName is specified, the method will return the first userGroup with matching name.</li>
        /// </ul>
        /// </remarks>
        public static IEdmUserGroup5 GetuserGroupObject(IEdmVault5 vault, string userGroupName, int userGroupID = -2147483648)
        {
            if (userGroupID != int.MinValue)
                return vault.GetObject(EdmObjectType.EdmObject_UserGroup, userGroupID) as IEdmUserGroup5;
            MethodReturn<IEdmUserGroup5[]> alluserGroups = vault.GetAllUserGroups();
            if (alluserGroups.IsError)
                throw alluserGroups.Exception;
            foreach (IEdmUserGroup5 iedmuserGroup5 in alluserGroups.Value)
            {
                if (iedmuserGroup5.Name == userGroupName)
                    return iedmuserGroup5;
            }
            return (IEdmUserGroup5)null;
        }
        /// <summary>
        /// Returns the workflow transition object from the specified workflow transition name or id.
        /// </summary>
        /// <param name="workflow">workflow object.</param>
        /// <param name="workflowTransitionName">Name of the workflow transition.</param>
        /// <param name="workflowTransitionID">ID of the workflow transition.</param>
        /// <returns>IEdmTransition5 object</returns>
        /// <remarks>
        /// <ul>
        /// <li>Use the workflow transition id instead of the workflow transition name if there are multiple workflow transitions with the same name.</li>
        /// <li>The parameter workflowTransitionID takes precedence over the workflowTransitionName.</li>
        /// <li>When workflowTransitionName is specified, the method will return the first workflow transition with matching name.</li>
        /// </ul>
        /// </remarks>
        public static IEdmTransition5 GetWorkflowTransition(
          IEdmWorkflow6 workflow,
          string workflowTransitionName,
          int workflowTransitionID = -2147483648)
        {
            MethodReturn<IEdmTransition5[]> workflowTransitions = workflow.GetAllWorkflowTransitions();
            if (workflowTransitions.IsError)
                throw workflowTransitions.Exception;
            foreach (IEdmTransition5 iedmTransition5 in workflowTransitions.Value)
            {
                if (workflowTransitionID != int.MinValue && iedmTransition5.ID == workflowTransitionID || iedmTransition5.Name == workflowTransitionName)
                    return iedmTransition5;
            }
            return (IEdmTransition5)null;
        }
    }


     

    /// <summary>
    /// Mock object of <see cref="IEdmCmdNode"/>.
    /// </summary>
    public class mEdmCmdNode : IEdmCmdNode
    {
        /// <summary>
        /// Custom implementation of <see cref="IEdmCmdNode.GetProperty(EdmCmdNodeProp, object)"/>.
        /// </summary>
        public Func<EdmCmdNodeProp, object, object> GetPropertyFunc { get; set; }

        /// <summary>
        /// Creates a new instance of the mEdmCmdNode.
        /// </summary>
        /// <param name="getPropertyFunc">Custom implementation of <see cref="IEdmCmdNode.GetProperty(EdmCmdNodeProp, object)"/>.</param>
        public mEdmCmdNode(Func<EdmCmdNodeProp, object, object> getPropertyFunc)
        {
            this.GetPropertyFunc = getPropertyFunc;
        }

        /// <summary>
        /// Gets the specified property for the file changing state.
        /// </summary>
        /// <param name="eProperty">EdmCmdNodeProp.</param>
        /// <param name="oArg">Argument.</param>
        /// <returns>Property value.</returns>
        public object GetProperty(EdmCmdNodeProp eProperty, object oArg)
        {
            return GetPropertyFunc(eProperty, oArg);
        }


    }
    /// <summary>Mock object for the IEdmTaskInstance</summary>
    /// <remarks>This object allows you to set a custom implementation of <see cref="GetValEx(string)"/> and <see cref="GetVar(object)"/> through extended func properties.</remarks>
    public class mEdmTaskInstance : IEdmTaskInstance
    {

        /// <summary>
        /// Gets or sets a custom implementation to <see cref="IEdmTaskInstance.GetVar(object)"./>
        /// </summary>
        public Func<object, string> GetVarExtended { get; set; } = (object obj) => { return obj as string; };

        /// <summary>
        /// Gets or sets a custom implementation to <see cref="IEdmTaskInstance.GetValEx(string)"/>
        /// </summary>
        public Func<string, string> GetVarExExtended { get; set; } = (string obj) => { return obj as string; };



        /// <summary>
        /// Creates a new mock instance of the <see cref="T:EPDM.Interop.epdm.IEdmTaskInstance" />.
        /// </summary>
        public mEdmTaskInstance()
        {

        }

        public void SetProgressRange(int lMax, int lPos, string bsDocStr)
        {
            Console.WriteLine($"Status: {lPos}/{lMax} - Message: {bsDocStr}");

        }
        public void SetProgressPos(int lPos, string bsDocStr)
        {
            Console.WriteLine(bsDocStr);
        }
        public void SetStatus(EdmTaskStatus eStatus, int lHRESULT, string bsCustomMsg, object oNotificationAttachments, string bsExtraNotificationMsg)
        {
            Console.WriteLine($"Status: {eStatus.ToString()} - Message: {bsCustomMsg}");
        }


 
        public EdmTaskStatus GetStatus()
        {
            return EdmTaskStatus.EdmTaskStat_Running;
        }

         

        Dictionary<object, object> internalVarList = new Dictionary<object, object>();
        Dictionary<string, object> internalVarExList = new Dictionary<string, object>();

        /// <summary>
        ///  Returns the value of a variable. Use <see cref="SetVar(object, object)"/> to set the variable. 
        /// </summary>
        /// <param name="oVarIDorName"></param>
        /// <returns></returns>
        /// <remarks><ul>
        /// <li><see cref="GetValEx(string)"/> keeps track of a different list of variables.</li>
        /// <li>Either use variable name or ID per variable. Mixing both will produce incorrect results.</li>
        /// </ul></remarks>.
        public object GetVar(object oVarIDorName)
        {

            var val = default(object);
            internalVarList.TryGetValue(oVarIDorName, out val);
            return val;
        }
        /// <summary>
        /// Sets the value of a variable.
        /// </summary>
        /// <param name="oVarIDorName">ID or Name</param>
        /// <param name="oValue">Value.</param>
        public void SetVar(object oVarIDorName, object oValue)
        {
            if (internalVarList.ContainsKey(oVarIDorName))
                internalVarList[oVarIDorName] = oValue;
            else
                internalVarList.Add(oVarIDorName, oValue);
 
        }
        /// <summary>
        ///  Gets the value of the specified user-defined variable.
        /// </summary>
        /// <param name="bsValName">Name of a user-defined variable.</param>
        /// <returns>Value of user-defined variable</returns>
        public object GetValEx(string bsValName)
        {
            var val = default(object);
            internalVarExList.TryGetValue(bsValName, out val);
            return val;
        }
        /// <summary>
        ///  Sets the value of the specified user-defined variable.
        /// </summary>
        /// <param name="bsValName">Name of a user-defined variable</param>
        /// <param name="oValue">Value of user-defined variable</param>
        /// <returns>Value of user-defined variable</returns>
        public void SetValEx(string bsValName, object oValue)
        {
            if (internalVarExList.ContainsKey(bsValName))
                internalVarExList[bsValName] = oValue;
            else
                internalVarExList.Add(bsValName, oValue);
         
        }
        /// <summary>
        /// Returns the ID of the variable.
        /// </summary>
        public long ID
        {
            get
            {
                var random = new Random();
                return random.Next(0, 1000);
            }
        }
        /// <summary>
        /// Returns the GUID of the instance.
        /// </summary>
        public string InstanceGUID
        {
            get
            {
                return "4AEC2511-9A48-4167-9F27-1A3CA4F162BF";
            }
        }

        public string TaskGUID
        {
            get
            {
                return "4AEC2511-9A48-4167-9F27-1A3CA4F162BF";
            }
        }

        public string TaskName
        {
            get
            {
                return "Test Task";
            }
        }
    }
    /// <summary>Mock-up object for IEdmTaskProperties</summary>
    public class mEdmTaskProperties : IEdmTaskProperties
    {

        Dictionary<int, object> internalVarList = new Dictionary<int, object>();
        Dictionary<string, object> internalVarExList = new Dictionary<string, object>();

        /// <summary>
        ///  Returns the value of a variable. Use <see cref="SetVar(int, object)"/> to set the variable. 
        /// </summary>
        /// <param name="oVarIDorName"></param>
        /// <returns></returns>
        /// <remarks><ul>
        /// <li><see cref="GetValEx(string)"/> keeps track of a different list of variables.</li>
        /// <li>Either use variable name or ID per variable. Mixing both will produce incorrect results.</li>
        /// </ul></remarks>.
        public object GetVar(int oVarIDorName)
        {

            var val = default(object);
            internalVarList.TryGetValue(oVarIDorName, out val);
            return val;
        }
        /// <summary>
        /// Sets the value of a variable.
        /// </summary>
        /// <param name="oVarIDorName">ID or Name</param>
        /// <param name="oValue">Value.</param>
        public void SetVar(int oVarIDorName, object oValue)
        {
            if (internalVarList.ContainsKey(oVarIDorName))
                internalVarList[oVarIDorName] = oValue;
            else
                internalVarList.Add(oVarIDorName, oValue);

        }
        /// <summary>
        ///  Gets the value of the specified user-defined variable.
        /// </summary>
        /// <param name="bsValName">Name of a user-defined variable.</param>
        /// <returns>Value of user-defined variable</returns>
        public object GetValEx(string bsValName)
        {
            var val = default(object);
            internalVarExList.TryGetValue(bsValName, out val);
            return val;
        }
        /// <summary>
        ///  Sets the value of the specified user-defined variable.
        /// </summary>
        /// <param name="bsValName">Name of a user-defined variable</param>
        /// <param name="oValue">Value of user-defined variable</param>
        /// <returns>Value of user-defined variable</returns>
        public void SetValEx(string bsValName, object oValue)
        {
            if (internalVarExList.ContainsKey(bsValName))
                internalVarExList[bsValName] = oValue;
            else
                internalVarExList.Add(bsValName, oValue);

        }


        public void SetSetupPages(EdmTaskSetupPage[] poPages)
        {

        }

        public void GetSetupPages(out EdmTaskSetupPage[] ppoPages)
        {
            throw new NotImplementedException();
        }

        public void SetMenuCmds(EdmTaskMenuCmd[] poCmds)
        {

        }

        public void GetMenuCmds(out EdmTaskMenuCmd[] ppoCmds)
        {
            throw new NotImplementedException();
        }

        public void SetSel(EdmTaskSel[] poSel)
        {

        }

        public void GetSel(out EdmTaskSel[] ppoSel)
        {
            throw new NotImplementedException();
        }

        public int TaskID
        {
            get
            {
                return 1;
            }
        }

        public string TaskName
        {
            get
            {
                return "Task Test";
            }
        }

        public string TaskGUID
        {
            get
            {
                return "027B52CD-5D00-4E2B-93F7-2B85791F2BA3"; ;
            }
        }

        public string AddInName
        {
            get
            {
                return "027B52CD-5D00-4E2B-93F7-2B85791F2BA3";
            }
        }

        public string FormName
        {
            get
            {
                return "027B52CD-5D00-4E2B-93F7-2B85791F2BA3";

            }
        }

        public string UserName
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public int RetryCount
        {
            get
            {
                throw new NotImplementedException();

            }
        }

        public int TimeoutInSeconds
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsScheduled
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public int TaskFlags
        {
            get
            {
                throw new NotImplementedException();

            }
            set
            {

            }
        }
    }


   

}

