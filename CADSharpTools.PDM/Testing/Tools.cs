﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CADSharpTools.Testing
{
    /// <summary>
    /// Testing automation tools for the SOLIDWORKS PDM API. 
    /// </summary>
    public static class Tools
    {
        private static string prefix = "CADSharpTools.PDM Test Executor - ";
        /// <summary>
        /// Executes an array of actions
        /// </summary>
        /// <param name="actions"></param>
        public static void ExecuteTests(Action[] actions)
        {
            var totalStopWatch = new Stopwatch();
            totalStopWatch.Start();
            Console.WriteLine($"{prefix}Starting Tests:");

            foreach (var action in actions)
            {
                var methodInfo = action.Method;
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                Console.WriteLine($"{prefix}Started test:");
                action();
                stopWatch.Stop();
                Console.WriteLine($"{prefix}Finished in {stopWatch.Elapsed.ToString(@"g")}.");

            }

            totalStopWatch.Stop();
            Console.WriteLine($"{prefix}Total elapsed time in {totalStopWatch.Elapsed.ToString(@"g")}.");

            Console.ReadKey();
        }

        /// <summary>
        /// Executes an Action and prints elapsed time in Console.
        /// </summary>
        /// <param name="action">Action object.</param>
        /// <param name="note">Note.</param>
        public static void ExecuteTest(Action action, string note = "")
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            Console.WriteLine($"{prefix}Started test {note}:");
            action();
            Console.WriteLine($"{prefix}Finished in {stopWatch.Elapsed.ToString(@"g")}.");
            stopWatch.Stop();
            Console.ReadKey();
        }

        /// <summary>
        /// Executes a Func and prints elapsed time in Console.
        /// </summary>
        /// <param name="func">Func</param>
        /// <param name="ar">Argument</param>
        /// <returns></returns>
        public static string[] ExecuteTest(Func<string, string[]> func, string ar)
        {
            var ret = func(ar);
            return ret;
        }
       
        /// <summary>
        /// Executes an Action and prints elapsed time in Console.
        /// </summary>
        /// <param name="action">Action</param>
        /// <param name="ar">Argument</param>
        /// <param name="note">Note</param>
        public static void ExecuteTest(Action<string> action, string ar, string note = "")
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            Console.WriteLine($"{prefix}Started test {note}:");
            action(ar);
            Console.WriteLine($"{prefix}Finished in {stopWatch.Elapsed.ToString(@"g")}.");
            stopWatch.Stop();
            Console.ReadKey();
        }

        /// <summary>
        /// Attaches the calling assembly to the debugger.
        /// </summary>
        /// <param name="processId">Process Id of the host application.</param>
        public static void AttachToDebugger(int processId)
        {
            var processStart = new ProcessStartInfo();
            processStart.Arguments = $"-p {processId}";
            processStart.FileName = "vsjitdebugger.exe";
            var process = System.Diagnostics.Process.Start(processStart);
        }
    }

    
}
