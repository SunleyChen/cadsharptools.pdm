﻿using CADSharpTools.MVVM.Views;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CADSharpTools.MVVM.ViewModels
{
    /// <summary>
    /// Serves as the view model for the custom buttons view dialog. Implements the <see cref="INotifyPropertyChanged"/> interface.
    /// </summary>
    /// <typeparam name="T">T that implements an enum.</typeparam>
    /// <example>
    /// <code>
    ///public enum Action_e
    ///{
    ///    Closed,
    ///    [CustomButtonsMessageViewButtonIdentifierAttribute(ButtonPosition_e.First)]
    ///    [Description("Action 1")]
    ///    Action1,
    ///    [CustomButtonsMessageViewButtonIdentifierAttribute(ButtonPosition_e.Second)]
    ///    [Description("Action 2")]
    ///    Action2,
    ///    [CustomButtonsMessageViewButtonIdentifierAttribute(ButtonPosition_e.Third)]
    ///    [Description("Action 3")]
    ///    Action3,
    ///}
    ///public class Test
    ///{
    ///    static void Main(string[] args)
    ///    {
    ///        var viewModel = new CustomButtonsMessageViewModel&lt;Action_e&gt; ();
    ///        viewModel.Message = "This is a message.";
    ///        viewModel.ShowView("Title");
    ///        switch (viewModel.Result.ResultEnumValue)
    ///        {
    ///            case Action_e.Action1:
    ///                Console.WriteLine("Action1 button clicked");
    ///                break;
    ///            case Action_e.Action2:
    ///                Console.WriteLine("Action2 button clicked");
    ///                break;
    ///            case Action_e.Action3:
    ///                Console.WriteLine("Action3 button clicked");
    ///                break;
    ///            default:
    ///                Console.WriteLine("Closed by user.");
    ///                break;
    ///        }
    ///        Console.ReadKey();
    ///    }
    ///}
    /// </code>
    /// This preview shows the flow direction of the action button (right to left):<br/>
    /// <img src="../Images/custombuttonsmessageboxview.png"></img><br/>
    /// </example>

    public class CustomButtonsMessageViewModel<T> : BindableBase where T : Enum
    {
        private string title;
        /// <summary>
        /// Title.
        /// </summary>
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        private string message;
        /// <summary>
        /// Message.
        /// </summary>
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        /// <summary>
        /// Button click command.
        /// </summary>
        public DelegateCommand<object> Command { get; set; }
        private T third;

        private string thirdCaption;
       /// <summary>
       /// Third button caption.
       /// </summary>
        public string ThirdCaption
        {
            get { return thirdCaption; }
            set { SetProperty(ref thirdCaption, value); }
        }

        private string firstCaption;
        /// <summary>
        /// First Button Caption.
        /// </summary>
        public string FirstCaption
        {
            get { return firstCaption; }
            set { SetProperty(ref firstCaption, value); }
        }
        private string secondCaption;
        /// <summary>
        /// First button caption.
        /// </summary>
        public string SecondCaption
        {
            get { return secondCaption; }
            set { SetProperty(ref secondCaption, value); }
        }

        private string fourthCaption;
        /// <summary>
        /// Fourth button caption.
        /// </summary>
        public string FourthCaption
        {
            get { return fourthCaption; }
            set { SetProperty(ref fourthCaption, value); }
        }

        /// <summary>
        /// Third button enum.
        /// </summary>
        public T Third
        {
            get { return third; }
            private set { SetProperty(ref third, value); }
        }
        T fourth;
        /// <summary>
        /// Fourth button enum.
        /// </summary>
        public T Fourth
        {
            get { return fourth; }
            private set { SetProperty(ref fourth, value); }
        }
        private T first;
        /// <summary>
        /// First button enum.
        /// </summary>
        public T First
        {
            get { return first; }
            private set { SetProperty(ref first, value); }
        }
        private T second;
        /// <summary>
        ///  Second button enum.
        /// </summary>
        public T Second
        {
            get { return second; }
            private set { SetProperty(ref second, value); }
        }


     

        private CustomMessageViewResult<T> result = new CustomMessageViewResult<T>();

        /// <summary>
        /// User interaction result from <see cref="Views.CustomButtonsMessageBoxView"/>.
        /// </summary>
        /// <typeparam name="T"><see cref="Enum"/> type that follows conditions set in the remarks section.</typeparam>
        public CustomMessageViewResult<T> Result
        {
            get { return result; }
            private set {  SetProperty(ref result, value); }
        }

        CustomButtonsMessageBoxView view;
        /// <summary>
        /// Shows the view.
        /// </summary>
        /// <param name="Title">Title.</param>
        /// <param name="playErrorSound">Play system sound.</param>
        public void ShowView(string Title = "", bool playErrorSound = false)
        {

            this.Title = Title;
            view = new CADSharpTools.MVVM.Views.CustomButtonsMessageBoxView();
            view.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            view.DataContext = this;
            if (playErrorSound)
                System.Media.SystemSounds.Beep.Play();
            view.ShowDialog();

        }

        string DescriptionAttr<T>(T source)
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }
        /// <summary>
        /// Create a new instance of the CustomMessageViewModel class.
        /// </summary>
        public CustomButtonsMessageViewModel()
        {
            var enums = Enum.GetValues(typeof(T));
            foreach (Enum _enum in enums)
            {
             
                var type = _enum.GetType();
                var memInfo = type.GetMember(_enum.ToString());
                var attributes = memInfo[0].GetCustomAttributes(true);
                foreach (var attribute in attributes)
                {
                    if (attribute is CustomButtonsMessageViewButtonIdentifierAttribute)
                    {
                        var actionResultAttribute = attribute as CustomButtonsMessageViewButtonIdentifierAttribute;
                        if (actionResultAttribute.ButtonPosition == ButtonPosition_e.First)
                        {
                            First = (T)_enum;
                            FirstCaption = DescriptionAttr(_enum);
                        }
                  
   
                        if (actionResultAttribute.ButtonPosition == ButtonPosition_e.Second)
                        {
                            Second = (T)_enum;
                            SecondCaption = DescriptionAttr(_enum);
                        }
                 
                        if (actionResultAttribute.ButtonPosition == ButtonPosition_e.Third)
                        {
                            Third = (T)_enum;
                            ThirdCaption = DescriptionAttr(_enum);
                        }
                        if (actionResultAttribute.ButtonPosition == ButtonPosition_e.Fourth)
                        {
                            Fourth = (T)_enum;
                            FourthCaption = DescriptionAttr(_enum);
                        }
                    }
                }
            }

            Command = new DelegateCommand<object>((object obj) => {

                this.Result = new CustomMessageViewResult<T>() { ResultEnumValue = (T)obj };
                if (view != null)
                    view.Hide();
            });
        }

       

       
    }



    /// <summary>
    /// Result from user in action with <see cref="Views.CustomButtonsMessageBoxView"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CustomMessageViewResult<T> where T : Enum
    {
        /// <summary>
        /// Value of the result.
        /// </summary>
        public T ResultEnumValue { get; set; }
    }



    /// <summary>
    /// Used to identify <see cref="Views.CustomButtonsMessageBoxView"/> buttons in your custom enum.
    /// </summary>
    public class CustomButtonsMessageViewButtonIdentifierAttribute : Attribute
    {
        /// <summary>
        /// Custom button result.
        /// </summary>
        /// <param name="position"></param>
        public CustomButtonsMessageViewButtonIdentifierAttribute(ButtonPosition_e position)
        {
            this.ButtonPosition = position;
        }

        /// <summary>
        /// Position of the button.
        /// </summary>
        public ButtonPosition_e ButtonPosition { get; set; }
    }

    /// <summary>
    /// Button position.
    /// </summary>
    public enum ButtonPosition_e
    {
        /// <summary>
        /// First.
        /// </summary>
        First,
        /// <summary>
        /// Second.
        /// </summary>
        Second,
        /// <summary>
        /// Third.
        /// </summary>
        Third,
        /// <summary>
        /// Fourth.
        /// </summary>
        Fourth
    }



}
