﻿using CADSharpTools.Core;
using CADSharpTools.MVVM.Views;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace CADSharpTools.MVVM.ViewModels
{
    /// <summary>
    /// Serves as the view model for the busy indicator view.
    /// </summary>
    /// <typeparam name="T">Callback type must implement <see cref="busy"/></typeparam>
    public class BusyIndicatorViewModel<T> : BindableBase  where T : busyIndicatorCallBack
    {
        BusyIndicatorView view = default(BusyIndicatorView);

        private bool isBusy = true;

        /// <summary>
        /// Gets or sets the state of the Busy state. 
        /// </summary>
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }


        private string timeElapsed;
        /// <summary>
        /// Returns formatted elapsed time since the view was shown.
        /// </summary>
        public string TimeElapsed
        {
            get { return timeElapsed; }
            set { SetProperty(ref timeElapsed, value); }
        }
        /// <summary>
        /// Cancel command.
        /// </summary>
        public DelegateCommand CancelCommand { get; set; }

        private string title;
        /// <summary>
        /// Title of the busy indicator window.
        /// </summary>
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }
        private T callback;

        /// <summary>
        /// Callback object.
        /// </summary>
        public T Callback
        {
            get { return callback; }
            set { SetProperty(ref callback, value); }
        }

        /// <summary>
        /// Creates a new instance of the BusyIndicatorViewModel class.
        /// </summary>
        /// <param name="callback">Callback object.</param>
        /// <param name="title">Title of the view.</param>
        public BusyIndicatorViewModel(T callback, string title)
        {
            this.Callback = callback;
            this.CancelCommand = new DelegateCommand(() =>
            {
                 
                this.Callback.Cancel();
                this.IsBusy = false; 

            }, ()=>{ return IsBusy; });

            CancelCommand.ObservesProperty(() => this.IsBusy);

            this.Title = title;
            this.Callback.Finished += Callback_Finished;
        }

        private BusyIndicatorViewModel()
        {

        }

        private void Callback_Finished(object sender, busyIndicatorCallBackFinishEventArgs e)
        {
            if (view != null)
            {
                view.Dispatcher.Invoke(() => {

                    switch (e.State)
                    {
                        case busyIndicatorCallBackState_e.Finished:
                            view.Hide();
                            break;
                        case busyIndicatorCallBackState_e.Canceled:
                            {
                                this.Callback.IsIndeterminate = false;
                                this.Title = "User has canceled this operation.";
                                if (timer != null)
                                    timer.Stop();
                                break;
                            }
                        default:
                            break;
                    }

                   
                });
            }
                    
             
        }

        private Timer timer = default(Timer);

        /// <summary>
        /// Shows the busy indicator view dialog. Use this method to avoid referencing WPF libraries.
        /// </summary>
        /// <param name="playErrorSound">Play error sound toggle.</param>
        public void ShowView(bool playErrorSound = false)
        {
                view = new Views.BusyIndicatorView();
                view.Topmost = true;
                view.InitializeComponent();
                view.DataContext = this;
                if (playErrorSound)
                    System.Media.SystemSounds.Beep.Play();
                // Start the Dispatcher Processing 
                timer = new Timer();
                timer.Interval = 1000;
                timer.Enabled = true;
                timer.Tick += Timer_Tick;
                timer.Start();
                view.ShowDialog();
                // When the window closes, shut down the dispatcher

             

        }
        private int elapsedSeconds = 0;
        private void Timer_Tick(object sender, EventArgs e)
        {
            elapsedSeconds++;
            var ts = TimeSpan.FromSeconds(elapsedSeconds);
            this.TimeElapsed = $"Time elapsed: {ts.Hours.ToString("D2")}:{ts.Minutes.ToString("D2")}:{ts.Seconds.ToString("D2")}";
        }
    }
}
