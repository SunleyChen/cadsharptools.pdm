﻿using CADSharpTools.Core;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;

namespace CADSharpTools.MVVM.ViewModels
{
    /// <summary>
    /// Serves as the view model for the error view dialog. Implements the <see cref="System.ComponentModel.INotifyPropertyChanged"/> interface.
    /// </summary>
    public class ErrorViewModel<T> :BindableBase
    {
        private string supportEmail = "amen@cadsharp.com";
        /// <summary>
        /// Gets or sets the support e-mail.
        /// </summary>
        public string SupportEmail
        {
            get { return supportEmail; }
            set { SetProperty(ref supportEmail, value); }
        }
        bool isIdle = true;      
        public bool IsIdle { get { return isIdle; } set { SetProperty(ref isIdle, value) ;} }
        string programFullName = string.Empty;

        /// <summary>
        /// Gets or sets the error view dialog title.
        /// </summary>
        public string ProgramFullName { get
            {
              return programFullName;
            } set
            {
                SetProperty(ref programFullName,value)
                ;
            }
        }

        MethodReturn<T> selectedError = default(MethodReturn<T>); 
        public MethodReturn<T> SelectedError { get
            {
                return selectedError;
            } set
            {
                SetProperty(ref selectedError, value);
            }
        }
        ObservableCollection<MethodReturn<T>> errors = new ObservableCollection<MethodReturn<T>>();
        /// <summary>
        /// Errors property.
        /// </summary>
        public ObservableCollection<MethodReturn<T>> Errors { get {
                return errors;
                ; } set {
                SetProperty(ref errors, value);
                ; } }

        /// <summary>
        /// Creates a new instance of the ErrorViewModel class 
        /// </summary>
        /// <param name="errors">Array MethodReturns</param>
        /// <param name="callingProgramAssembly">Calling assembly.</param>
        /// <remarks>The calling assembly's product name and version will used as the title of the error view dialog.</remarks>
        public ErrorViewModel(MethodReturn<T>[] errors, Assembly callingProgramAssembly)
        {
            foreach (var error in errors)
            {
                Errors.Add(error);
            }
            ProgramFullName = callingProgramAssembly.GetAssemblyNameAndVersion();
            SendReportCommand = new DelegateCommand(() => { SendReport(); }, () => { return IsIdle; });
            SendReportCommand.ObservesProperty<bool>(() => this.IsIdle);

            if (Errors.Count > 0)
                SelectedError = Errors[0];

        }

        /// <summary>
        /// Show the error view.
        /// </summary>
        /// <param name="Title">Title to be shown in the error view window.</param>
        /// <param name="playErrorSound">Play system sound.</param>
        public void ShowView(string Title, bool playErrorSound = false)
        {
            ProgramFullName = Title;
            var view = new CADSharpTools.MVVM.Views.ErrorView();
            view.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            view.DataContext = this;
            if (playErrorSound)
                System.Media.SystemSounds.Beep.Play();
            view.ShowDialog();

        }


        /// <summary>
        /// Send the report command.
        /// </summary>
        public DelegateCommand SendReportCommand { get; set; }

        /// <summary>
        /// Sends the report to the specified email. Set email by using <see cref="SupportEmail"/>.
        /// </summary>
        public void SendReport()
        {
            IsIdle = false;
            string NewLine = "%0A";
            string errors = string.Empty;
            var errorsList = new List<string>(); 
            foreach (var error in Errors)
            {
                string errorMessage =  NewLine + "TimeStamp: " + error.TimeStamp +  NewLine  +"Message: " + error.Exception.Message;
                string stackTrace = string.IsNullOrWhiteSpace(error.Exception.StackTrace) ? "No stack trace for this exception." : error.Exception.StackTrace; 
                errorMessage = errorMessage + NewLine + "Stacktrace: " + stackTrace  +   NewLine;
                errorsList.Add(errorMessage);  
            }

            string body = string.Join(NewLine.ToString(), errorsList.ToArray()); 
            string message =   NewLine + "Please review the following exceptions:" +  NewLine   + body;
            message = message.Trim();
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.FileName = $"mailto:{SupportEmail}?subject=Error Log for {ProgramFullName}&body={message}";
            IsIdle = true;
            proc.Start();
            
        }



     

    }
}
