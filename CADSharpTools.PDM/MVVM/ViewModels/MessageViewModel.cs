﻿using CADSharpTools.Core;
using Prism.Commands;
using Prism.Mvvm;

namespace CADSharpTools.MVVM.ViewModels
{
    /// <summary>
    /// Serves as the view model for the MessageView dialog. Implements the <see cref="System.ComponentModel.INotifyPropertyChanged"/> interface.
    /// </summary>
    /// <remarks>The message view dialog has a read-only caption that is copyable by the user.
    /// </remarks>

    public class MessageViewModel : BindableBase 
    {
        CADSharpTools.MVVM.Views.MessageBoxView view = default(CADSharpTools.MVVM.Views.MessageBoxView);
        private DelegateCommand closeCommand;
        /// <summary>
        /// Close command.
        /// </summary>
        public DelegateCommand CloseCommand
        {
            get { return closeCommand; }
            set { SetProperty(ref closeCommand, value); }
        }


        private string programFullName;
        /// <summary>
        /// Gets or sets the program name in the title of the message view. 
        /// </summary>
        public string ProgramFullName
        {
            get { return programFullName; }
            set { SetProperty(ref programFullName, value); }
        }
        private string message;
        /// <summary>
        /// Get or sets the message in the caption of the message view.
        /// </summary>
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }
        /// <summary>
        /// Creates a new instance of the MessageViewModel class.
        /// </summary>
        /// <param name="message"></param>
        public MessageViewModel(string message)
        {
            this.Message = message;
            this.CloseCommand = new DelegateCommand(() => {
               if(view != null)
                {
                    view.Close();
                }
            });

        }

        /// <summary>
        /// Shows the message view.
        /// </summary>
        /// <param name="callingAssembly">Assembly object.</param>
        /// <param name="playErrorSound">Play error sound toggle.</param>
        /// <remarks>The calling assembly's product name and version will used as the title of the message view dialog.</remarks>
        /// <example>
        /// <code>
        ///using CADSharpTools.MVVM.ViewModels;
        ///using System;
        ///namespace CADSharpTools.Tests
        ///{
        ///    class Test
        ///    {     
        ///        [STAThread]
        ///        static void Main(string[] args)
        ///        {
        ///            var messageViewModel = new MessageViewModel("This is a copyable caption.");
        ///            string programName = "Program version 1.0.0";
        ///            messageViewModel.ShowView(programName, false);
        ///            Console.ReadKey();
        ///        }
        ///    }
        ///}
        /// </code>
        /// </example>
        public void ShowView(System.Reflection.Assembly callingAssembly, bool playErrorSound = false)
        {


            ProgramFullName = callingAssembly.GetAssemblyNameAndVersion();
            view = new CADSharpTools.MVVM.Views.MessageBoxView();
            view.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            view.DataContext = this;
            if (playErrorSound)
            System.Media.SystemSounds.Beep.Play();
            view.ShowDialog();

        }
        /// <summary>
        /// Shows the message view.
        /// </summary>
        /// <param name="Title">message view dialog's title</param>
        /// <param name="playErrorSound">Play error sound toggle.</param>
        /// <example>
        /// <code>
        ///using CADSharpTools.MVVM.ViewModels;
        ///using System;
        ///namespace CADSharpTools.Tests
        ///{
        ///    class Test
        ///    {     
        ///        [STAThread]
        ///        static void Main(string[] args)
        ///        {
        ///            var messageViewModel = new MessageViewModel("This is a copyable caption.");
        ///            string programName = "Program version 1.0.0";
        ///            messageViewModel.ShowView(programName, false);
        ///            Console.ReadKey();
        ///        }
        ///    }
        ///}
        /// </code>
        /// </example>
        public void ShowView(string Title, bool playErrorSound = false)
        {
            ProgramFullName = Title;
            view = new CADSharpTools.MVVM.Views.MessageBoxView();
            view.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            view.DataContext = this;
            if (playErrorSound)
                System.Media.SystemSounds.Beep.Play();
            view.ShowDialog();

        }


    }
}
