﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CADSharpTools.MVVM.Models
{
    /// <summary>
    /// This class can be used to track the progress of repetitive operations. This class implements the <see cref="BindableBase"/> class and <see cref="IProgress{T}"/> interface.
    /// </summary> 
    public class Progress : BindableBase , IProgress<int>
    {
        #region fields 
        int _value;
        int max;
        bool isIndeterminate;
        string message;
        string progressColor = Consts.GreenColor;
        #endregion
        #region properties  
        /// <summary>
        /// Current operation index.
        /// </summary>
        public int Value
        {
            get
            {
                return _value;
            }
            set
            {
                SetProperty<int>(ref _value, value);
            }
        }
        /// <summary>
        /// Index of the last operation.
        /// </summary>
        public int Max
        {
            get
            {
                return max;
            }
            set
            {
                SetProperty<int>(ref max, value);
            }
        }
        /// <summary>
        ///  Gets or sets the IsInderminate property.
        /// </summary>
        /// <remarks>Bind this property to the <see cref="ProgressBar.IsIndeterminate"/> property.</remarks>
        public bool IsIndeterminate
        {
            get
            {
                return isIndeterminate;
            }
            set
            {
                SetProperty<bool>(ref isIndeterminate, value);
            }
        }
        /// <summary>
        /// Gets or sets a descriptive message of the current <see cref="Value"/> being processed.
        /// </summary>
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                SetProperty<string>(ref message, value);
            }
        }
        /// <summary>
        /// Gets or sets the color of the progress bar.
        /// </summary>
        /// <remarks>When binding to the WPF <see cref="ProgressBar"/> control, you may use hexadecimal colors for a wider array of choices.</remarks>
        public string ProgressColor
        {
            get
            {
                return progressColor;
            }
            set
            {
                SetProperty<string>(ref progressColor, value);
            }
        }

        #endregion
        #region constructors
        public Progress()
        {

        }
        #endregion
        #region methods 

        /// <summary>
        /// Cancels the progress. Invoke this method when the user requests a cancellation.
        /// </summary>
        public void Cancel()
        {
            this.Value = this.Max;
            this.IsIndeterminate = false;
            this.ProgressColor = Consts.RedColor;
            this.Message = Consts.Canceled;
        }
        /// <summary>
        /// Force the progress to complete successfully.
        /// </summary>
        public void Complete()
        {
            this.Value = this.Max;
            this.IsIndeterminate = false;
            this.ProgressColor = Consts.GreenColor;
            this.Message = Consts.Complete;
        }
        /// <summary>
        /// Completes the progress with an error.  
        /// </summary>
        /// <param name="Error">Error message.</param>
        public void CompleteWithError(string Error = "")
        {
            this.Value = this.Max;
            this.IsIndeterminate = false;
            this.ProgressColor = Consts.RedColor;
            if (string.IsNullOrWhiteSpace(Error))
                this.Message = Consts.CompleteWithError;
            else
                this.Message = Error;
        }

        /// <summary>
        /// Reset the progress object. Invoke this method after user cancellation or to reset the progress object after completion.
        /// </summary>
        public void Reset()
        {
            this.Value = 0;
            this.Max = int.MaxValue;
            this.IsIndeterminate = false;
            this.ProgressColor = Consts.GreenColor;
            this.Message = Consts.Complete;
        }

        /// <summary>
        /// Report the progress. 
        /// </summary>
        /// <param name="value">Progress value.</param>
        /// <remarks>This method sets the value of the <see cref="Value"/> property.</remarks>
        public void Report(int value)
        {
            this.Value = value;
        }

        #endregion
    }


    
}
