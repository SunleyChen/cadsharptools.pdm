﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CADSharpTools.MVVM.Views
{
    /// <summary>
    /// Interaction logic for CustomMessageBoxView.xaml
    /// </summary>
    public partial class CustomButtonsMessageBoxView : Window
    {
        /// <summary>
        /// Creates a new instance of the CustomButtonsMessageBoxView class.
        /// </summary>
        public CustomButtonsMessageBoxView()
        {
            InitializeComponent();
        }
    }
}
