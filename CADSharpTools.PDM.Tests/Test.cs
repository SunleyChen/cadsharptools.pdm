﻿using SolidWorks.Interop.sldcostingapi;
using CADSharpTools.SOLIDWORKS;
using System;
using SolidWorks.Interop.sldworks;
using CADSharpTools.Core;
using EPDM.Interop.epdm;
using CADSharpTools.Testing;
using CADSharpTools.Core.Models;
using CADSharpTools.MVVM.ViewModels;
using System.ComponentModel;
using CADSharpTools.SOLIDWORKS.UIAutomation;
using System.Runtime.Serialization;
using Prism.Mvvm;

namespace CADSharpTools.PDM.Tests
{
 
public enum Action_e
    {
     
        Closed,
        [CustomButtonsMessageViewButtonIdentifierAttribute(ButtonPosition_e.First)]
        [Description("Action 1")]
        Action1,
        [CustomButtonsMessageViewButtonIdentifierAttribute(ButtonPosition_e.Second) ]
        [Description("Action 2")]
        Action2,
       [CustomButtonsMessageViewButtonIdentifierAttribute(ButtonPosition_e.Third)]
        [Description("Action 3")]
        Action3,
        [CustomButtonsMessageViewButtonIdentifierAttribute(ButtonPosition_e.First)]
        [Description("Action 4")]
        Action4,

    }

    public class Callback : BindableBase, ctpICallback
    {
        public bool CancelRequested => false;

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }
        public void Cancel()
        {
            Console.WriteLine("Canceled.");
        }

        public void Finish()
        {
            Console.WriteLine("Finished");
        }
    }

    public class Test
    {
            #region fields 
            static string dmKey = "CADSharpLLC:swdocmgr_general-11785-02051-00064-01025-08567-34307-00007-29120-54298-07506-41248-61715-06266-30575-28676-54158-18481-24557-29847-14773-56823-22779-01292-19729-34209-49609-12593-00269-25656-23148-57052-23276-24676-28770-2,swdocmgr_previews-11785-02051-00064-01025-08567-34307-00007-43040-36281-26991-40707-05830-50596-39683-18434-32612-47076-48670-23310-47737-60023-23098-01292-19729-34209-49609-12593-00269-25656-23148-57052-23276-24676-28770-2,swdocmgr_dimxpert-11785-02051-00064-01025-08567-34307-00007-06288-00679-42549-02170-07026-35415-17529-12292-34177-63394-28625-49684-44090-06454-24551-01292-19729-34209-49609-12593-00269-25656-23148-57052-23276-24676-28770-0,swdocmgr_geometry-11785-02051-00064-01025-08567-34307-00007-48296-09260-24767-53186-37338-23673-55012-43015-24043-56384-64818-14007-17434-16465-23618-01292-19729-34209-49609-12593-00269-25656-23148-57052-23276-24676-28770-8,swdocmgr_xml-11785-02051-00064-01025-08567-34307-00007-11912-30875-03042-00474-32057-53501-55659-45063-60708-40034-19966-44180-59104-59590-24091-01292-19729-34209-49609-12593-00269-25656-23148-57052-23276-24676-28770-6,swdocmgr_tessellation-11785-02051-00064-01025-08567-34307-00007-22792-27674-36549-22791-26332-58805-39243-10240-08450-56057-00714-52213-55442-12672-22598-01292-19729-34209-49609-12593-00269-25656-23148-57052-23276-24676-28770-8";
        #endregion

        public const string VaultName = "PDM2019";
        public const string pathName = @"c:\pdm2019\pdm2019\assem1.sldasm";
        public const string largeAssemblyPathName = @"c:\pdm2019\pdm2019\costing\large customer assy\2596-A0000.sldasm";
        [STAThread]
        static void Main(string[] args)
        {
            var callback = new Callback();
            callback.PropertyChanged += Callback_PropertyChanged;
            var vault = EdmObjectFactory.LoginToVault("PDM2019");
            var folder = vault.RootFolder;

            var file = EdmObjectFactory.GetFileObjectFromRelativePath(vault, @"references\file.sldprt");
            var FolderID = file.GetFolder().ID;
            var document = new Document(file.ID, FolderID, file.GetLocalPath(FolderID));
            document.ConstructWhereUsedReferenceTree(vault, callback);
            Console.ReadLine();
        }

        private static void Callback_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Console.WriteLine((sender as Callback).Message);
        }

        public static void Serialize()
    {
            var vault = EdmObjectFactory.LoginToVault("PDM2019");
            var file = EdmObjectFactory.GetFileObjectFromRelativePath(vault, @"\");

        Console.ReadKey();
    }
  
     

    public static void OnTaskSetup_Test()
        {
            var vault = CADSharpTools.Testing.EdmObjectFactory.LoginToVault("PDM2019", 0);
            var uc = new testUC();
            // mock command 
            var poCmd = CADSharpTools.Testing.EdmObjectFactory.MockEdmCmd(EdmCmdType.EdmCmd_TaskSetup, vault, "", 0, 0, 0, EdmRefreshFlag.EdmRefresh_FileList, false, false, new CADSharpTools.Testing.mEdmTaskInstance());
            // create shell 
            var shell = OnTaskSetupUserControlContainer<testUC>.CreateShell(uc, ref poCmd);
            shell.ShowDialog();
        }

        public static void CreateApplicationInstanceAndSleepForTwoSec_Test()
        {
            using (CADSharpTools.SOLIDWORKS.Application app = new Application(-1, true))
            {
                System.Threading.Thread.Sleep(2000);
            }
        }

       public static void PrintAllVisiblePartsWithDocMan(string key, string assemblyPathName, string configurationName)
        {

            swDocManCallBackTestClass callback = default(swDocManCallBackTestClass);
            callback = new swDocManCallBackTestClass();
            callback.IsIndeterminate = true;
            MethodReturn<DocumentManager.Models.swDocManVisibleReferencedDocument[]> visibleReferencesRet = default(MethodReturn<DocumentManager.Models.swDocManVisibleReferencedDocument[]>);
             var docMan = new CADSharpTools.DocumentManager.swDocMan(key);


            var longTimeAction = new Action(() => {

                visibleReferencesRet = docMan.GetAllVisibleReferencedPartsFromAssembly(assemblyPathName, configurationName, callback);
            });

            var busyIndicatorInvoke = new busyIndicatorInvoker(callback);
            busyIndicatorInvoke.InvokeAsync(longTimeAction);
            var busyIndicator = new CADSharpTools.MVVM.ViewModels.BusyIndicatorViewModel<swDocManCallBackTestClass>(callback,"This is a title");
            busyIndicator.ShowView(true);

            
        
            if (visibleReferencesRet.IsError)
                Console.WriteLine(visibleReferencesRet.Error);
            else
            {
                int count = 0;
                var visibleReferences = visibleReferencesRet.Value;
                Console.WriteLine($"Found {visibleReferences.Length} visible referenced documents.");
                foreach (var visibleReference in visibleReferences)
                {
                    count = count + visibleReference.ReferencedConfigurationNames.Length;
                    Console.WriteLine(visibleReference.PathName.ToLower());
                }
                Console.WriteLine($"Found {count} referenced configurations.");
            }

        }   
        public static void WriteEvaluatedDirectoryToConsole()
        {
            var ret = Core.Extension.EvaluateDirectory(@"<Desktop>\Hello.txt", CADSharpTools_e.SpecialFolder.Desktop);
            if (ret.IsError)
                Console.WriteLine(ret.Error);
            else
                Console.WriteLine(ret.Value);
        }
        public static void CreateEdmCmd_Test()
        {
            var vault = CADSharpTools.Testing.EdmObjectFactory.LoginToVault(VaultName);
            var taskInstance = new CADSharpTools.Testing.mEdmTaskInstance();
            var file = CADSharpTools.Testing.EdmObjectFactory.GetFileObjectFromRelativePath(vault, @"RandomPart.sldprt");
            var cmdData = CADSharpTools.Testing.EdmObjectFactory.MockEdmCmdDataFromFile(file, EPDM.Interop.epdm.EdmCmdType.EdmCmd_TaskRun, taskInstance,"Default");
            var cmd = CADSharpTools.Testing.EdmObjectFactory.MockEdmCmd(EPDM.Interop.epdm.EdmCmdType.EdmCmd_TaskRun, vault);
        }
        public static void GetUserObjectAndPrint_Test()
        {

            var vault = CADSharpTools.Testing.EdmObjectFactory.LoginToVault(VaultName);
            var userName = CADSharpTools.Testing.EdmObjectFactory.GetUserObject(vault,"Admin");
            Console.Write($"User login: {userName.Name}");
        }
        public static void CreateFileObjectFromRelativePathAndPrint_Test()
        {

            var vault = CADSharpTools.Testing.EdmObjectFactory.LoginToVault(VaultName);
            var file = CADSharpTools.Testing.EdmObjectFactory.GetFileObjectFromRelativePath(vault,@"RandomPart.sldprt");
            Console.Write($"File name : {file.Name}");
        }
        public static void PrintRandomPDMUserFullName_Test()
        {
            var vault = CADSharpTools.Testing.EdmObjectFactory.LoginToVault(VaultName);
            var usersRet = vault.GetAllUsers();

            var users = usersRet.Value;
            var randomUser = users[0];

            Console.WriteLine($"User full name: {randomUser.GetFullName()}");
        

        }
        public static void GetWorkflowstatesAndPrintNameAndDescription_Test()
        {
            var vault = CADSharpTools.Testing.EdmObjectFactory.LoginToVault(VaultName);
            var workflowsRet = vault.GetAllWorkflows();
            var workflows = workflowsRet.Value;
            foreach (var workflow in workflows)
            {
                Console.WriteLine($"Workflow name:[{workflow.Name}] - workflow description:[{workflow.Description}]");
                var statesRet = workflow.GetAllWorkflowStates();
                var states = statesRet.Value;
                foreach (var state in states)
                {
                    Console.WriteLine($"    state name:[{state.Name}]");
                }
            }

        }
        public static void GetWorkflowTransitionsAndPrintNameAndDescription_Test()
        {
            var vault = CADSharpTools.Testing.EdmObjectFactory.LoginToVault(VaultName);
            var workflowsRet = vault.GetAllWorkflows();
            var workflows = workflowsRet.Value;
            foreach (var workflow in workflows)
            {
                Console.WriteLine($"Workflow name:[{workflow.Name}] - workflow description:[{workflow.Description}]");
                var transitionsRet = workflow.GetAllWorkflowTransitions();
                var transitions = transitionsRet.Value;
                foreach (var transition in transitions)
                {
                    Console.WriteLine($"    transition name:[{transition.Name}]");
                }
            }

        }
        public static void GetWorkflowsAndPrintNameAndDescription_Test()
        {
            var vault = CADSharpTools.Testing.EdmObjectFactory.LoginToVault(VaultName);
            var workflowsRet = vault.GetAllWorkflows();
            var workflows = workflowsRet.Value;
            foreach (var workflow in workflows)
            {
                Console.WriteLine($"Workflow name:[{workflow.Name}] - workflow description:[{workflow.Description}]");
            }

        }
        public static void CreateVaultObject_Test()
        {
            var vault = CADSharpTools.Testing.EdmObjectFactory.LoginToVault(VaultName);
            Console.WriteLine(vault.Name);

        }
        public static void GetDataVariableForSldPrtFile_Test()
        {
            var vault = CADSharpTools.Testing.EdmObjectFactory.LoginToVault("PDM2019");
            var file = CADSharpTools.Testing.EdmObjectFactory.GetFileObjectFromRelativePath(vault, "randompart.sldprt");
            var authorVariableRer = file.GetDataCardVariable("Author");
            Console.WriteLine($"Author is : {authorVariableRer.Value}");

        }
        public static void GetDataVariableForTextFileWithThereNoLocalCopy_Test()
        {
            string variableName = "sw-lastcosted";
            var vault = CADSharpTools.Testing.EdmObjectFactory.LoginToVault("PDM2019");
            var file = CADSharpTools.Testing.EdmObjectFactory.GetFileObjectFromRelativePath(vault, "part1.sldprt");
            var authorVariableRer = file.GetVariableFromDb(variableName, "Default");
            Console.WriteLine($"{variableName} : {authorVariableRer.Value}");

        }
        public static void GetDataVariableForDrawing_Test()
        {
            var vault = CADSharpTools.Testing.EdmObjectFactory.LoginToVault("PDM2019");
            var file = CADSharpTools.Testing.EdmObjectFactory.GetFileObjectFromRelativePath(vault, "draw1.slddrw");
            var authorVariableRer = file.GetDataCardVariable("Author");
            Console.WriteLine($"Author is :{authorVariableRer.Value}");

        }
        public static void CloseNewSolidworksDocumentWindowDialogWithJustTitleInformation_Test()
        {
            var swApp = CADSharpTools.SOLIDWORKS.Singleton.GetApplication();
            swApp.Visible = true;
            int processID = swApp.GetProcessID();
            var newDocumentDialog = new CADSharpTools.SOLIDWORKS.UIAutomation.swDialog("New SOLIDWORKS Document",null, SOLIDWORKS.UIAutomation.swDialogButtonAction_e.CloseDialog, SOLIDWORKS.UIAutomation.swDialogCompareMethod_e.Title);
            var swDialogWatcher = new CADSharpTools.SOLIDWORKS.UIAutomation.swDialogWatcher(processID);
            swDialogWatcher.AddDialog(newDocumentDialog);
            swDialogWatcher.Start();
        }
        public static void ClickOKNewSolidworksDocumentWindowDialogWithJustTitleInformation_Test()
        {
            var swApp = CADSharpTools.SOLIDWORKS.Singleton.GetApplication(-1,true);
            int processID = swApp.GetProcessID();
            var newDocumentDialog =
                new CADSharpTools.SOLIDWORKS.UIAutomation.swDialog("New SOLIDWORKS Document", null, SOLIDWORKS.UIAutomation.swDialogButtonAction_e.ClickOK, SOLIDWORKS.UIAutomation.swDialogCompareMethod_e.Title);
            var swDialogWatcher = new CADSharpTools.SOLIDWORKS.UIAutomation.swDialogWatcher(processID);
            swDialogWatcher.AddDialog(newDocumentDialog);
            swDialogWatcher.Start();           
            // swDialogWatcher.Stop() stops watching.
        }
        public static void ClickCancelOnCloseWindow_Test()
        {
            var swApp = CADSharpTools.SOLIDWORKS.Singleton.GetApplication();
            swApp.Visible = true;
            int processID = swApp.GetProcessID();
            var warningModifiedFilesWindow =
                                new CADSharpTools.SOLIDWORKS.UIAutomation.swDialog("SOLIDWORKS", new string[] { "One or more documents being closed as a result of this operation have been modified." }, SOLIDWORKS.UIAutomation.swDialogButtonAction_e.ClickCancel, SOLIDWORKS.UIAutomation.swDialogCompareMethod_e.Title);

     
            var swDialogWatcher = new CADSharpTools.SOLIDWORKS.UIAutomation.swDialogWatcher(processID);
            swDialogWatcher.AddDialog(warningModifiedFilesWindow);
            swDialogWatcher.Start();

            // Use swDialogWatcher.Stop() stops watching.
        } 
      
 
         
    }

    public class swDocManCallBackTestClass : busyIndicatorCallBack
    {

        public override void Finish()
        {
            this.Message = "Complete.";
            base.Finish();
        }

        
      

         

    }
}
