﻿Public Module Test

    Public Function GetSOLIDWORKSPDMAddInLocation() As String
        Dim baseReg = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry64)
        Dim reg = baseReg.OpenSubKey("SOFTWARE\Classes\CLSID\{DD2533E5-1513-40D8-82B4-927790D0A895}\InprocServer32")
        If reg Is Nothing Then
            Return String.Empty
        End If
        Return reg.GetValue("").ToString()
    End Function

    Public Sub main()
        Debug.Print(GetSOLIDWORKSPDMAddInLocation())
    End Sub

End Module
